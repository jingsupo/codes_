//go:build linux || darwin || windows || 386 || amd64

package main

import (
	"database/sql"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"gitee.com/jingsupo/goutils/utils"
	"github.com/go-gota/gota/dataframe"
	"github.com/go-gota/gota/series"
	_ "github.com/mattn/go-sqlite3"
)

type Place struct {
	Status   string
	Info     string
	Infocode string
	Pois     []Pois
	Count    string
}

type Pois struct {
	Name     string
	Id       string
	Location string
	Type_    string `json:"type"`
	Typecode string
	Pname    string
	Cityname string
	Adname   string
	Address  string
	Pcode    string
	Citycode string
	Adcode   string
}

type Geocoding struct {
	Status    string
	Regeocode Regeocode
	Info      string
	Infocode  string
}

type Regeocode struct {
	AddressComponent AddressComponent
	FormattedAddress string
}

type AddressComponent struct {
	City          []string
	Province      string
	Adcode        string
	District      string
	Towncode      string
	StreetNumber  StreetNumber
	Country       string
	Township      string
	BusinessAreas []BusinessAreas
	Building      Building
	Neighborhood  Neighborhood
	Citycode      string
}

type StreetNumber struct {
	Number    string
	Location  string
	Direction string
	Distance  string
	Street    string
}

type BusinessAreas struct {
	Location string
	Name     string
	Id       string
}

type Building struct {
	Name  string
	Type_ string `json:"type"`
}

type Neighborhood struct {
	Name  string
	Type_ string `json:"type"`
}

var ak_amap = os.Getenv("ak_amap")

// 累计发送请求次数
var request_num = 0

var zxs = []string{"北京市", "上海市", "天津市", "重庆市"}
var zxx []string

func main() {
	defer utils.RecoverError()
	pcasv := get_df("data.sqlite")
	sxx := utils.Unique(pcasv.Filter(dataframe.F{Colname: "city", Comparator: series.Eq, Comparando: "省直辖县级行政区划"}).Col("county").Records())
	zzx := utils.Unique(pcasv.Filter(dataframe.F{Colname: "city", Comparator: series.Eq, Comparando: "自治区直辖县级行政区划"}).Col("county").Records())
	zxx = append(sxx, zzx...)
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		panic(err)
	}
	city_list := utils.ReadLines(filepath.Join(dir, "city_list.conf"))
	query_list := utils.ReadLines(filepath.Join(dir, "query_list.conf"))
	for _, city := range city_list {
		for _, query := range query_list {
			save_csv(pcasv, city, query)
		}
	}
}

func get_df(dbPath string) dataframe.DataFrame {
	db, err := sql.Open("sqlite3", dbPath)
	if err != nil {
		panic(err)
	}
	rows, err := db.Query(`
	SELECT v.code,v.areaCode,p.name AS province,c.name AS city,a.name AS county,s.name AS town,v.name AS village
	FROM village v
	JOIN street s ON v.streetCode=s.code
	JOIN area a ON v.areaCode=a.code
	JOIN city c ON v.cityCode=c.code
	JOIN province p ON v.provinceCode=p.code
	ORDER BY v.code
	`)
	if err != nil {
		panic(err)
	}
	var records [][]string
	for rows.Next() {
		var code, areaCode, province, city, county, town, village string
		err = rows.Scan(&code, &areaCode, &province, &city, &county, &town, &village)
		if err != nil {
			panic(err)
		}
		records = append(records, []string{code, areaCode, province, city, county, town, village})
	}
	pcasv := dataframe.LoadRecords(records, dataframe.HasHeader(false))
	newnames := []string{"code", "areaCode", "province", "city", "county", "town", "village"}
	for i, name := range pcasv.Names() {
		pcasv = pcasv.Rename(newnames[i], name)
	}
	return pcasv
}

func amap_place_search(api_key, query, region, page_size, page_num string) *Place {
	url := fmt.Sprintf("https://restapi.amap.com/v5/place/text?key=%s&output=json&types=%s&region=%s&page_size=%s&page_num=%s", api_key, query, region, page_size, page_num)
	resp := utils.Get(url)
	v := new(Place)
	resp.Json(v)
	return v
}

func amap_reverse_geocoding(api_key, location string) *Geocoding {
	url := fmt.Sprintf("https://restapi.amap.com/v3/geocode/regeo?key=%s&output=json&location=%s", api_key, location)
	resp := utils.Get(url)
	v := new(Geocoding)
	resp.Json(v)
	return v
}

func save_csv(df dataframe.DataFrame, city string, query string) {
	fmt.Printf("%s%s%s\n", strings.Repeat("-", 60), city, strings.Repeat("-", 60))
	fmt.Printf("%s%s%s\n", strings.Repeat("-", 30), query, strings.Repeat("-", 30))
	var county []string
	switch {
	case utils.InSlice(zxs, city):
		county = utils.Unique(df.Filter(dataframe.F{Colname: "province", Comparator: series.Eq, Comparando: city}).Col("county").Records())
	case utils.InSlice(zxx, city):
		county = []string{city}
		city = utils.Unique(df.Filter(dataframe.F{Colname: "county", Comparator: series.Eq, Comparando: city}).Col("province").Records())[0]
	default:
		county = utils.Unique(df.Filter(dataframe.F{Colname: "city", Comparator: series.Eq, Comparando: city}).Col("county").Records())
	}
	fmt.Printf("county: %v\n", county)
	for _, area := range county {
		fmt.Println(area)
		var adcode string
		if utils.InSlice(zxs, city) || utils.InSlice(zxx, city) {
			adcode = utils.Unique(df.
				Filter(dataframe.F{Colname: "province", Comparator: series.Eq, Comparando: city}).
				Filter(dataframe.F{Colname: "county", Comparator: series.Eq, Comparando: area}).
				Col("areaCode").Records())[0]
		} else {
			adcode = utils.Unique(df.
				Filter(dataframe.F{Colname: "city", Comparator: series.Eq, Comparando: city}).
				Filter(dataframe.F{Colname: "county", Comparator: series.Eq, Comparando: area}).
				Col("areaCode").Records())[0]
		}
		var page_num = 1
		var data []string
		for {
			if request_num%100 == 0 {
				fmt.Printf("**********累计发送请求次数：%d**********\n", request_num)
			}
			resp := amap_place_search(ak_amap, query, adcode, "20", strconv.Itoa(page_num))
			if resp.Status != "1" || len(resp.Pois) == 0 {
				break
			}
			request_num += 1
			for _, item := range resp.Pois {
				regeocode := amap_reverse_geocoding(ak_amap, item.Location).Regeocode
				ac := regeocode.AddressComponent
				var city string
				if len(ac.City) == 0 {
					city = ""
				} else {
					city = ac.City[0]
				}
				line := fmt.Sprintf("%s,%s,%s,%s,%s,%s,%s,%s\n",
					ac.Province,
					city,
					ac.District,
					ac.Township,
					ac.StreetNumber.Street,
					ac.StreetNumber.Number,
					ac.Neighborhood.Name,
					ac.Building.Name)
				data = append(data, line)
			}
			fmt.Printf("page_num: %d\n", page_num)
			page_num += 1
		}
		utils.WriteLines(fmt.Sprintf("%s_%s_%s.csv", city, area, query), data)
	}
}
