package main

import (
	"context"
	"fmt"
	"gitee.com/jingsupo/goutils/database"
	"gitee.com/jingsupo/goutils/utils"
	"github.com/go-redis/redis/v8"
	"go.mongodb.org/mongo-driver/mongo"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

var (
	ctx context.Context
	//任务数
	taskNum int
	//保存链接的channel
	urlChan chan string
	//保存监控任务的channel
	taskChan   chan string
	wg         sync.WaitGroup
	collection *mongo.Collection
	rdb        *redis.Client
)

const (
	baseUrl = "https://www.allbankswiftcodes.com"
)

//获取当前页所有目标链接并添加到channel中
func getUrls(url string) {
	html := utils.Get(url).Text()
	h3 := utils.Xpath(html, "/html/body/main/h3/text()")
	num, _ := strconv.Atoi(strings.Split(strings.Split(h3[0], "(")[1], ")")[0])
	for i := 2; i < num+2; i++ {
		href := utils.Xpath(html, fmt.Sprintf("/html/body/main/table/tbody/tr[%d]/td[3]/a", i))
		fullUrl := baseUrl + href[0]
		urlChan <- fullUrl
	}
	taskChan <- url
	wg.Done()
}

//获取当前页所需内容
func GetResult(url string) {
	{
		//判断url是否已经爬取过
		val, err := rdb.SIsMember(ctx, "crawled-links", url).Result()
		if err != nil {
			fmt.Println(err)
			return
		}
		if val {
			fmt.Printf("%s\t已经爬取过\n", url)
			return
		}
	}
	html := utils.Get(url).Text()
	var _bic []string
	if len(strings.Split(url, "/")[4]) == 8 {
		_bic = utils.Xpath(html, "/html/body/main/table/tbody/tr[1]/td[2]/span[1]/text()")
	} else {
		_bic = utils.Xpath(html, "/html/body/main/table/tbody/tr[1]/td[2]/text()")
	}
	_name := utils.Xpath(html, "/html/body/main/table/tbody/tr[2]/td[2]/span/text()")
	_country := utils.Xpath(html, "/html/body/main/table/tbody/tr[5]/td[2]/div/a/div[2]/span/text()")
	_address := utils.Xpath(html, "/html/body/main/table/tbody/tr[7]/td[2]/text()")
	if len(_bic) == 0 || len(_name) == 0 || len(_country) == 0 || len(_address) == 0 {
		fmt.Println("xpath解析结果为空")
		{
			//将爬取失败的url存入redis
			_, err := rdb.SAdd(ctx, "failed-links", url).Result()
			if err != nil {
				fmt.Println(err)
				return
			}
		}
		return
	}
	bic := _bic[0]
	name := strings.ReplaceAll(_name[0], "\n", " ")
	country := _country[0]
	address := strings.ReplaceAll(_address[0], "\n", " ")
	{
		//写入数据库
		fmt.Println("insert one...")
		data := map[string]interface{}{
			"country": country,
			"data": map[string]string{
				"bic":     bic,
				"name":    name,
				"country": country,
				"address": address,
			},
		}
		result, err := collection.InsertOne(context.TODO(), data)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println(result)
	}
	fmt.Printf("完成爬取内容：%s\n", url)
	{
		//将已爬取的url存入redis
		_, err := rdb.SAdd(ctx, "crawled-links", url).Result()
		if err != nil {
			fmt.Println(err)
			return
		}
	}
}

//从channel中获取url并爬取所需内容
func getResults() {
	for url := range urlChan {
		GetResult(url)
	}
	wg.Done()
}

//监控所有任务是否完成，完成则关闭channel
func taskCheck() {
	//计数
	var count int
	for {
		url := <-taskChan
		fmt.Printf("完成爬取链接：%s\n", url)
		count++
		if count == taskNum {
			close(urlChan)
			break
		}
	}
	wg.Done()
}

func main() {
	//获取命令行参数
	args := os.Args
	if len(args) < 2 {
		fmt.Println("请输入国家和页数")
		return
	}
	if len(args) < 3 {
		fmt.Println("请输入页数")
		return
	}
	country := args[1]
	num, err := strconv.Atoi(args[2])
	if err != nil {
		fmt.Println("页数必须为数字")
		return
	}
	taskNum = num

	//初始化MongoDB
	database.InitMongo("mongodb://localhost:27017")
	defer database.CloseMongo()
	db := database.GetDatabase("demo")
	collection = database.GetCollection(db, country)
	//获取Redis连接
	ctx = context.TODO()
	rdb = database.GetRedis("localhost:6379", 1)

	//初始化数据channel
	urlChan = make(chan string, 1000000)
	taskChan = make(chan string, taskNum)

	//计时开始
	start := time.Now()

	fmt.Println("开始爬取链接...")

	//开启链接爬取协程
	for i := 1; i < taskNum+1; i++ {
		wg.Add(1)
		go getUrls(baseUrl + fmt.Sprintf("/%s/?page=%d", country, i))
	}

	//开启任务监控协程
	wg.Add(1)
	go taskCheck()

	fmt.Println("开始爬取内容...")

	//开启内容爬取协程
	for i := 0; i < 5; i++ {
		wg.Add(1)
		go getResults()
	}

	wg.Wait()

	fmt.Println("==========")
	fmt.Println("任务完成")

	//计时结束
	fmt.Printf("开始时间：%s\n", start.Format(time.Stamp))
	fmt.Printf("结束时间：%s\n", time.Now().Format(time.Stamp))
	fmt.Printf("程序运行时间：%s\n", time.Since(start))
}
