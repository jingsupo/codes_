package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"

	"github.com/bits-and-blooms/bitset"
)

func main() {
	n := 1000000000
	start := time.Now()
	var b bitset.BitSet
	for i := 0; i < n; i++ {
		v := uint(rand.Intn(n))
		b.Set(v)
	}
	f, _ := os.Create("bs.txt")
	b.WriteTo(f)
	f, _ = os.Open("bs.txt")
	b.ReadFrom(f)
	fmt.Printf("b.Count(): %v\n", b.Count())
	f, _ = os.Create("bs_set.txt")
	for i, e := b.NextSet(0); e; i, e = b.NextSet(i + 1) {
		f.WriteString(strconv.Itoa(int(i)) + "\n")
	}
	fmt.Println("耗时：", time.Since(start))
}
