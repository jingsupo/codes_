package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/gob"
	"log"
	"time"
)

type Block struct {
	// 版本号
	Version uint64
	// 前区块哈希
	PrevHash []byte
	// Merkel根（一个哈希值）
	MerkelRoot []byte
	// 时间戳
	TimeStamp uint64
	// 难度值
	Difficulty uint64
	// 随机数，挖矿要找的数据
	Nonce uint64

	// 当前区块哈希
	Hash []byte //正常比特币区块中没有，为了方便做了简化
	// 真实的交易
	Transactions []*Transaction
}

func NewBlock(txs []*Transaction, prevHash []byte) *Block {
	block := Block{
		Version:      0,
		PrevHash:     prevHash,
		MerkelRoot:   []byte{},
		TimeStamp:    uint64(time.Now().Unix()),
		Difficulty:   difficulty,
		Nonce:        0,
		Hash:         []byte{},
		Transactions: txs,
	}
	block.MerkelRoot = block.GenMerkelRoot()
	// 工作量证明
	pow := NewProofOfWork(&block)
	// 查找随机数，不停地进行哈希运算
	hash, nonce := pow.Run()
	block.Hash = hash
	block.Nonce = nonce
	return &block
}

func (block *Block) Serialize() []byte {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(block)
	if err != nil {
		log.Fatal(err)
	}
	return buf.Bytes()
}

func Deserialize(data []byte) Block {
	var block Block
	dec := gob.NewDecoder(bytes.NewReader(data))
	err := dec.Decode(&block)
	if err != nil {
		log.Fatal(err)
	}
	return block
}

// 生成梅克尔根
func (block *Block) GenMerkelRoot() []byte {
	var info []byte
	for _, tx := range block.Transactions {
		info = append(info, tx.TXID...)
	}
	hash := sha256.Sum256(info)
	return hash[:]
}
