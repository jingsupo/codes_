package main

import (
	"bytes"
	"crypto/ecdsa"
	"fmt"
	"log"
	"os"
	"time"

	"gitee.com/jingsupo/goutils/utils"
	bolt "go.etcd.io/bbolt"
)

const blockChainDB = "blockChain.db"
const blockBucket = "blockBucket"
const lastHashKey = "lastHash"

type BlockChain struct {
	db       *bolt.DB
	lastHash []byte //最后一个区块的哈希
}

// 创世块
func GenesisBlock(address string, data string) *Block {
	cb := NewCoinbaseTX(address, data)
	return NewBlock([]*Transaction{cb}, []byte{})
}

// 创建区块链
func CreateBlockChain(address string, data string) *BlockChain {
	// 校验地址
	if !IsValidAddress(address) {
		fmt.Printf("地址无效：%s\n", address)
		os.Exit(1)
	}
	if utils.Exists(blockChainDB) {
		fmt.Println("区块链已经存在！")
		os.Exit(1)
	}
	var lastHash []byte //最后一个区块的哈希
	db, err := bolt.Open(blockChainDB, 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucket([]byte(blockBucket))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		genesisBlock := GenesisBlock(address, data)
		b.Put(genesisBlock.Hash, genesisBlock.Serialize())
		b.Put([]byte(lastHashKey), genesisBlock.Hash)
		lastHash = genesisBlock.Hash
		return nil
	})
	return &BlockChain{db, lastHash}
}

// 返回已存在的区块链实例
func NewBlockChain() *BlockChain {
	if !utils.Exists(blockChainDB) {
		fmt.Println("请先创建区块链！")
		os.Exit(1)
	}
	var lastHash []byte //最后一个区块的哈希
	db, err := bolt.Open(blockChainDB, 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(blockBucket))
		if b == nil {
			fmt.Printf("bucket不存在")
			os.Exit(1)
		}
		lastHash = b.Get([]byte(lastHashKey))
		return nil
	})
	return &BlockChain{db, lastHash}
}

func (bc *BlockChain) AddBlock(txs []*Transaction) {
	var validTX []*Transaction
	for _, tx := range txs {
		if !bc.VerifyTransaction(tx) {
			fmt.Println("矿工发现无效交易")
			continue
		}
		validTX = append(validTX, tx)
	}
	db := bc.db
	lastHash := bc.lastHash
	db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(blockBucket))
		if b == nil {
			log.Fatal("bucket为空")
		}
		block := NewBlock(validTX, lastHash)
		b.Put(block.Hash, block.Serialize())
		b.Put([]byte(lastHashKey), block.Hash)
		// 更新内存中区块链的最后一个区块哈希
		bc.lastHash = block.Hash
		return nil
	})
}

func (bc *BlockChain) PrintBlockChain() {
	blockHeight := 0
	bc.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(blockBucket))
		if b == nil {
			log.Fatal("bucket不存在")
		}
		b.ForEach(func(k, v []byte) error {
			if bytes.Equal(k, []byte(lastHashKey)) {
				return nil
			}
			block := Deserialize(v)
			fmt.Printf("==========区块高度：%d==========\n", blockHeight)
			blockHeight++
			fmt.Printf("版本号：%d\n", block.Version)
			fmt.Printf("前区块哈希：%x\n", block.PrevHash)
			fmt.Printf("Merkel根：%x\n", block.MerkelRoot)
			timestamp := time.Unix(int64(block.TimeStamp), 0).Format("2006-01-02 15:04:05")
			fmt.Printf("时间戳：%s\n", timestamp)
			fmt.Printf("难度值：%d\n", block.Difficulty)
			fmt.Printf("随机数：%d\n", block.Nonce)
			fmt.Printf("当前区块哈希：%x\n", block.Hash)
			fmt.Printf("区块数据：%s\n", block.Transactions[0].TXInputs[0].PubKey)
			return nil
		})
		return nil
	})
}

func (bc *BlockChain) PrintTX() {
	it := bc.NewIterator()
	for {
		block := it.Next()
		for _, tx := range block.Transactions {
			fmt.Println(tx)
		}
		if len(block.PrevHash) == 0 {
			fmt.Println("====================")
			fmt.Println("区块链遍历完成.")
			break
		}
	}
}

func (bc *BlockChain) GetBalance(address string) {
	// 校验地址
	if !IsValidAddress(address) {
		fmt.Printf("地址无效：%s\n", address)
		return
	}
	// 生成公钥哈希
	pubKeyHash := GetPubKeyFromAddress(address)
	utxos := bc.FindUTXO(pubKeyHash)
	total := 0.0
	for _, utxo := range utxos {
		total += utxo.Amount
	}
	fmt.Printf("\"%s\"余额为：%f\n", address, total)
}

func (bc *BlockChain) Send(from string, to string, amount float64, miner string, data string) {
	// 校验地址
	if !IsValidAddress(from) {
		fmt.Printf("invalid from: %s\n", from)
		return
	}
	if !IsValidAddress(to) {
		fmt.Printf("invalid to: %s\n", to)
		return
	}
	if !IsValidAddress(miner) {
		fmt.Printf("invalid miner: %s\n", miner)
		return
	}
	fmt.Printf("from: %s\nto: %s\namount: %f\nminer: %s\ndata: %s\n", from, to, amount, miner, data)
	// 1.创建挖矿交易
	cb := NewCoinbaseTX(miner, data)
	// 2.创建普通交易
	tx := NewTransaction(from, to, amount, bc)
	if tx == nil {
		return
	}
	// 3.添加到区块
	bc.AddBlock([]*Transaction{cb, tx})
	fmt.Println("转账成功")
}

func (bc *BlockChain) FindUTXOTransaction(pubKeyHash []byte) []*Transaction {
	var txs []*Transaction
	// 消耗过的output，key是交易ID，value是交易中索引的数组
	spentUTXO := make(map[string][]int64)
	it := bc.NewIterator()
	for {
		// 1.遍历区块
		block := it.Next()
		// 2.遍历交易
		for _, tx := range block.Transactions {
			// fmt.Printf("当前交易ID：%x\n", tx.TXID)
		OUTPUT:
			// 3.遍历output，找到和自己相关的UTXO（添加output之前检查一下是否已消耗过）
			for i, output := range tx.TXOutputs {
				// fmt.Printf("当前索引：%d\n", i)
				// 过滤已经消耗过的output
				if spentUTXO[string(tx.TXID)] != nil {
					for _, j := range spentUTXO[string(tx.TXID)] {
						if int64(i) == j {
							continue OUTPUT
						}
					}
				}
				if bytes.Equal(output.PubKeyHash, pubKeyHash) {
					// 重点
					txs = append(txs, tx)
				}
			}
			// 跳过挖矿交易
			if !tx.IsCoinbase() {
				// 4.遍历input，找到自己消耗过的UTXO
				for _, input := range tx.TXInputs {
					_pubKeyHash := HashPubKey(input.PubKey)
					if bytes.Equal(_pubKeyHash, pubKeyHash) {
						spentUTXO[string(input.TxId)] = append(spentUTXO[string(input.TxId)], input.Index)
					}
				}
			}
		}
		if len(block.PrevHash) == 0 {
			break
		}
	}
	return txs
}

// 找到指定地址的所有UTXO
func (bc *BlockChain) FindUTXO(pubKeyHash []byte) []TXOutput {
	var UTXO []TXOutput
	txs := bc.FindUTXOTransaction(pubKeyHash)
	for _, tx := range txs {
		for _, output := range tx.TXOutputs {
			if bytes.Equal(output.PubKeyHash, pubKeyHash) {
				UTXO = append(UTXO, output)
			}
		}
	}
	return UTXO
}

// 找到需要的UTXO
func (bc *BlockChain) FindNeededUTXO(pubKeyHash []byte, amount float64) (map[string][]int64, float64) {
	UTXO := make(map[string][]int64)
	var totalAmount float64
	txs := bc.FindUTXOTransaction(pubKeyHash)
	for _, tx := range txs {
		for i, output := range tx.TXOutputs {
			if bytes.Equal(output.PubKeyHash, pubKeyHash) {
				if totalAmount < amount {
					UTXO[string(tx.TXID)] = append(UTXO[string(tx.TXID)], int64(i))
					totalAmount += output.Amount
					if totalAmount >= amount {
						return UTXO, totalAmount
					}
				}
			}
		}
	}
	return UTXO, totalAmount
}

//根据id查找交易
func (bc *BlockChain) FindTransactionByID(id []byte) *Transaction {
	it := bc.NewIterator()
	for {
		block := it.Next()
		for _, tx := range block.Transactions {
			if bytes.Equal(tx.TXID, id) {
				return tx
			}
		}
		if len(block.PrevHash) == 0 {
			break
		}
	}
	return nil
}

// 交易创建的最后进行签名
func (bc *BlockChain) SignTransaction(tx *Transaction, privateKey *ecdsa.PrivateKey) bool {
	if tx.IsCoinbase() {
		return true
	}
	prevTXMap := make(map[string]*Transaction)
	// 找到所有引用的交易
	for _, input := range tx.TXInputs {
		tx := bc.FindTransactionByID(input.TxId)
		if tx == nil {
			return false
		}
		prevTXMap[string(input.TxId)] = tx
	}
	return tx.Sign(privateKey, prevTXMap)
}

func (bc *BlockChain) VerifyTransaction(tx *Transaction) bool {
	if tx.IsCoinbase() {
		return true
	}
	prevTXMap := make(map[string]*Transaction)
	// 找到所有引用的交易
	for _, input := range tx.TXInputs {
		tx := bc.FindTransactionByID(input.TxId)
		if tx == nil {
			return false
		}
		prevTXMap[string(input.TxId)] = tx
	}
	return tx.Verify(prevTXMap)
}
