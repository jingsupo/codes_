package main

import (
	"fmt"
	"os"
	"strconv"
)

const usage = `
Usage:
createBlockChain 地址          "创建区块链"
printChain                     "打印区块链"
printTX                        "打印交易"
getBalance 地址                "获取指定地址的余额"
send FROM TO AMOUNT MINER DATA "转账"
createWallet                   "创建钱包"
listAddresses                  "列出所有钱包地址"
`

type Cli struct {
	// bc *BlockChain
}

func checkArgs(count int) {
	if len(os.Args) != count {
		fmt.Println("参数错误")
		fmt.Print(usage)
		os.Exit(1)
	}
}

func (cli *Cli) Run() {
	// 获取命令行参数
	args := os.Args
	if len(args) < 2 {
		fmt.Print(usage)
		return
	}
	cmd := args[1]
	switch cmd {
	case "createBlockChain":
		fmt.Println("创建区块链...")
		checkArgs(3)
		cli.CreateBlockChain(args[2], "创世块")
	case "printChain":
		fmt.Println("打印区块链...")
		checkArgs(2)
		cli.PrintBlockChain()
	case "printTX":
		fmt.Println("打印交易...")
		checkArgs(2)
		cli.PrintTX()
	case "getBalance":
		fmt.Println("获取余额...")
		checkArgs(3)
		cli.GetBalance(args[2])
	case "send":
		fmt.Println("开始转账...")
		checkArgs(7)
		from := args[2]
		to := args[3]
		amount, _ := strconv.ParseFloat(args[4], 64)
		miner := args[5]
		data := args[6]
		cli.Send(from, to, amount, miner, data)
	case "newWallet":
		fmt.Println("创建钱包...")
		checkArgs(2)
		cli.CreateWallet()
	case "listAddresses":
		fmt.Println("列出所有的钱包地址...")
		checkArgs(2)
		cli.ListAddresses()
	default:
		fmt.Print(usage)
	}
}

func (cli *Cli) CreateBlockChain(address string, data string) {
	bc := CreateBlockChain(address, data)
	defer bc.db.Close()
}

func (cli *Cli) PrintBlockChain() {
	bc := NewBlockChain()
	defer bc.db.Close()
	bc.PrintBlockChain()
}

func (cli *Cli) PrintTX() {
	bc := NewBlockChain()
	defer bc.db.Close()
	bc.PrintTX()
}

func (cli *Cli) GetBalance(address string) {
	bc := NewBlockChain()
	defer bc.db.Close()
	bc.GetBalance(address)
}

func (cli *Cli) Send(from string, to string, amount float64, miner string, data string) {
	bc := NewBlockChain()
	defer bc.db.Close()
	bc.Send(from, to, amount, miner, data)
}

func (cli *Cli) CreateWallet() {
	ws := NewWallets()
	address := ws.CreateWallet()
	fmt.Printf("地址：%s\n", address)
}

func (cli *Cli) ListAddresses() {
	ws := NewWallets()
	addresses := ws.GetAddresses()
	for _, address := range addresses {
		fmt.Printf("地址：%s\n", address)
	}
}
