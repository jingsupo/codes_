package main

import (
	"log"

	bolt "go.etcd.io/bbolt"
)

type BlockChainIterator struct {
	db          *bolt.DB
	currentHash []byte //游标哈希
}

func (bc *BlockChain) NewIterator() *BlockChainIterator {
	return &BlockChainIterator{
		db:          bc.db,
		currentHash: bc.lastHash, //初始为最后一个区块的哈希
	}
}

func (it *BlockChainIterator) Next() *Block {
	var block Block
	it.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(blockBucket))
		if b == nil {
			log.Fatal("bucket不存在")
		}
		v := b.Get(it.currentHash)
		block = Deserialize(v)
		// 游标哈希左移
		it.currentHash = block.PrevHash
		return nil
	})
	return &block
}
