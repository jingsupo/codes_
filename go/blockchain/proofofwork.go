package main

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"math/big"

	"gitee.com/jingsupo/goutils/utils"
)

//系统调节的难度值
const difficulty = 16

type ProofOfWork struct {
	block  *Block
	target *big.Int
}

func NewProofOfWork(block *Block) *ProofOfWork {
	pow := ProofOfWork{
		block: block,
	}
	// // 指定的难度值，string类型
	// targetStr := "00001000000000000000000000000000000000000000000000000000000000000"
	// // 将难度值转换为big.Int类型
	// targetInt := big.Int{}
	// targetInt.SetString(targetStr, 16)
	targetInt := big.NewInt(1)
	targetInt.Lsh(targetInt, 256-difficulty)
	pow.target = targetInt
	return &pow
}

func (pow *ProofOfWork) Run() ([]byte, uint64) {
	var nonce uint64
	var hash [32]byte
	block := pow.block
	for {
		// 拼接数据
		field := [][]byte{
			utils.Uint64ToByte(block.Version),
			block.PrevHash,
			block.MerkelRoot,
			utils.Uint64ToByte(block.TimeStamp),
			utils.Uint64ToByte(block.Difficulty),
			utils.Uint64ToByte(nonce),
		}
		blockInfo := bytes.Join(field, []byte{})
		// 哈希运算
		hash = sha256.Sum256(blockInfo)
		// 将hash数组转化为big.Int
		bigInt := big.Int{}
		bigInt.SetBytes(hash[:])
		// 比较当前哈希值和目标值，如果当前哈希值小于目标值就说明找到了
		if bigInt.Cmp(pow.target) == -1 {
			fmt.Printf("挖矿成功！hash: %x, nonce: %d\n", hash, nonce)
			break
		} else {
			nonce++
		}
	}
	return hash[:], nonce
}
