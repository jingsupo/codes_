package main

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/sha256"
	"encoding/gob"
	"fmt"
	"log"
	"math/big"
	"strings"
)

const reward = 50 //挖矿奖励

// 1.定义交易结构
type Transaction struct {
	TXID      []byte     //交易ID
	TXInputs  []TXInput  //交易输入
	TXOutputs []TXOutput //交易输出
}

type TXInput struct {
	TxId      []byte //引用的交易ID
	Index     int64  //引用的output的索引值
	Signature []byte //数字签名
	PubKey    []byte //公钥，不是公钥的哈希
}

type TXOutput struct {
	Amount     float64 //转账金额
	PubKeyHash []byte  //公钥的哈希，不是公钥
}

func NewTXOutput(amount float64, address string) TXOutput {
	output := TXOutput{Amount: amount}
	output.Lock(address)
	return output
}

func (output *TXOutput) Lock(address string) {
	output.PubKeyHash = GetPubKeyFromAddress(address)
}

func (tx *Transaction) SetHash() {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(tx)
	if err != nil {
		log.Fatal(err)
	}
	data := buf.Bytes()
	hash := sha256.Sum256(data)
	tx.TXID = hash[:]
}

// 2.创建交易（挖矿交易）
func NewCoinbaseTX(address string, data string) *Transaction {
	// 挖矿交易的特点
	// 1.只有一个input
	// 2.无需引用交易ID
	// 3.无需引用index
	// 由于矿工挖矿时无需指定签名，所以Sig字段可以由矿工随意填写，一般填写矿池的名字
	// 签名先填写为空，后面创建完整交易后，最后做一次签名即可
	input := TXInput{TxId: []byte{}, Index: -1, Signature: nil, PubKey: []byte(data)}
	output := NewTXOutput(reward, address)
	// 对于挖矿交易来说，只有一个input和一个output
	tx := Transaction{TXID: []byte{}, TXInputs: []TXInput{input}, TXOutputs: []TXOutput{output}}
	tx.SetHash()
	return &tx
}

func (tx *Transaction) IsCoinbase() bool {
	if len(tx.TXInputs) == 1 && len(tx.TXInputs[0].TxId) == 0 && tx.TXInputs[0].Index == -1 {
		return true
	}
	return false
}

// 创建普通转账交易
func NewTransaction(from string, to string, amount float64, bc *BlockChain) *Transaction {
	// 创建交易之后要进行数字签名->获取私钥->打开钱包
	ws := NewWallets()
	// 找到自己的钱包
	wallet := ws.WalletsMap[from]
	if wallet == nil {
		fmt.Println("没有找到该地址的钱包，交易创建失败！")
		return nil
	}
	// 获取公钥、私钥
	pubKey := wallet.PubKey
	privateKey := wallet.PrivateKey
	// 传递公钥的哈希，而不是传递地址
	pubKeyHash := HashPubKey(pubKey)

	var inputs []TXInput
	var outputs []TXOutput
	// 1.找到需要的UTXO
	utxos, totalAmount := bc.FindNeededUTXO(pubKeyHash, amount)
	if totalAmount < amount {
		fmt.Println("余额不足")
		return nil
	}
	// 2.将UTXO转为inputs
	for id, indexArr := range utxos {
		for _, index := range indexArr {
			input := TXInput{TxId: []byte(id), Index: index, Signature: nil, PubKey: pubKey}
			inputs = append(inputs, input)
		}
	}
	// 3.创建outputs
	output := NewTXOutput(amount, to)
	outputs = append(outputs, output)
	// 4.找零
	if totalAmount > amount {
		output = NewTXOutput(totalAmount-amount, from)
		outputs = append(outputs, output)
	}
	tx := Transaction{TXID: []byte{}, TXInputs: inputs, TXOutputs: outputs}
	tx.SetHash()
	if !bc.SignTransaction(&tx, privateKey) {
		fmt.Println("签名失败")
		return nil
	}
	return &tx
}

// 创建当前交易的副本，字段Signature和PubKey设置为nil
func (tx *Transaction) TrimmedCopy() *Transaction {
	var inputs []TXInput
	for _, input := range tx.TXInputs {
		inputs = append(inputs, TXInput{TxId: input.TxId, Index: input.Index, Signature: nil, PubKey: nil})
	}
	return &Transaction{TXID: tx.TXID, TXInputs: inputs, TXOutputs: tx.TXOutputs}
}

// 签名的具体实现
func (tx *Transaction) Sign(privateKey *ecdsa.PrivateKey, prevTXMap map[string]*Transaction) bool {
	if tx.IsCoinbase() {
		return true
	}
	txCopy := tx.TrimmedCopy()
	for i, input := range txCopy.TXInputs {
		prevTX := prevTXMap[string(input.TxId)]
		if len(prevTX.TXID) == 0 {
			log.Fatal("引用的交易无效")
		}
		txCopy.TXInputs[i].PubKey = prevTX.TXOutputs[input.Index].PubKeyHash
		// SetHash得到TXID，这个TXID就是要签名的最终数据
		txCopy.SetHash()
		// 还原，以免影响后面input的签名
		txCopy.TXInputs[i].PubKey = nil
		signHash := txCopy.TXID
		// 签名
		r, s, err := ecdsa.Sign(rand.Reader, privateKey, signHash)
		if err != nil {
			log.Fatal(err)
		}
		signature := append(r.Bytes(), s.Bytes()...)
		tx.TXInputs[i].Signature = signature
	}
	return true
}

// 对每一个签名的input进行校验
func (tx *Transaction) Verify(prevTXMap map[string]*Transaction) bool {
	if tx.IsCoinbase() {
		return true
	}
	txCopy := tx.TrimmedCopy()
	for i, input := range txCopy.TXInputs {
		prevTX := prevTXMap[string(input.TxId)]
		if len(prevTX.TXID) == 0 {
			log.Fatal("引用的交易无效")
		}
		txCopy.TXInputs[i].PubKey = prevTX.TXOutputs[input.Index].PubKeyHash
		txCopy.SetHash()
		signHash := txCopy.TXID
		signature := input.Signature
		pubKey := input.PubKey
		r, s := big.Int{}, big.Int{}
		r.SetBytes(signature[:len(signature)/2])
		s.SetBytes(signature[len(signature)/2:])
		X, Y := big.Int{}, big.Int{}
		X.SetBytes(pubKey[:len(pubKey)/2])
		Y.SetBytes(pubKey[len(pubKey)/2:])
		_pubKey := ecdsa.PublicKey{Curve: elliptic.P256(), X: &X, Y: &Y}
		// 校验
		if !ecdsa.Verify(&_pubKey, signHash, &r, &s) {
			return false
		}
	}
	return true
}

func (tx *Transaction) String() string {
	var lines []string
	lines = append(lines, fmt.Sprintf("*** Transaction %x:", tx.TXID))
	for i, input := range tx.TXInputs {
		lines = append(lines, fmt.Sprintf("    Input %d:", i))
		lines = append(lines, fmt.Sprintf("    TxId:      %x", input.TxId))
		lines = append(lines, fmt.Sprintf("    Index:     %d", input.Index))
		lines = append(lines, fmt.Sprintf("    Signature: %x", input.Signature))
		lines = append(lines, fmt.Sprintf("    PubKey:    %x", input.PubKey))
	}
	for i, output := range tx.TXOutputs {
		lines = append(lines, fmt.Sprintf("    Output %d:", i))
		lines = append(lines, fmt.Sprintf("    Amount:     %f", output.Amount))
		lines = append(lines, fmt.Sprintf("    PubKeyHash: %x", output.PubKeyHash))
	}
	return strings.Join(lines, "\n")
}
