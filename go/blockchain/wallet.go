package main

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/sha256"
	"log"

	"github.com/btcsuite/btcd/btcutil/base58"
	"golang.org/x/crypto/ripemd160"
)

type Wallet struct {
	// 私钥
	PrivateKey *ecdsa.PrivateKey
	// 这里的PubKey不存储原始的公钥，而是存储X和Y的拼接字符串，在校验端重新拆分
	PubKey []byte
}

func NewWallet() *Wallet {
	// 创建曲线
	curve := elliptic.P256()
	// 生成私钥
	privateKey, err := ecdsa.GenerateKey(curve, rand.Reader)
	if err != nil {
		log.Fatal(err)
	}
	// 生成公钥（原始的公钥）
	_pubKey := privateKey.PublicKey
	// 拼接X和Y
	pubKey := append(_pubKey.X.Bytes(), _pubKey.Y.Bytes()...)
	return &Wallet{PrivateKey: privateKey, PubKey: pubKey}
}

func (w *Wallet) NewAddress() string {
	pubKey := w.PubKey
	ripHashValue := HashPubKey(pubKey)
	version := byte(00)
	payload := append([]byte{version}, ripHashValue...)
	checkCode := CheckSum(payload)
	payload = append(payload, checkCode...) //25字节
	address := base58.Encode(payload)
	return address
}

func HashPubKey(data []byte) []byte {
	hash := sha256.Sum256(data)
	ripHasher := ripemd160.New()
	_, err := ripHasher.Write(hash[:])
	if err != nil {
		log.Fatal(err)
	}
	ripHashValue := ripHasher.Sum(nil)
	return ripHashValue
}

func CheckSum(data []byte) []byte {
	h := sha256.Sum256(data)
	h = sha256.Sum256(h[:])
	checkCode := h[:4]
	return checkCode
}

func IsValidAddress(address string) bool {
	addressByte := base58.Decode(address)
	if len(addressByte) < 4 {
		return false
	}
	payload := addressByte[:len(addressByte)-4]
	checksum1 := addressByte[len(addressByte)-4:]
	checksum2 := CheckSum(payload)
	return bytes.Equal(checksum1, checksum2)
}
