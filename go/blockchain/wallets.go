package main

import (
	"bytes"
	"crypto/elliptic"
	"encoding/gob"
	"log"
	"os"

	"gitee.com/jingsupo/goutils/utils"
	"github.com/btcsuite/btcd/btcutil/base58"
)

const walletFile = "wallet.dat"

type Wallets struct {
	// map[地址]钱包
	WalletsMap map[string]*Wallet
}

// 返回当前所有钱包的实例
func NewWallets() *Wallets {
	var ws Wallets
	ws.WalletsMap = make(map[string]*Wallet)
	ws.loadFile()
	return &ws
}

func (ws *Wallets) CreateWallet() string {
	wallet := NewWallet()
	address := wallet.NewAddress()
	ws.WalletsMap[address] = wallet
	ws.saveFile()
	return address
}

// 把新建的wallet保存到文件里
func (ws *Wallets) saveFile() {
	var buf bytes.Buffer
	// panic: gob: type not registered for interface: elliptic.p256Curve
	gob.Register(elliptic.P256())
	encoder := gob.NewEncoder(&buf)
	err := encoder.Encode(ws)
	if err != nil {
		log.Fatal(err)
	}
	os.WriteFile(walletFile, buf.Bytes(), 0600)
}

// 从文件里读取所有钱包的实例
func (ws *Wallets) loadFile() {
	// 如果文件不存在，则直接退出
	if !utils.Exists(walletFile) {
		return
	}
	// 读取文件
	content, err := os.ReadFile(walletFile)
	if err != nil {
		log.Fatal(err)
	}
	// 解码
	var wallets Wallets
	// panic: gob: type not registered for interface: elliptic.p256Curve
	gob.Register(elliptic.P256())
	decoder := gob.NewDecoder(bytes.NewReader(content))
	err = decoder.Decode(&wallets)
	if err != nil {
		log.Fatal(err)
	}
	ws.WalletsMap = wallets.WalletsMap
}

func (ws *Wallets) GetAddresses() []string {
	var addresses []string
	for address := range ws.WalletsMap {
		addresses = append(addresses, address)
	}
	return addresses
}

func GetPubKeyFromAddress(address string) []byte {
	// 1.解码
	// 2.截取出公钥哈希：去除version（1字节），去除校验码（4字节）
	addressByte := base58.Decode(address) //25字节
	return addressByte[1 : len(addressByte)-4]
}
