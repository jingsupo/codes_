from celery import Celery

app = Celery('go_tasks', broker='redis://127.0.0.1:6379')

app.conf.update(
    # CELERY_TASK_SERIALIZER='json',
    task_serializer='json',
    # CELERY_ACCEPT_CONTENT=['json'],
    accept_content=['json'],
    # CELERY_RESULT_SERIALIZER='json',
    result_serializer='json',
    CELERY_ENABLE_UTC=True,
    # CELERY_TASK_PROTOCOL=1,
    task_protocol=1,
)


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # 每5秒调度一次，一亿减到一，不过不跑python worker
    # 由于go worker在运行，这里的minus会被go worker消费
    sender.add_periodic_task(5.0, minus.s())


@app.task
def minus():
    # 这里的minus函数实际上只是为了能被识别到而编写的，
    # 其内容毫无意义，直接写个pass都没问题（因为实际上是go worker在消费）。
    x = 100000000
    while x > 1:
        x -= 1

# 启动go worker: go run main.go
# 启动python worker: celery -A go_tasks worker -l info --pool=eventlet
# 启动beat调度器: celery -A go_tasks beat
