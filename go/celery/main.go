package main

import (
	"fmt"
	"github.com/gocelery/gocelery"
	"github.com/gomodule/redigo/redis"
	"time"
)

func minus() {
	start := time.Now()
	decrement(100000000)
	fmt.Println(time.Since(start))
}

func decrement(n int) {
	for n > 0 {
		n -= 1
	}
}

func main() {
	//create redis connection pool
	redisPool := &redis.Pool{
		MaxIdle:     3,
		MaxActive:   0,
		IdleTimeout: 300 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.DialURL("redis://")
			if err != nil {
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}

	//initialize celery client
	cli, _ := gocelery.NewCeleryClient(
		gocelery.NewRedisBroker(redisPool),
		&gocelery.RedisCeleryBackend{Pool: redisPool},
		5,
	)

	//register task
	cli.Register("go_tasks.minus", minus)

	//start workers (non-blocking call)
	cli.StartWorker()

	//wait for client request
	time.Sleep(1000 * time.Second)

	//stop workers gracefully (blocking call)
	cli.StopWorker()
}
