package main

import "C"

import (
	"fmt"
	"strings"
	"unsafe"
)

//export addStr
func addStr(a, b *C.char) *C.char {
	add := C.GoString(a) + C.GoString(b)
	return C.CString(add)
}

//export array
func array(cArray *C.int, cSize C.int) *C.int {
	goSlice := (*[1 << 30]C.int)(unsafe.Pointer(cArray))[: cSize : cSize+1]
	size := C.int(666)
	goSlice = append(goSlice, size)
	fmt.Println(goSlice)
	goResult := [][]int{{123, 456}, {789, 666}, {888, 999}}
	cArr := C.malloc(C.size_t(len(goResult)) * C.size_t(unsafe.Sizeof(uintptr(0))))
	//convert the C array to a Go Array so we can index it
	arr := (*[1 << 30]*C.int)(cArr)
	for idx, value := range goResult {
		cArr2 := C.malloc(C.size_t(len(goResult[0])) * C.size_t(unsafe.Sizeof(uintptr(0))))
		arr2 := (*[1 << 30]C.int)(cArr2)
		for i, v := range value {
			arr2[i] = C.int(v)
		}
		arr[idx] = (*C.int)(cArr2)
	}
	//return (*C.int)(unsafe.Pointer(&goSlice[0]))
	return (*C.int)(cArr)
}

//export arrayNumber
func arrayNumber(val []C.long, size C.long) C.long {
	itemSize := unsafe.Sizeof(C.long(0))
	var sum int
	for i := 0; i < int(size); i++ {
		itemP := unsafe.Pointer(uintptr(unsafe.Pointer(&val)) + itemSize*uintptr(i))
		sum += int(*(*C.long)(itemP))
	}
	return C.long(sum)
}

//export arrayUnicode
func arrayUnicode(val []*C.char, size C.long) *C.char {
	itemSize := unsafe.Sizeof(C.CString(""))
	res := make([]string, 0)
	for i := 0; i < int(size); i++ {
		itemP := unsafe.Pointer(uintptr(unsafe.Pointer(&val)) + itemSize*uintptr(i))
		res = append(res, C.GoString(*(**C.char)(itemP)))
	}
	return C.CString(strings.Join(res, "--"))
}

//export arrayUnicodePointer
func arrayUnicodePointer(val *[]**C.char, size C.long) *C.char {
	//接收一个数组指针，数组里面存的是*C.char的指针
	itemSize := uintptr(8)
	res := make([]string, 0)
	//获取指针指向的值
	_val := *val
	for i := 0; i < int(size); i++ {
		itemP := unsafe.Pointer(uintptr(unsafe.Pointer(&_val)) + itemSize*uintptr(i))
		res = append(res, C.GoString(**(***C.char)(itemP)))
	}
	return C.CString(strings.Join(res, "--"))
}

//export multiRes
func multiRes(v1 *C.int, v2 **C.char) {
	*v1 = 2021
	*v2 = C.CString("hello world")
}

func main() {}
