from ctypes import CDLL, c_char_p, c_int, POINTER, pointer

lib = CDLL("./main.so")

lib.addStr.argtypes = [c_char_p, c_char_p]
lib.addStr.restype = c_char_p
print(lib.addStr("I love you, ".encode("utf-8"), "Han han.".encode("utf-8")).decode("utf-8"))

lib.array.restype = POINTER(POINTER(c_int))
pyarr = [x for x in range(256)]
arr = (c_int * len(pyarr))(*pyarr)
result = lib.array(arr, len(arr))
result = [result[i][j] for i in range(3) for j in range(2)]
print(result)

v1 = (c_int * 4)(83, 10, 84, 6)
print(lib.arrayNumber(v1, 4))

lib.arrayUnicode.restype = c_char_p
v2 = (c_char_p * 3)(c_char_p("曾经".encode("utf-8")),
                    c_char_p("沧海".encode("utf-8")),
                    c_char_p("难为水".encode("utf-8")))
print(lib.arrayUnicode(v2, 3).decode("utf-8"))

lib.arrayUnicodePointer.restype = c_char_p
v3 = (type(pointer(c_char_p(b""))) * 3)(pointer(c_char_p("我永远".encode("utf-8"))),
                                        pointer(c_char_p("喜欢".encode("utf-8"))),
                                        pointer(c_char_p("芳芳".encode("utf-8")))
                                        )
print(lib.arrayUnicodePointer(pointer(v3), 3).decode("utf-8"))

a = c_int(0)
b = c_char_p(b"")
lib.multiRes.restype = None
lib.multiRes(pointer(a), pointer(b))
print(a.value)
print(b.value.decode("utf-8"))
