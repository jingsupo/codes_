package main

import (
	"fmt"
	"image"
	"math"

	"gocv.io/x/gocv"
)

func divide(cvMat1, cvMat2 gocv.Mat) gocv.Mat {
	cvMatBytes1 := cvMat1.ToBytes()
	cvMatBytes2 := cvMat2.ToBytes()
	var resBytes []byte
	for i := 0; i < len(cvMatBytes1); i++ {
		val := float64(cvMatBytes1[i]) / float64(cvMatBytes2[i]) * 255
		if val > 255 {
			val = 255
		}
		resBytes = append(resBytes, byte(math.Round(val)))
	}
	result, _ := gocv.NewMatFromBytes(cvMat1.Rows(), cvMat1.Cols(), gocv.MatTypeCV8U, resBytes)
	return result
}

func CvtImg(imgPath string, outPath string) {
	//读取图像
	img := gocv.IMRead(imgPath, gocv.IMReadColor)
	if img.Empty() {
		fmt.Println("图像读取错误")
		return
	}
	//灰度化
	gray := gocv.NewMat()
	gocv.CvtColor(img, &gray, gocv.ColorBGRToGray)
	//取反
	grayInv := gocv.NewMat()
	gocv.BitwiseNot(gray, &grayInv)
	//高斯滤波
	blur := gocv.NewMat()
	gocv.GaussianBlur(grayInv, &blur, image.Point{X: 15, Y: 15}, 50, 50, gocv.BorderDefault)
	//取反
	blurInv := gocv.NewMat()
	gocv.BitwiseNot(blur, &blurInv)
	//颜色减淡混合
	div := divide(gray, blurInv)
	//保存图像
	gocv.IMWrite(outPath, div)
}

func main() {
	CvtImg(`D:\Pictures\mm.png`, "out.png")
}
