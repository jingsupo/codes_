package main

import (
	"fmt"
	"gitee.com/jingsupo/goutils/utils"
	"gonum.org/v1/gonum/floats/scalar"
	"log"
	"os"
	"strconv"
)

func main() {
	args := os.Args
	if len(args) < 2 {
		log.Fatal("请输入文件路径")
	}
	nums := utils.ReadLines(args[1])
	var sum float64
	for _, value := range nums {
		num, _ := strconv.ParseFloat(value, 64)
		sum += num
	}
	avg := scalar.Round(sum/float64(len(nums)), 3)
	fmt.Printf("计数：%v\t求和：%v\t平均：%v", len(nums), sum, avg)
}
