package main

import (
	"fmt"
	"gitee.com/jingsupo/goutils/utils"
	"io"
	"net/http"
	"os"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

var (
	//邮箱
	reEmail = `\w+@\w+\.\w+(\.\w+)?`
	//链接
	reLink = `href="(https?://[\s\S]+?)"`
	//手机
	rePhone = `1[3456789]\d\s?\d{4}\s?\d{4}`
	//身份证
	//410222 1987 06 13 4038
	reIdcard = `[12345678]\d{5}((19\d{2})|(20[01]))((0[1-9]|[1[012]]))((0[1-9])|[12]\d|[3[01]])\d{3}[\dXx]`
	//图片
	reImage = `"(https?://[^"]+?(\.((jpg)|(jpeg)|(png)|(gif)|(ico))))"`
)

//异常处理
func handleError(err error, why string) {
	if err != nil {
		fmt.Println(why, err)
	}
}

//获取url页面内容
func GetPageByte(url string) []byte {
	//发送请求
	resp, err := http.Get(url)
	handleError(err, "http.Get url")
	//捕获panic错误并recover
	defer func() {
		if err := recover(); err != nil {
			switch err.(type) {
			case runtime.Error: //运行时错误
				fmt.Println(err)
			default: //非运行时错误
				fmt.Println("error: ", err)
			}
		}
	}()
	//关闭资源
	defer resp.Body.Close()
	//读取响应内容
	buf, err := io.ReadAll(resp.Body)
	handleError(err, "io.ReadAll resp.Body")
	return buf
}

//获取url页面中的正则匹配结果
func GetResult(url string, pattern string) (ret []string) {
	pageStr := string(GetPageByte(url))
	re := regexp.MustCompile(pattern)
	results := re.FindAllStringSubmatch(pageStr, -1)
	fmt.Printf("找到%d条结果:\n", len(results))
	for _, result := range results {
		ret = append(ret, result[1])
	}
	return
}

var (
	//保存图片链接的channel
	imageChan chan string
	//保存监控任务的channel
	taskChan chan string
	wg       sync.WaitGroup
)

//任务数
const taskNum = 50

//获取当前页所有图片链接并添加到channel中
func getImages(url string) {
	//获取图片链接
	urls := GetResult(url, reImage)
	//添加到channel中
	for _, url := range urls {
		imageChan <- url
	}
	//标识当前协程任务完成
	taskChan <- url
	wg.Done()
}

//下载文件
func downloadFile(url string, filename string) bool {
	//获取url页面内容
	buf := GetPageByte(url)
	if len(buf) != 0 {
		//文件保存目录
		saveDir := "./img/"
		if !utils.Exists(saveDir) {
			err := os.Mkdir(saveDir, os.ModePerm)
			if err != nil {
				fmt.Println(err)
			}
		}
		//拼接文件完整路径
		filepath := saveDir + filename
		//写入磁盘
		err := os.WriteFile(filepath, buf, 644)
		handleError(err, "os.WriteFile")
		if err != nil {
			return false
		} else {
			return true
		}
	}
	return false
}

//下载图片
func downloadImage() {
	for url := range imageChan {
		lastIndex := strings.LastIndex(url, "/")
		filename := url[lastIndex+1:]
		timePrefix := strconv.Itoa(int(time.Now().UnixNano()))
		filename = timePrefix + "_" + filename
		ok := downloadFile(url, filename)
		if ok {
			fmt.Printf("%s 下载成功\n", filename)
		} else {
			fmt.Printf("%s 下载失败\n", filename)
		}
	}
	wg.Done()
}

//监控所有任务是否完成，完成则关闭channel
func taskCheck() {
	//计数
	var count int
	for {
		url := <-taskChan
		fmt.Printf("%s 完成爬取任务\n", url)
		count++
		if count == taskNum {
			close(imageChan)
			break
		}
	}
	wg.Done()
}

func main() {
	//初始化数据channel
	imageChan = make(chan string, 1000000)
	taskChan = make(chan string, taskNum)
	//开启图片链接爬取协程
	for i := 1; i < taskNum+1; i++ {
		wg.Add(1)
		go getImages("https://www.umei.net/meinvtupian/xingganmeinv/index_" + strconv.Itoa(i) + ".htm")
	}
	//开启任务监控协程
	wg.Add(1)
	go taskCheck()
	//开启图片下载协程
	for i := 0; i < 5; i++ {
		wg.Add(1)
		go downloadImage()
	}
	wg.Wait()
}
