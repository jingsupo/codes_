package main

import (
	"gitee.com/jingsupo/goutils/utils"
	"log"
	"os"
	"path/filepath"
)

func removeAll(path string) {
	files, err := os.ReadDir(path)
	if err != nil {
		log.Println(err)
		return
	}
	for _, file := range files {
		err := os.RemoveAll(filepath.Join(path, file.Name()))
		if err != nil {
			log.Println(err)
			continue
		}
		log.Printf("文件\t%s\t删除成功\n", file.Name())
	}
}

func main() {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Println(err)
	}
	paths := utils.ReadLines(filepath.Join(dir, "dc.conf"))
	for _, path := range paths {
		removeAll(path)
	}
}
