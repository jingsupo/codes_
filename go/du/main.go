package main

import (
	"flag"
	"fmt"
	"gitee.com/jingsupo/goutils/utils"
	"os"
	"path/filepath"
	"strconv"
)

func noFile(p string) {
	if !utils.Exists(p) {
		fmt.Println(p, "\tis not exists")
		os.Exit(1)
	}
}

//定义显示样式
func autoDisplay(num float64) string {
	if num < 1024 {
		return strconv.FormatFloat(num, 'f', 2, 64) + "B"
	} else if num < 1024*1024 {
		return strconv.FormatFloat(num/1024, 'f', 2, 64) + "K"
	} else if num < 1024*1024*1024 {
		return strconv.FormatFloat(num/1024/1024, 'f', 2, 64) + "M"
	} else {
		return strconv.FormatFloat(num/1024/1024/1024, 'f', 2, 64) + "G"
	}
}

//统计目录大小
func getSize(p string) (int64, error) {
	var size int64
	err := filepath.Walk(p, func(_ string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			size += info.Size()
		}
		return err
	})
	return size, err
}

func main() {
	flag.Parse()
	args := flag.Args()

	for _, p := range args {
		fmt.Println("============================================================")
		noFile(p)
		//获取文件信息
		s, err := os.Stat(p)
		if err != nil {
			fmt.Println(err)
		} else if s.IsDir() { //文件夹
			var sum int64 = 0
			entries, err := os.ReadDir(p)
			if err != nil {
				fmt.Println(err)
			}
			for _, entry := range entries {
                info, err := entry.Info()
                if err != nil {
                    fmt.Println(err)
                }
				if info.IsDir() {
					subDir := filepath.Join(p, info.Name())
					size, _ := getSize(subDir)
					fmt.Println(info.Name(), "\t", autoDisplay(float64(size)))
					sum += size
				} else {
					sum += info.Size()
				}
			}
			fmt.Println("**********")
			fmt.Println("汇总", "\t", autoDisplay(float64(sum)))
		} else { //文件
			size := s.Size()
			fmt.Println(p, "\t", autoDisplay(float64(size)))
		}
	}
}
