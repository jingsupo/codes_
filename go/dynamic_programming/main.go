package main

import (
	"fmt"
	"gonum.org/v1/gonum/mat"
	"log"
	"math"
	"math/rand"
	"time"
)

func RandInt(min, max int) int {
	if min <= 0 || max <= 0 {
		log.Fatal("min and max must > 0")
	} else if min >= max {
		log.Fatal("max must > min")
	}
	return rand.Intn(max-min) + min
}

//DP算法（动态规划）初试
//
//假设有一个6 * 6的棋盘，每个格子里面有一个奖品（每个奖品的价值在100到1000之间），现在要求从左上角开始到右下角结束，
//每次只能往右或往下走一个格子，所经过的格子里的奖品归自己所有。问最多能收集价值多少的奖品。
//DP算法适用于前一步的决策影响后一步决策的问题。
//本题右下角格子的决策取决于其左边和上面的最优决策，因此，右下角a[i][j]只需要取max(a[i-1][j], a[i][j-1]) + a[i][j]；
//其余部分只受左边或者上面的决策影响，因此，横向的a[i][j]应该取a[i][j-1] + a[i][j], 纵向的a[i][j]应该取a[i-1][j] + a[i][j]。
func DynamicProgramming() {
	rand.Seed(time.Now().UnixNano())

	arr := mat.NewDense(6, 6, nil)
	for i := 0; i < 6; i++ {
		for j := 0; j < 6; j++ {
			arr.Set(i, j, float64(RandInt(100, 1000)))
		}
	}
	fmt.Println("随机生成一个6*6的二维数组做为棋盘中的权值：")
	fmt.Println(arr)

	//首先将第一行和第一列的格子向右或向下累加，除了第一个格子，其余格子的值为前一个格子的值加当前格子的值
	for i := 1; i < 6; i++ {
		arr.Set(0, i, arr.At(0, i-1)+arr.At(0, i))
		arr.Set(i, 0, arr.At(i-1, 0)+arr.At(i, 0))
	}
	//计算每个格子的最大值
	for i := 1; i < 6; i++ {
		for j := 1; j < 6; j++ {
			arr.Set(i, j, math.Max(arr.At(i-1, j), arr.At(i, j-1))+arr.At(i, j))
		}
	}
	fmt.Println("变化后的数组：")
	fmt.Println(arr)
}

func main() {
	DynamicProgramming()
}
