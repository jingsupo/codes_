package main

import (
	"bufio"
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"

	"gitee.com/jingsupo/goutils/utils"
)

func main() {
	args := os.Args
	if len(args) < 2 {
		log.Fatal("请输入文件夹路径")
	}
	output := utils.GetNow(utils.TimeLayout2) + ".csv"
	path := args[1]
	files, err := os.ReadDir(path)
	if err != nil {
		log.Println(err)
		return
	}
	for i, file := range files {
		func(file fs.DirEntry) {
			fp := filepath.Join(path, file.Name())
			fmt.Println(fp)
			f, err := os.Open(fp)
			if err != nil {
				panic(err)
			}
			defer f.Close()
			scanner := bufio.NewScanner(f)
			scanner.Split(bufio.ScanLines)
			var lines []string
			count := 0
			for scanner.Scan() {
				if scanner.Text() != "" {
					count++
					if (i != 0) && (count == 1) {
						continue
					}
					lines = append(lines, scanner.Text()+"\n")
				}
			}
			utils.WriteLines(output, lines)
		}(file)
	}
}
