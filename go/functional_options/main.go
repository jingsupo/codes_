package main

import (
	"crypto/tls"
	"fmt"
	"time"
)

type Server struct {
	Addr     string        // required
	Port     int           // required
	Protocol string        // not null
	Timeout  time.Duration // not null
	MaxConn  int           // not null
	TLS      *tls.Config   //
}

type Option func(*Server)

func Protocol(p string) Option {
	return func(s *Server) {
		s.Protocol = p
	}
}
func Timeout(timeout time.Duration) Option {
	return func(s *Server) {
		s.Timeout = timeout
	}
}
func MaxConn(maxConn int) Option {
	return func(s *Server) {
		s.MaxConn = maxConn
	}
}
func TLS(tls *tls.Config) Option {
	return func(s *Server) {
		s.TLS = tls
	}
}

func NewServerFP(addr string, port int, options ...Option) (*Server, error) {
	srv := Server{
		Addr:     addr,
		Port:     port,
		Protocol: "tcp",
		Timeout:  30 * time.Second,
		MaxConn:  1000,
		TLS:      nil,
	}
	for _, option := range options {
		option(&srv)
	}
	return &srv, nil
}

func main() {
	s1, _ := NewServerFP("localhost", 8080)
	s2, _ := NewServerFP("localhost", 8080, Protocol("udp"))
	s3, _ := NewServerFP("0.0.0.0", 8080, Timeout(300*time.Second), MaxConn(1000))
	fmt.Println(s1, s2, s3)
}
