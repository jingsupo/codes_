package main

import (
	"container/heap"
	"fmt"
)

type IntHeap struct {
	data []int
	mode string //模式：可选范围["max", "min"]
}

func (h IntHeap) Len() int {
	return len(h.data)
}

func (h IntHeap) Less(i, j int) bool {
	if h.mode == "max" {
		return h.data[i] > h.data[j]
	} else if h.mode == "min" {
		return h.data[i] < h.data[j]
	}
	return false
}

func (h IntHeap) Swap(i, j int) {
	h.data[i], h.data[j] = h.data[j], h.data[i]
}

func (h *IntHeap) Push(x any) {
	// Push and Pop use pointer receivers because they modify the slice's length,
	// not just its contents.
	h.data = append(h.data, x.(int))
}

func (h *IntHeap) Pop() any {
	old := *h
	n := len(old.data)
	x := old.data[n-1]
	h.data = old.data[:n-1]
	return x
}

func (h *IntHeap) New(arr []int, mode string) *IntHeap {
	if mode == "max" || mode == "min" {
		h.data = arr
		h.mode = mode
		return h
	}
	return nil
}

func TopKIntHeap(arr []int, k int, mode string) (topk []int) {
	h := new(IntHeap).New(arr, mode)
	if h != nil {
		heap.Init(h)
		fmt.Printf("%s: %d\n", mode, h.data[0])
		for i := 0; i < k; i++ {
			if h.Len() > 0 {
				topk = append(topk, heap.Pop(h).(int))
			}
		}
	} else {
		fmt.Println("nil pointer")
	}
	return topk
}

func main() {
	arr := []int{2, 4, 7, 4, 5, 1, 6, 8, 7, 2, 8}
	var topk []int
	topk = TopKIntHeap(arr, 5, "max")
	fmt.Printf("topk: %v\n", topk)
	topk = TopKIntHeap(arr, 5, "min")
	fmt.Printf("topk: %v\n", topk)
	topk = TopKIntHeap(arr, 7, "max")
	fmt.Printf("topk: %v\n", topk)
	topk = TopKIntHeap(arr, 7, "min")
	fmt.Printf("topk: %v\n", topk)
	topk = TopKIntHeap(arr, 7, "xxx")
	fmt.Printf("topk: %v\n", topk)
}
