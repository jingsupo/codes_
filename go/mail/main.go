package main

import (
	"crypto/tls"
	"fmt"
	"gitee.com/jingsupo/goutils/utils"
	"github.com/spf13/viper"
	"gopkg.in/mail.v2"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

var currentTime = time.Now().Format("2006-01-02")

func initViper() {
	if err := utils.InitViper(""); err != nil {
		log.Fatal(err)
	}
}

func ReadLines(name string, sep string) string {
	lines := utils.ReadLines(name)
	return strings.Join(lines, sep)
}

func SendMail(message string) {
	//初始化配置文件
	initViper()
	//发送邮件
	m := mail.NewMessage()
	m.SetHeader("From", viper.GetString("USERNAME"))
	m.SetHeader("To", viper.GetString("TO"))
	m.SetHeader("Subject", viper.GetString("SUBJECT")+currentTime)
	m.SetBody("text/html", message)
	d := mail.NewDialer(
		viper.GetString("HOST"),
		viper.GetInt("PORT"),
		viper.GetString("USERNAME"),
		os.Getenv("MAIL_AUTH"),
	)
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	if err := d.DialAndSend(m); err != nil {
		log.Fatal(err)
	}
	fmt.Println("邮件发送成功...")
}

func main() {
	args := os.Args
	if len(args) < 2 {
		log.Fatal("请输入文件路径")
	}
	fmt.Println(currentTime)
	wd, _ := os.Getwd()
	content := ReadLines(filepath.Join(wd, args[1]), "<br/>")
	tmpl := ReadLines(filepath.Join(wd, "conf/tmpl.html"), "")
	pattern := `<div id="content">([\s\S]*?)</div>`
	re := regexp.MustCompile(pattern)
	message := re.ReplaceAllString(tmpl, fmt.Sprintf(`<div id="content">%s</div>`, content))
	SendMail(message)
}
