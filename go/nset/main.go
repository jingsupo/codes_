package main

import (
	"fmt"

	"gonum.org/v1/gonum/floats"
	"gonum.org/v1/gonum/mat"
)

func cdist(a, b *mat.Dense) *mat.Dense {
	ar, _ := a.Dims()
	br, _ := b.Dims()
	m := mat.NewDense(ar, br, nil)
	for i := 0; i < ar; i++ {
		for j := 0; j < br; j++ {
			m.Set(i, j, floats.Distance(a.RawRowView(i), b.RawRowView(j), 2))
		}
	}
	return m
}

func nset(matrix_d *mat.Dense, obs_vector *mat.Dense) *mat.Dense {
	dist_d_d := cdist(matrix_d, matrix_d)
	var dist_d_d_inv mat.Dense
	dist_d_d_inv.Inverse(dist_d_d)
	var d_dot_dist_d_d_inv mat.Dense
	d_dot_dist_d_d_inv.Mul(matrix_d.T(), &dist_d_d_inv)
	obs_vector_row, obs_vector_col := obs_vector.Dims()
	est_vector := mat.NewDense(obs_vector_row, obs_vector_col, nil)
	matrix_d_row, _ := matrix_d.Dims()
	dist_d_obs := mat.NewDense(matrix_d_row, 1, nil)
	for i := 0; i < obs_vector_row; i++ {
		for j := 0; j < matrix_d_row; j++ {
			var d_sub_obs mat.Dense
			d_sub_obs.Sub(matrix_d.RowView(j), obs_vector.RowView(i))
			dist_d_obs.Set(j, 0, d_sub_obs.Norm(2))
		}
		var est_value mat.Dense
		est_value.Mul(&d_dot_dist_d_d_inv, dist_d_obs)
		est_value_t := mat.DenseCopyOf(est_value.T())
		est_vector.SetRow(i, est_value_t.RawMatrix().Data)
	}
	return est_vector
}

func main() {
	matrix_d := mat.NewDense(4, 3, []float64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12})
	obs_vector := mat.NewDense(2, 3, []float64{1, 3, 5, 2, 4, 6})
	est_vector := nset(matrix_d, obs_vector)
	fmt.Println(est_vector)
}
