package main

import "fmt"

func Combine(arr []int, k int) [][]int {
	var res [][]int
	dfs(arr, k, 0, []int{}, &res)
	return res
}

func dfs(graph []int, target int, index int, path []int, paths *[][]int) {
	if len(path) == target {
		tmp := make([]int, target)
		copy(tmp, path)
		*paths = append(*paths, tmp)
		return
	}
	for i, v := range graph {
		if i < index {
			continue
		}
		dfs(graph, target, index+1, append(path, v), paths)
		index++
	}
}

func Permute(arr []int) [][]int {
	var res [][]int
	toNext(arr, 0, &res)
	return res
}

func toNext(arr []int, n int, res *[][]int) {
	if n == len(arr)-1 {
		tmp := make([]int, len(arr))
		copy(tmp, arr)
		*res = append(*res, tmp)
	}
	for i := n; i < len(arr); i++ {
		arr[i], arr[n] = arr[n], arr[i]
		toNext(arr, n+1, res)
		arr[i], arr[n] = arr[n], arr[i]
	}
}

func Permutations(arr []int, k int) [][]int {
	var res [][]int
	_res := Combine(arr, k)
	for _, v := range _res {
		res = append(res, Permute(v)...)
	}
	return res
}

func main() {
	arr := []int{3, 4, 6, 10, 15, 17}
	fmt.Println(Permutations(arr, 6))
}
