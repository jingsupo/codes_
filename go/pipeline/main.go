package main

import "log"

func main() {
	checkpoint := loadCheckpoint()

	//工序(1)在pipeline外执行，最后一个工序是保存checkpoint
	pipeline := NewPipeline(8, 32, 2, 1)
	for {
		//(1)
		//加载100条数据，并修改变量checkpoint
		//data是数组，每个元素是一条评论，之后的联表、NLP都直接修改data里的每条记录。
		data, err := extractReviewsFromA(&checkpoint, 100)
		if err != nil {
			log.Print(err)
			break
		}
		curCheckpoint := checkpoint

		ok := pipeline.Async(func() error {
			//(2)
			return joinUserFromB(data)
		}, func() error {
			//(3)
			return nlp(data)
		}, func() error {
			//(4)
			return loadDataToC(data)
		}, func() error {
			//(5)保存checkpoint
			log.Print("done:", curCheckpoint)
			return saveCheckpoint(curCheckpoint)
		})
		if !ok {
			break
		}

		if len(data) < 100 {
			break
		} //处理完毕
	}
	err := pipeline.Wait()
	if err != nil {
		log.Print(err)
	}
}
