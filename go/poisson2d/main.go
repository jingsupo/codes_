package main

import (
	"fmt"
	"math"
	"time"
)

const M = 300

func findMaxAbs(x, y *[M][M]float64) float64 {
	var max, diff float64
	for i := 0; i < M; i++ {
		for j := 0; j < M; j++ {
			diff = math.Abs(x[i][j] - y[i][j])
			if diff > max {
				max = diff
			}
		}
	}
	return max
}

func rho(x, y float64) float64 {
	if (x > 0.6 && x < 0.8) && (y > 0.6 && y < 0.8) {
		return 1.0
	} else if (x > 0.2 && x < 0.4) && (y > 0.2 && y < 0.4) {
		return -1.0
	} else {
		return 0.0
	}
}

func run() {
	const eps, toler, a float64 = 8.85e-12, 1.0e-6, 0.01

	var phi, phiPrime, rhoArr [M][M]float64

	for i := 0; i < M; i++ {
		for j := 0; j < M; j++ {
			rhoArr[i][j] = rho(float64(i)*a, float64(j)*a)
		}
	}

	var iter int
	var delta float64 = 1.0
	var a2 float64 = a * a

	for delta > toler {
		iter += 1
		for i := 1; i < M-1; i++ {
			for j := 1; j < M-1; j++ {
				phiPrime[i][j] = (phi[i+1][j]+phi[i-1][j]+phi[i][j+1]+phi[i][j-1])/4 + a2/4/eps*rhoArr[i][j]
			}
		}
		delta = findMaxAbs(&phi, &phiPrime)
		phi = phiPrime
	}
	fmt.Printf("iters: %d\n", iter)
}

func main() {
	startTime := time.Now()
	run()
	elapsed := time.Since(startTime)
	fmt.Printf("Execution time: %v\n", elapsed)
}
