package main

import (
	"fmt"
	"gonum.org/v1/gonum/floats"
	"gonum.org/v1/gonum/mat"
)

//搜索矩阵中连续为1最多的区域的元素个数
//连续的定义：上下左右相邻
type Matrix struct {
	Mat  *mat.Dense
	elem [][]int
}

func (m *Matrix) reset(row int, col int) {
	r, c := m.Mat.Dims()
	//置为0
	m.Mat.Set(row, col, 0)
	//保存当前元素的行列索引
	m.elem = append(m.elem, []int{row, col})
	//上
	if row-1 >= 0 && m.Mat.At(row-1, col) == 1 {
		m.reset(row-1, col)
	}
	//下
	if row+1 <= r-1 && m.Mat.At(row+1, col) == 1 {
		m.reset(row+1, col)
	}
	//左
	if col-1 >= 0 && m.Mat.At(row, col-1) == 1 {
		m.reset(row, col-1)
	}
	//右
	if col+1 <= c-1 && m.Mat.At(row, col+1) == 1 {
		m.reset(row, col+1)
	}
}

func (m *Matrix) search() (num int, res [][][]int) {
	r, c := m.Mat.Dims()
	//连续为1的区域的数量
	num = 0
	for i := 0; i < r; i++ {
		for j := 0; j < c; j++ {
			if m.Mat.At(i, j) == 1 {
				m.reset(i, j)
				res = append(res, m.elem)
				//清空列表以供下个区域使用
				m.elem = [][]int{}
				num += 1
			}
		}
	}
	return
}

func (m *Matrix) Run() {
	blockNum, result := m.search()
	var count []float64
	for _, block := range result {
		count = append(count, float64(len(block)))
	}
	maxCount := floats.Max(count)
	fmt.Printf("连续为1的区域的数量为：%d\n", blockNum)
	fmt.Printf("每个连续为1的区域内的元素的行列索引为：\n%v\n", result)
	fmt.Printf("连续为1的区域内元素个数最多为：%f\n", maxCount)
}

func Demo() {
	arr := []float64{
		1, 0, 0, 1, 0,
		1, 0, 1, 0, 0,
		0, 0, 1, 0, 1,
		1, 0, 1, 0, 1,
		1, 0, 1, 1, 0,
	}
	d := mat.NewDense(5, 5, arr)
	s := new(Matrix)
	s.Mat = d
	s.Run()
}

func main() {
	Demo()
}
