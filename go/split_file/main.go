package main

import (
	"bufio"
	"fmt"
	"gitee.com/jingsupo/goutils/utils"
	"hash/crc32"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"sync"
	"time"
)

var (
	num        = 100
	fileList   []*os.File
	writerList []*bufio.Writer
	valueChan  []chan string
	wg         sync.WaitGroup
)

func Init(outputPath string) {
	if !utils.Exists(outputPath) {
		os.MkdirAll(outputPath, 0666)
	}
	for i := 0; i < num; i++ {
		name := filepath.Join(outputPath, strconv.Itoa(i)+".txt")
		file, err := os.OpenFile(name, os.O_RDWR|os.O_CREATE|os.O_TRUNC|os.O_APPEND, 0666)
		if err != nil {
			fmt.Println(err)
		}
		writer := bufio.NewWriter(file)
		writerList = append(writerList, writer)
		fileList = append(fileList, file)
		valueChan = append(valueChan, make(chan string, 1000000))
	}
}

func Read(inputFile string) {
	file, err := os.Open(inputFile)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		if scanner.Text() != "" {
			//h := crc64.Checksum([]byte(scanner.Text()), crc64.MakeTable(crc64.ISO))
			h := crc32.Checksum([]byte(scanner.Text()), crc32.MakeTable(crc32.IEEE))
			//m := h % uint64(num)
			m := h % uint32(num)
			valueChan[m] <- scanner.Text()
		}
	}
	for _, ch := range valueChan {
		close(ch)
	}
	wg.Done()
}

func Write(ch chan string, writer *bufio.Writer) {
	for value := range ch {
		writer.WriteString(value + "\n")
	}
	writer.Flush()
	wg.Done()
}

func SplitFile(inputFile, outputPath string) {
	runtime.GOMAXPROCS(runtime.NumCPU())
	//计时开始
	start := time.Now()
	//初始化
	fmt.Println("开始初始化...")
	Init(outputPath)
	fmt.Println("初始化完成")
	fmt.Println("开始分割文件...")
	//开启协程读取大文件
	wg.Add(1)
	go Read(inputFile)
	//开启协程写入小文件
	for i := 0; i < num; i++ {
		wg.Add(1)
		go Write(valueChan[i], writerList[i])
	}
	wg.Wait()
	//关闭文件
	for _, file := range fileList {
		file.Close()
	}
	fmt.Println("文件分割完成")
	//计时结束
	fmt.Printf("开始时间：%s\n", start.Format(time.Stamp))
	fmt.Printf("结束时间：%s\n", time.Now().Format(time.Stamp))
	fmt.Printf("程序运行时间：%s\n", time.Since(start))
}

func main() {
	//获取命令行参数
	args := os.Args
	if len(args) < 2 {
		fmt.Println("请输入文件路径和输出路径")
		return
	}
	if len(args) < 3 {
		fmt.Println("请输入输出路径")
		return
	}
	inputFile := args[1]
	outputPath := args[2]
	SplitFile(inputFile, outputPath)
}
