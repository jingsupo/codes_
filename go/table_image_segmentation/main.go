package main

import "C"

import (
	"fmt"
	"gitee.com/jingsupo/goutils/utils"
	"gocv.io/x/gocv"
	"gonum.org/v1/gonum/mat"
	"image"
	"math"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
)

var paramsMarginX = 10   //x轴点达到一定的距离，才算两个有效点，确定线的个数
var paramsMarginY = 10   //y轴点达到一定的距离，才算两个有效点，确定线的个数
var paramsDotMargin = 10 //和平均线的偏移量（对缝隙起作用，可去除点，也可变为独立一个点）
var paramsLineX = 10     //x上点个数的差值调节（线不均匀，有的粗有的细，甚至有的不连续）
var paramsLineY = 10     //y上点个数的差值调节（线不均匀，有的粗有的细，甚至有的不连续）

//获取数值大于0的像素点
func whereGreaterThan0(cvMat gocv.Mat) ([]int, []int) {
	size := cvMat.Size()
	cvBytes := cvMat.ToBytes()
	var data []float64
	for _, v := range cvBytes {
		data = append(data, float64(v))
	}
	cvDense := mat.NewDense(size[0], size[1], data)
	var x []int
	var y []int
	for i := 0; i < size[0]; i++ {
		for j := 0; j < size[1]; j++ {
			if cvDense.At(i, j) > 0 {
				y = append(y, i)
				x = append(x, j)
			}
		}
	}
	return y, x
}

//判断元素是否不在数组中
func notInSlice(arr []int, target int) bool {
	set := make(map[int]struct{})
	for _, v := range arr {
		set[v] = struct{}{}
	}
	if _, ok := set[target]; ok {
		return false
	} else {
		return true
	}
}

//判断数组是否不在数组中
func arrNotInSlice(arr [][2]int, target [2]int) bool {
	for _, v := range arr {
		if target[0] == v[0] && target[1] == v[1] {
			return false
		}
	}
	return true
}

func recognizeLine(lineXs []int, line1 []int, line2 []int, num, num1, num2 int) int {
	var lineList []int
	for _, k := range []int{-3, -2, -1, 0, 1, 2, 3} {
		for i := 0; i < len(lineXs); i++ {
			if line1[i] == num+k {
				if num1 <= line2[i] && line2[i] <= num2 && notInSlice(lineList, line2[i]) {
					lineList = append(lineList, line2[i])
				}
			}
		}
	}
	return len(lineList)
}

func getXY(coords []int, paramsMargin int) ([]int, []int) {
	var coordsList []int
	i := 0
	sortedCoords := make([]int, len(coords))
	copy(sortedCoords, coords)
	sort.Ints(sortedCoords)
	for ; i < len(sortedCoords)-1; i++ {
		if sortedCoords[i+1]-sortedCoords[i] > paramsMargin {
			coordsList = append(coordsList, sortedCoords[i])
		}
	}
	coordsList = append(coordsList, sortedCoords[i])
	return sortedCoords, coordsList
}

//export SplitImage
//func SplitImage(imgPath string, outPath string) [][4]int { //Go版本
func SplitImage(input *C.char, output *C.char) *C.char { //export版本
	//将C char类型转换为Go string类型
	imgPath := C.GoString(input)  //export版本
	outPath := C.GoString(output) //export版本

	dir, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
	}
	outPath = filepath.Join(dir, outPath)
	if !utils.Exists(outPath) {
		err := os.MkdirAll(outPath, os.ModePerm)
		if err != nil {
			fmt.Println(err)
		}
	}

	//读取图像
	img := gocv.IMRead(imgPath, gocv.IMReadColor)
	if img.Empty() {
		fmt.Println("图像读取错误")
		return nil
	}
	//灰度化
	gray := gocv.NewMat()
	gocv.CvtColor(img, &gray, gocv.ColorBGRToGray)
	//自适应阈值二值化
	binary := gocv.NewMat()
	_gray := gocv.NewMat()
	gocv.BitwiseNot(gray, &_gray) //图像按位取反
	gocv.AdaptiveThreshold(_gray, &binary, 255, gocv.AdaptiveThresholdGaussian, gocv.ThresholdBinary, 15, -10)
	gocv.IMWrite(outPath+"/"+"cell.jpg", binary)
	//识别横线
	eroded := gocv.NewMat()
	transverse := gocv.NewMat()
	kSize := image.Point{X: 25, Y: 1}
	kernel := gocv.GetStructuringElement(gocv.MorphRect, kSize)
	gocv.Erode(binary, &eroded, kernel)      //腐蚀
	gocv.Dilate(eroded, &transverse, kernel) //膨胀
	gocv.IMWrite(outPath+"/"+"dilated1.jpg", transverse)
	//识别竖线
	vertical := gocv.NewMat()
	kSize = image.Point{X: 1, Y: 25}
	kernel = gocv.GetStructuringElement(gocv.MorphRect, kSize)
	gocv.Erode(binary, &eroded, kernel)    //腐蚀
	gocv.Dilate(eroded, &vertical, kernel) //膨胀
	gocv.IMWrite(outPath+"/"+"dilated2.jpg", vertical)
	//标识交点
	bitwiseAnd := gocv.NewMat()
	gocv.BitwiseAnd(transverse, vertical, &bitwiseAnd)
	gocv.IMWrite(outPath+"/"+"bitwise_and.jpg", bitwiseAnd)
	//标识表格
	added := gocv.NewMat()
	gocv.Add(transverse, vertical, &added)
	gocv.IMWrite(outPath+"/"+"add.jpg", added)
	//识别表格中所有的线
	lineYs, lineXs := whereGreaterThan0(added)
	//识别黑白图中的白色点
	ys, xs := whereGreaterThan0(bitwiseAnd)

	sortedYs, listY := getXY(ys, paramsMarginY)
	_, listX := getXY(xs, paramsMarginX)
	fmt.Println("横向线数：", len(listY))
	fmt.Println("纵向线数：", len(listX))

	var coordsList [][2]int
	coordsMap := make(map[int][][2]int)
	for i := 0; i < len(sortedYs); i++ {
		for _, y := range listY {
			for _, x := range listX {
				if math.Abs(float64(y-ys[i])) < float64(paramsDotMargin) &&
					math.Abs(float64(x-xs[i])) < float64(paramsDotMargin) &&
					arrNotInSlice(coordsList, [2]int{y, x}) {
					coordsList = append(coordsList, [2]int{y, x})
				}
			}
		}
	}
	for i := 0; i < len(listY); i++ {
		var lineList [][2]int
		for _, coords := range coordsList {
			if coords[0] == listY[i] {
				lineList = append(lineList, coords)
			}
		}
		sort.Slice(lineList, func(i, j int) bool {
			return lineList[i][1] < lineList[j][1]
		})
		coordsMap[i] = lineList
	}

	var cellsCoords [][4]int //单元格坐标
	for i := 0; i < len(coordsMap)-1; i++ {
		for index, value := range coordsMap[i] {
			cellUp := value[0]   //单元格上边框
			cellLeft := value[1] //单元格左边框
			if index == len(coordsMap[i])-1 {
				break
			}
			for j := 1; j < len(coordsMap[i]); j++ {
				m := i
				n := index + j
				markNum := 0
				if n == len(coordsMap[i]) {
					break
				}
				cellDown := coordsMap[m+1][0][0] //单元格下边框
				cellRight := coordsMap[i][n][1]  //单元格右边框
				for m <= len(coordsMap)-2 {
					var tmp []int
					for _, v := range coordsMap[m+1] {
						tmp = append(tmp, v[1])
					}
					if !notInSlice(tmp, cellLeft) && !notInSlice(tmp, cellRight) &&
						math.Abs(float64(recognizeLine(lineXs, lineXs, lineYs, cellLeft, cellUp, cellDown)-(cellDown-cellUp))) <= float64(paramsLineY) &&
						math.Abs(float64(recognizeLine(lineXs, lineXs, lineYs, cellRight, cellUp, cellDown)-(cellDown-cellUp))) <= float64(paramsLineY) &&
						math.Abs(float64(recognizeLine(lineXs, lineYs, lineXs, cellUp, cellLeft, cellRight)-(cellRight-cellLeft))) <= float64(paramsLineX) &&
						math.Abs(float64(recognizeLine(lineXs, lineYs, lineXs, cellDown, cellLeft, cellRight)-(cellRight-cellLeft))) <= float64(paramsLineX) {
						markNum = 1
						roi1 := img.RowRange(cellUp, cellDown)
						roi2 := roi1.ColRange(cellLeft, cellRight)
						cellsCoords = append(cellsCoords, [4]int{cellUp, cellDown, cellLeft, cellRight})

						_imgName := filepath.Base(imgPath)
						imgName := strings.Split(_imgName, ".")[0]
						saveDir := filepath.Join(outPath, "img", imgName)
						if !utils.Exists(saveDir) {
							err := os.MkdirAll(saveDir, os.ModePerm)
							if err != nil {
								fmt.Println(err)
							}
						}
						newImgName := strconv.Itoa(cellUp) + "-" +
							strconv.Itoa(cellDown) + "-" +
							strconv.Itoa(cellLeft) + "-" +
							strconv.Itoa(cellRight) + ".jpg"
						savePath := filepath.Join(saveDir, newImgName)
						gocv.IMWrite(savePath, roi2)

						break
					} else {
						m += 1
					}
				}
				if markNum == 1 {
					break
				}
			}
		}
	}
	//将Go对象转换为C char类型
	var cells []string
	for _, cell := range cellsCoords {
		var coords []string
		for _, v := range cell {
			coords = append(coords, strconv.Itoa(v))
		}
		cells = append(cells, strings.Join(coords, ","))
	}
	result := C.CString(strings.Join(cells, " "))
	return result
}

func main() {
	//cc := SplitImage("D:\\Pictures\\images\\resume\\jl.jpeg", "output") //Go版本
	cc := SplitImage(C.CString("D:\\Pictures\\images\\resume\\jl.jpeg"), C.CString("output")) //export版本
	fmt.Println(cc)
}
