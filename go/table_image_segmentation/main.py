from ctypes import CDLL, c_char_p


def split_image(img_path, out_path, dll="./main.so"):
    lib = CDLL(dll)
    lib.SplitImage.restype = c_char_p
    result = lib.SplitImage(img_path, out_path)
    if result:
        result = result.decode("utf-8").split(' ')
        result = [[int(i) for i in r.split(',')] for r in result]
        return result


if __name__ == '__main__':
    img = "D:\\Pictures\\images\\resume\\jl.jpeg".encode("utf-8")
    out = "output".encode("utf-8")
    res = split_image(img, out)
    print(res)
