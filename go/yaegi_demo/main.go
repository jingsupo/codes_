package main

import (
	"fmt"

	"github.com/traefik/yaegi/interp"
	"github.com/traefik/yaegi/stdlib"
)

const src = `
package fib
func fibonacci(n int) int { if n <= 2 { return 1 }; return fibonacci(n-1) + fibonacci(n-2) };
`

func main() {
	{
		i := interp.New(interp.Options{})
		i.Use(stdlib.Symbols)
		_, err := i.Eval(src)
		if err != nil {
			panic(err)
		}
		v, err := i.Eval("fib.fibonacci")
		if err != nil {
			panic(err)
		}
		fib := v.Interface().(func(int) int)
		r := fib(30)
		fmt.Println("case 1==========")
		fmt.Println(r)
	}
	{
		i := interp.New(interp.Options{GoPath: "./_pkg"})
		err := i.Use(stdlib.Symbols)
		if err != nil {
			panic(err)
		}
		_, err = i.Eval(`import "_fib/fib"`)
		if err != nil {
			panic(err)
		}
		v, err := i.Eval("fib.Fib")
		if err != nil {
			panic(err)
		}
		fib := v.Interface().(func(int) int)
		r := fib(30)
		fmt.Println("case 2==========")
		fmt.Println(r)
	}
}
