# coding: utf-8

import matplotlib.pyplot as plt
import numpy as np
import torch
from torch.utils.data import DataLoader, TensorDataset


class AutoEncoder(object):
    """
    基于AE自编码器的异常检测
    """

    def __init__(self, train_data, test_data):
        self.X_train, self.X_test = torch.FloatTensor(train_data), torch.FloatTensor(test_data)

        self.train_set = TensorDataset(self.X_train)
        self.train_loader = DataLoader(dataset=self.train_set, batch_size=10, shuffle=True)

        input_size = self.X_train.size(1)

        self.model = torch.nn.Sequential(
            torch.nn.Linear(input_size, 6),
            torch.nn.Tanh(),
            torch.nn.Linear(6, 3),
            torch.nn.Tanh(),
            torch.nn.Linear(3, 6),
            torch.nn.Tanh(),
            torch.nn.Linear(6, input_size)
        )

    def get_recon_err(self, x):
        return torch.mean((self.model(x) - x) ** 2, dim=1).detach().numpy()

    def run(self):
        num_epochs = 10

        optimizer = torch.optim.Adam(self.model.parameters(), 0.001)
        loss_func = torch.nn.MSELoss()

        for epoch in range(num_epochs):
            total_loss = 0.0
            for step, (x,) in enumerate(self.train_loader):
                x_recon = self.model(x)
                loss = loss_func(x_recon, x)
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                total_loss += loss.item() * len(x)
            total_loss /= len(self.train_set)
            print(f'Epoch {epoch + 1}/{num_epochs} : loss: {total_loss:.4f}')

        recon_err_train = self.get_recon_err(self.X_train)
        recon_err_test = self.get_recon_err(self.X_test)
        recon_err = np.concatenate([recon_err_train, recon_err_test])

        plt.plot(recon_err)
        plt.show()
