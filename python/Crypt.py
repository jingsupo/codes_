# coding:utf-8

from Crypto.Cipher import AES
from binascii import b2a_hex, a2b_hex


class Crypt(object):

    def __init__(self, key):
        self.key = key.encode('utf-8')
        self.mode = AES.MODE_CBC

    def encrypt(self, text):
        """
        加密函数
        text如果不足16位就用空格补足为16位，如果大于16但不是16的倍数，那就补足为16的倍数。
        """
        # 每次加密必须使用新的AES对象，否则报错如下：
        # decrypt() cannot be called after encrypt()
        cryptor = AES.new(self.key, self.mode, b'0000000000000000')
        text = text.encode('utf-8')
        # 密钥key长度必须为16(AES-128)、24(AES-192)或者32(AES-256)Bytes
        length = 16
        count = len(text)
        if count < length:
            add = (length - count)
            # \0 backspace
            text = text + (b'\0' * add)
        elif count > length:
            add = (length - (count % length))
            text = text + (b'\0' * add)
        self.cipher_text = cryptor.encrypt(text)
        # 因为AES加密得到的字符串不一定是ASCII字符集的，输出到终端或者保存时可能存在问题，
        # 所以这里统一把加密后的字符串转化为16进制字符串。
        return b2a_hex(self.cipher_text)

    def decrypt(self, text):
        """
        解密函数
        解密后，用strip()去掉补足的空格。
        """
        # 每次加密必须使用新的AES对象，否则报错如下：
        # decrypt() cannot be called after encrypt()
        cryptor = AES.new(self.key, self.mode, b'0000000000000000')
        raw_text = cryptor.decrypt(a2b_hex(text))
        return raw_text.rstrip(b'\0')
