import numpy as np


"""熵权法TOPSIS"""


# 极小型->极大型
def min_to_max(x):
    # 方法1
    r = np.max(x) - x
    # 方法2
    r = 1 / np.array(x)
    return r


# 中间型->极大型
def mid_to_max(x, x_best):
    diff = abs(np.array(x) - x_best)
    r = 1 - diff / np.max(diff)
    return r


# 区间型->极大型
def intvl_to_max(x, x_min, x_max, x_minimum, x_maximum):
    # 方法1
    m = max(x_min - np.min(x), np.max(x) - x_max)
    r = []
    for i in x:
        if i < x_min:
            r.append(1 - (x_min - i) / m)
        elif i > x_max:
            r.append(1 - (i - x_max) / m)
        else:
            r.append(1)
    # 方法2
    r = []
    for i in x:
        if i >= x_min and i <= x_max:
            r.append(1)
        elif i <= x_minimum or i >= x_maximum:
            r.append(0)
        elif i > x_max and i < x_maximum:
            r.append(1 - (i - x_max) / (x_maximum - x_max))
        elif i < x_min and i > x_minimum:
            r.append(1 - (x_min - i) / (x_min - x_minimum))
    return np.array(r)


# 标准化
def standardize(x):
    x = np.array(x)
    k = np.sqrt((x**2).sum(axis=0))
    return x / k


# 熵权法计算权重
def entropy_weight(x):
    x = np.array(x)
    # 计算概率
    p = x / x.sum(axis=0)
    # 计算熵值
    e = np.nansum(-p * np.log(p) / np.log(len(x)), axis=0)
    # 计算信息效用值
    d = 1 - e
    # 计算权重
    w = d / d.sum()
    return w


def topsis(df, weight=None, norm_score=True):
    # 标准化处理
    x = standardize(df[df.columns[1:]].values)
    # 计算权重
    if weight is None:
        w = entropy_weight(x)
    else:
        w = weight
    # 计算加权后的矩阵
    x = w * x
    # 计算最优解和最劣解
    max_vec = np.max(x, axis=0)
    min_vec = np.min(x, axis=0)
    # 计算每个评价对象与最优解和最劣解的距离
    d_plus = np.sqrt(((max_vec - x)**2).sum(axis=1))
    d_minus = np.sqrt(((min_vec - x)**2).sum(axis=1))
    # 计算得分
    score = d_minus / (d_plus + d_minus)
    # 归一化处理
    if norm_score:
        # score = 100 * score / np.max(score)
        score = 100 * score / np.sum(score)
    return score
