# coding: utf-8

import random
import time


class LogGenerator(object):
    def __init__(self):
        # HTTP请求头User-Agent
        self.user_agent = {
            0.0: "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)",
            0.1: "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)",
            0.2: "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727)",
            0.3: "Mozilla/4.0 (compatible; MSIE6.0; Windows NT 5.0; .NET CLR 1.1.4322)",
            0.4: "Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko",
            0.5: "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36 Edg/89.0.774.68",
            0.6: "Mozilla/4.0 (compatible; MSIE6.0; Windows NT 5.0; .NET CLR 1.1.4322)",
            0.7: "Mozilla/5.0 (iPhone; CPU iPhone OS 7_0_3 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11B511 Safari/9537.53",
            0.8: "Mozilla/5.0 (Linux; Android 4.2.1; Galaxy Nexus Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19",
            0.9: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36",
            1: " ",
        }
        # URL访问列表
        self.url_path = [
            "class/112.html",
            "class/128.html",
            "class/145.html",
            "class/146.html",
            "class/500.html",
            "class/250.html",
            "class/131.html",
            "class/130.html",
            "class/271.html",
            "class/127.html",
            "learn/821",
            "learn/823",
            "learn/987",
            "learn/500",
            "course/list"
        ]
        # IP列表，随机选择4个数字作为IP
        self.ip_slice = [10, 29, 30, 46, 55, 63, 72, 87, 98, 132, 156, 124,
                         167, 143, 187, 168, 190, 201, 202, 214, 215, 222]
        # HTTP请求头Referer {query}代表搜索关键字
        self.http_referer = [
            "https://www.baidu.com/s?wd={query}",
            "https://www.sogou.com/web?query={query}",
            "https://cn.bing.com/search?q={query}",
            "https://search.yahoo.com/search?p={query}",
        ]
        # 搜索关键字列表
        self.search_keyword = [
            "Hadoop基础",
            "Spark SQL实战",
            "Spark Streaming实战",
            "10小时入门大数据",
            "Linux进阶",
        ]
        # 状态码列表
        self.status_code = ["200", "403", "404", "500"]

    # 随机产生一个User-Agent
    def sample_user_agent(self):
        return self.user_agent[float("%.1f" % random.uniform(0, 1))]

    # 随机选择一个URL
    def sample_url(self):
        return random.sample(self.url_path, 1)[0]

    # 随机组合一个IP
    def sample_ip(self):
        return ".".join([str(item) for item in random.sample(self.ip_slice, 4)])

    # 随机产生一个引用来源URL
    def sample_referer(self):
        # 有一半的概率会产生非法URL，用以模拟非法用户日志
        if random.uniform(0, 1) > 0.5:
            return "-"
        refer = random.sample(self.http_referer, 1)
        query = random.sample(self.search_keyword, 1)
        return refer[0].format(query=query[0])

    # 随机产生一个状态码
    def sample_status_code(self):
        return random.sample(self.status_code, 1)[0]

    # 组合一条用户访问日志
    def generate_log(self, count=10):
        # 获取本机时间并将其作为访问时间写入访问日志
        time_str = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        f = open("access.log", "a+")
        while count >= 1:
            query_log = "{local_time} | {status_code} | {ip} | \"GET /{url}\" | {referer} | \"{user_agent}\"".format(
                user_agent=self.sample_user_agent(),
                url=self.sample_url(),
                ip=self.sample_ip(),
                referer=self.sample_referer(),
                status_code=self.sample_status_code(),
                local_time=time_str)
            f.write(query_log + "\n")
            count -= 1
