# coding: utf-8

import matplotlib.pyplot as plt
import numpy as np
from scipy import linalg, stats

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False


class PCAFaultDetection(object):
    """
    PCA故障诊断
    """

    def __init__(self, train_data=None, test_data=None):
        self.Xtrain = train_data
        self.Xtest = test_data

    def _std(self):
        """
        标准化处理
        """
        self.X_mean = np.mean(self.Xtrain, axis=0)  # 按列求Xtrain平均值
        self.X_std = np.std(self.Xtrain, axis=0)  # 求标准差
        self.X_row, self.X_col = self.Xtrain.shape  # 求Xtrain行、列数
        self.Xtrain = (self.Xtrain - np.tile(self.X_mean, (self.X_row, 1))) / np.tile(self.X_std, (self.X_row, 1))

    def _eigen_decomposition(self):
        """
        特征值分解
        """
        # 求协方差矩阵
        sigma = np.cov(self.Xtrain, rowvar=False)
        # 对协方差矩阵进行特征值分解，_lambda为特征值构成的对角阵，T的列为单位特征向量，且与_lambda中的特征值一一对应
        self._lambda, T = linalg.eig(sigma)

        # 取对角元素(结果为一列向量)，即_lambda值，并上下反转使其从大到小排列
        T = T[:, self._lambda.argsort()]  # 将T的列按照_lambda升序排列
        self._lambda.sort()  # 将_lambda按升序排列
        self.D = -np.sort(-np.real(self._lambda))  # 提取实数部分，按降序排列

        # 主元数量初值为1，若累计贡献率小于90%则增加主元数量
        self.num_pc = 1  # 主元数量
        while sum(self.D[0:self.num_pc]) / sum(self.D) < 0.9:
            self.num_pc += 1

        # 取与_lambda相对应的特征向量
        self.P = T[:, self.X_col - self.num_pc:self.X_col]

    def _statistical_control_limits(self):
        """
        计算统计控制限
        """
        # 置信度为95%的T2统计控制限
        self.T2UCL1 = self.num_pc * (self.X_row - 1) * (self.X_row + 1) * stats.f.ppf(
            0.95, self.num_pc, self.X_row - self.num_pc) / (self.X_row * (self.X_row - self.num_pc))

        # 置信度为95%的Q统计控制限
        theta = []
        for i in range(1, self.num_pc + 1):
            theta.append(sum((self.D[self.num_pc:]) ** i))
        h0 = 1 - 2 * theta[0] * theta[2] / (3 * theta[1] ** 2)
        ca = stats.norm.ppf(0.95, 0, 1)
        self.QUCL = theta[0] * (h0 * ca * np.sqrt(2 * theta[1]) /
                                theta[0] + 1 + theta[1] * h0 * (h0 - 1) / theta[0] ** 2) ** (1 / h0)

    def _online_monitoring(self):
        """
        在线监测
        """
        self.n = self.Xtest.shape[0]  # 求Xtest行数
        self.Xtest = (self.Xtest - np.tile(self.X_mean, (self.n, 1))) / np.tile(self.X_std, (self.n, 1))

        r, y = (self.P.dot(self.P.T)).shape
        I = np.eye(r, y)

        # 求T2统计量，Q统计量
        self.T2 = np.zeros(self.n)
        self.Q = np.zeros(self.n)
        for i in range(self.n):
            self.T2[i] = self.Xtest[i, :].dot(self.P).dot(
                linalg.pinv(np.diag(self._lambda.real)[self.X_col - self.num_pc:, self.X_col - self.num_pc:])).dot(
                self.P.T).dot(self.Xtest[i, :].T)
            self.Q[i] = self.Xtest[i, :].dot((I - self.P.dot(self.P.T))).dot((I - self.P.dot(self.P.T)).T).dot(
                self.Xtest[i, :].T)

        # 1. 确定造成失控状态的得分
        S = self.Xtest[51, :].dot(self.P[:, :self.num_pc])
        r = []
        for i in range(0, self.num_pc):
            if S[i] ** 2 / self._lambda[i].real > self.T2UCL1 / self.num_pc:
                r = np.r_[r, i]
        # 2. 计算每个变量相对于上述失控得分的贡献
        cont = np.zeros((len(r), self.X_col))
        i = len(r) - 1
        for j in range(0, self.X_col):
            cont[i, j] = abs(S[i] / self.D[i] * self.P[j, i] * self.Xtest[51, j])
        # 3. 计算每个变量的总贡献
        self.contj = np.zeros(self.X_col)
        for j in range(0, self.X_col):
            self.contj[j] = sum(cont[:, j])
        # 4. 计算每个变量对Q的贡献
        e = self.Xtest[51, :].dot((I - self.P.dot(self.P.T)))
        self.contq = e ** 2

        # 计算控制限
        alpha = 0.9
        S = np.diag(self._lambda.real)[self.X_col - self.num_pc:, self.X_col - self.num_pc:]
        PHI = self.P.dot(linalg.pinv(S)).dot(self.P.T) / self.T2UCL1 + (
                np.eye(self.X_col) - self.P.dot(self.P.T)) / self.QUCL
        S = np.cov(self.Xtrain, rowvar=False)
        g = np.trace((S.dot(PHI)) ** 2) / np.trace(S.dot(PHI))
        h = (np.trace(S.dot(PHI))) ** 2 / np.trace((S.dot(PHI)) ** 2)
        self.ksi = g * stats.chi2.ppf(alpha, h)
        # 综合指标
        self.phi = (self.Q / self.QUCL) + (self.T2 / self.T2UCL1)

    def _plot(self):
        """
        绘图
        """
        plt.subplot(2, 1, 1)
        plt.plot(np.r_[1:self.n + 1], self.T2, 'k')
        plt.title('主元分析统计量变化图')
        plt.xlabel('采样数')
        plt.ylabel('T^2')
        plt.plot(np.r_[1:self.n + 1], self.T2UCL1 * np.ones(self.n), 'r--')

        plt.subplot(2, 1, 2)
        plt.plot(np.r_[1:self.n + 1], self.Q, 'k')
        plt.xlabel('采样数')
        plt.ylabel('SPE')
        plt.plot(np.linspace(1, self.n, self.n), self.QUCL * np.ones(self.n), 'r--')
        plt.show()

        plt.subplot(2, 1, 1)
        plt.bar(np.r_[1:self.X_col + 1], self.contj)
        plt.xlabel('变量号')
        plt.ylabel('T^2贡献率 %')

        plt.subplot(2, 1, 2)
        plt.bar(np.r_[1:self.X_col + 1], self.contq)
        plt.xlabel('变量号')
        plt.ylabel('Q贡献率 %')
        plt.show()

        plt.plot(np.r_[1:self.n + 1], self.phi)
        plt.title('混合指标')
        plt.plot(np.linspace(1, self.n, self.n), self.ksi * np.ones(self.n), 'r--')
        plt.show()

    @staticmethod
    def gen_data(num):
        a = 10 * np.random.randn(num, 1)
        x1 = a + np.random.randn(num, 1)
        x2 = 1 * np.sin(a) + np.random.randn(num, 1)
        x3 = 5 * np.cos(5 * a) + np.random.randn(num, 1)
        x4 = 0.8 * x2 + 0.1 * x3 + np.random.randn(num, 1)
        return np.hstack((x1, x2, x3, x4))

    def run(self):
        if self.Xtrain is None or self.Xtest is None:
            # 生成训练数据
            x_train = self.gen_data(100)
            # 生成测试数据
            x_test = self.gen_data(100)
            x_test[50:, 1] = x_test[50:, 1] + 15 * np.ones(50)
            self.Xtrain = x_train
            self.Xtest = x_test
        self._std()
        self._eigen_decomposition()
        self._statistical_control_limits()
        self._online_monitoring()
        self._plot()
