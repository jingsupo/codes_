# coding: utf-8

import numpy as np


class SearchContinuousRegions(object):
    """
    搜索矩阵中连续为1最多的区域的元素个数
    连续的定义：上下左右相邻
    """

    def __init__(self, arr=None):
        if arr is None:
            arr = [
                [1, 0, 0, 1, 0],
                [1, 0, 1, 0, 0],
                [0, 0, 1, 0, 1],
                [1, 0, 1, 0, 1],
                [1, 0, 1, 1, 0],
            ]
        self.array = np.array(arr)
        self.rows = self.array.shape[0]
        self.cols = self.array.shape[1]

    def reset_to_0(self, arr, row, col, elem):
        # 置为0
        arr[row, col] = 0
        # 保存当前元素的行列索引
        elem.append((row, col))
        # 上
        if row - 1 >= 0 and arr[row - 1, col] == 1:
            self.reset_to_0(arr, row - 1, col, elem)
        # 下
        if row + 1 <= self.rows - 1 and arr[row + 1, col] == 1:
            self.reset_to_0(arr, row + 1, col, elem)
        # 左
        if col - 1 >= 0 and arr[row, col - 1] == 1:
            self.reset_to_0(arr, row, col - 1, elem)
        # 右
        if col + 1 <= self.cols - 1 and arr[row, col + 1] == 1:
            self.reset_to_0(arr, row, col + 1, elem)

    def search(self):
        # 连续为1的区域的数量
        num = 0
        # 保存每个连续为1的区域内的元素的行列索引
        ret = []
        # 临时保存每个连续为1的区域内的元素的行列索引，每个区域在最后都清空列表以供下个区域使用
        tmp = []
        for i in range(self.rows):
            for j in range(self.cols):
                if self.array[i, j] == 1:
                    self.reset_to_0(self.array, i, j, tmp)
                    ret.append(tmp)
                    # 清空列表以供下个区域使用
                    tmp = []
                    num += 1
        return num, ret

    def run(self):
        block_num, result = self.search()
        max_count = max([len(block) for block in result])
        print(f'连续为1的区域的数量为：{block_num}')
        print('每个连续为1的区域内的元素的行列索引为：')
        print(result)
        print(f'连续为1的区域内元素个数最多为：{max_count}')
