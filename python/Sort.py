# coding: utf-8


class Sort(object):
    """
    排序算法
    """

    @staticmethod
    def bubble_sort(arr):
        # 冒泡排序
        n = len(arr)
        for j in range(n - 1):
            # 设置原有数据是否有序的标志
            flag = 0
            # 内层每循环一次，最后的元素变为最大，所以针对所有的元素重复以上步骤的时候要排除最后一个。
            for i in range(n - 1 - j):
                # 如果相邻的两个数值中前值大于后值，则交换两个数值
                if arr[i] > arr[i + 1]:
                    arr[i], arr[i + 1] = arr[i + 1], arr[i]
                    flag += 1
            # 内层循环遍历一次后，如果从来没有交换过数据，证明列表已经有序
            if not flag:
                break

    @staticmethod
    def select_sort(arr):
        # 选择排序
        n = len(arr)
        for j in range(n - 1):
            # 默认0位置的数据最小
            min_index = j
            for i in range(j + 1, n):
                if arr[min_index] > arr[i]:
                    # 把更小的数据的位置记录起来
                    min_index = i
            # 循环结束时，min_index指向的是最小的数据的位置
            arr[min_index], arr[j] = arr[j], arr[min_index]

    @staticmethod
    def insert_sort(arr):
        # 插入排序
        n = len(arr)
        # 从第2个位置，即下标为1的元素开始向前插入
        for j in range(1, n):
            # 从第j个元素开始向前比较，如果小于前一个元素，交换位置
            for i in range(j, 0, -1):
                if arr[i] < arr[i - 1]:
                    arr[i], arr[i - 1] = arr[i - 1], arr[i]
                else:
                    break

    @staticmethod
    def quick_sort(self, arr, start, end):
        # 快速排序
        # 递归的退出条件
        if start >= end:
            return
        # 设定起始元素为要寻找位置的基准元素
        mid = arr[start]
        # 序列左边的由左向右移动的游标
        left = start
        # 序列右边的由右向左移动的游标
        right = end
        while left < right:
            while left < right and arr[right] >= mid:
                right -= 1
            arr[left] = arr[right]
            while left < right and arr[left] < mid:
                left += 1
            arr[right] = arr[left]
        # 退出循环后，low与high重合
        # 将基准元素放到该位置
        arr[left] = mid
        # 对基准元素左边的子序列进行快速排序
        self.quick_sort(arr, start, left - 1)
        # 对基准元素右边的子序列进行快速排序
        self.quick_sort(arr, left + 1, end)

    @staticmethod
    def merge_sort(self, arr):
        # 归并排序
        # 强制转换list
        if not isinstance(arr, list):
            arr = list(arr)
        # 递归的退出条件
        if len(arr) == 1:
            return arr
        # 二分分解
        mid = len(arr) // 2
        left = arr[:mid]
        right = arr[mid:]
        # 递归分解
        left_arr = self.merge_sort(left)
        right_arr = self.merge_sort(right)
        # 合并
        result = self._merge(left_arr, right_arr)
        return result

    @staticmethod
    def _merge(left, right):
        # 把两个有序序列合并为一个有序序列
        # 定义两个下标指针，分别从0开始
        le = 0
        ri = 0
        result = []
        while le < len(left) and ri < len(right):
            if left[le] <= right[ri]:
                result.append(left[le])
                le += 1
            else:
                result.append(right[ri])
                ri += 1
        result += left[le:]
        result += right[ri:]
        return result
