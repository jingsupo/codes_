def AMPD(data, top=1):
    """
    在数字信号处理中，经常涉及到波峰（或波谷）查找算法，比如心率测量、步数计数等。
    对于周期信号或者准周期信号，有一种称为
    Automatic multiscale-based peak detection (AMPD)，即自动多尺度峰值查找算法。

    实现AMPD算法
    :param data: 1-D numpy.ndarray
    :param top: [1, 2, 3, ...] 代表[第一高，第二高，第三高，...]
    :return: 波峰所在索引值的列表
    """

    import numpy as np

    pks = np.zeros_like(data, dtype=np.int32)
    count = data.shape[0]
    rowsum_list = []
    for k in range(1, count // 2 + 1):
        rowsum = 0
        for i in range(k, count - k):
            if data[i] > data[i - k] and data[i] > data[i + k]:
                rowsum -= 1
        rowsum_list.append(rowsum)
    max_window_length = np.argmin(rowsum_list)
    for k in range(1, max_window_length + 1):
        for i in range(k, count - k):
            if data[i] > data[i - k] and data[i] > data[i + k]:
                pks[i] += 1
    return np.where(pks == max_window_length + 1 - top)[0]


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    import numpy as np

    def sim_data(n=1000):
        x = np.linspace(0, 200, n)
        y = (
            2 * np.cos(2 * np.pi * 300 * x)
            + 5 * np.sin(2 * np.pi * 100 * x)
            + 4 * np.random.randn(n)
        )
        return y

    def vis():
        y = sim_data()
        pks = AMPD(y)

        plt.plot(range(len(y)), y, color="black")
        plt.scatter(pks, y[pks], color="red")
        plt.savefig("ampd.png")
        plt.show()

    vis()

