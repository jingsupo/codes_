# coding: utf-8

import struct

import matplotlib.pyplot as plt
import numpy as np
import pyaudio

CHUNK = 1024 * 2
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100

p = pyaudio.PyAudio()

stream = p.open(
    format=FORMAT,
    channels=CHANNELS,
    rate=RATE,
    input=True,
    output=True,
    frames_per_buffer=CHUNK
)

fig, (ax, ax2) = plt.subplots(2, figsize=(15, 8))

x = np.arange(0, 2 * CHUNK, 2)
x_fft = np.linspace(0, RATE, CHUNK)

line, = ax.plot(x, np.random.rand(CHUNK), '-', lw=2)
line_fft, = ax2.plot(x_fft, np.random.rand(CHUNK), '-', lw=2)

ax.set_title('Audio Waveform')
ax.set_xlabel('samples')
ax.set_ylabel('volume')
ax.set_xlim(0, 2 * CHUNK)
ax.set_ylim(0, 255)
plt.setp(ax, xticks=[0, CHUNK, 2 * CHUNK], yticks=[0, 128, 255])

ax2.set_title('Audio FFT')
ax2.set_xlabel('freq')
ax2.set_ylabel('volume')
ax2.set_xlim(20, RATE / 2)

while True:
    data = stream.read(CHUNK)
    data_int = struct.unpack(str(2 * CHUNK) + 'B', data)
    data_np = np.array(data_int, dtype='b')[::2] + 128
    data_fft = np.fft.fft(data_int)
    line.set_ydata(data_np)
    line_fft.set_ydata(np.abs(data_fft[0:CHUNK]) * 2 / (256 * CHUNK))
    fig.canvas.draw()
    fig.canvas.flush_events()

