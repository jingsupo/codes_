# coding: utf-8


def impala():
    """
    Impala
    pip install impyla
    """

    from impala.dbapi import connect

    conn = connect(host='10.11.3.85', port=21050)
    cur = conn.cursor()
    cur.execute('use prealarm_caeri;')
    cur.execute('describe model_base_reconsitution;')
    data = cur.fetchall()
    df = pd.DataFrame(data)
    columns = [c[0] for c in cur.description]
    df.columns = columns
    cur.close()
    conn.close()


def hbase():
    """
    HBase
    """

    import happybase

    conn = happybase.Connection('10.6.9.211', 9090)
    print(conn.tables())
    table = conn.table('t2a_trans')

    dataset = []
    num = 1000000
    scanner = table.scan(limit=num)
    try:
        for key, data in scanner:
            dataset.append({k.decode(): v.decode() for k, v in data.items()})
    except Exception as e:
        print(e)

    conn.close()


def hive():
    """
    Hive
    """

    import pandas as pd
    from pyhive import hive

    conn = hive.Connection(host='10.6.9.211', port=10000, database='mdr')

    databases = pd.read_sql('show databases', conn)
    print(databases)
    tables = pd.read_sql('show tables', conn)
    print(tables)

    num = 1000000
    sql = f'select * from t2a_trans_t limit {num}'

    cursor = conn.cursor()
    cursor.execute(sql)

    description = cursor.description
    columns = []
    for column in description:
        columns.append(column[0])

    print(cursor.fetchone())
    print(cursor.fetchmany(10))
    print(cursor.fetchall())

    try:
        df = pd.read_sql(sql, conn)
    except Exception as e:
        print(e)

    conn.close()


def mysql():
    """
    MySQL
    """

    import pandas as pd
    import pymysql

    conn = pymysql.connect(host='10.6.9.211', user='root', password='root', database='admin')
    # conn = sqlalchemy.create_engine('mysql+pymysql://root:root@10.6.9.211/admin')

    sql = 'select * from iris_n'
    df = pd.read_sql(sql, conn)
    print(df)

    conn.close()


def oracle():
    """
    Oracle
    """

    import os

    import cx_Oracle as oracle
    import pandas as pd
    from sshtunnel import SSHTunnelForwarder

    os.environ['NLS_LANG'] = 'SIMPLIFIED CHINESE_CHINA.UTF8'

    with SSHTunnelForwarder(
            ('10.6.9.211', 22),  # 跳板服务器ip，ssh端口
            ssh_username='root',  # 跳板ssh用户名
            ssh_password='Roz4GKBD',  # 跳板ssh密码
            remote_bind_address=('10.6.50.185', 1521),  # 目标数据库ip，端口
            local_bind_address=('localhost', 1521)  # 映射到本地端口
    ) as server:
        conn = oracle.connect('aml63', 'aml63', 'localhost/amldb')
        # conn = sqlalchemy.create_engine("oracle+cx_oracle://aml63:aml63@localhost/amldb")

        sql = 'select * from dept'
        df = pd.read_sql(sql, conn)
        print(df)

        conn.close()


def mssql():
    """
    SQL Server
    """

    import pandas as pd
    import pymssql

    conn = pymssql.connect(server='localhost', user='sa', password='sql', database='tmp', charset='cp936')
    # conn = sqlalchemy.create_engine(
    #     'mssql+pymssql://sa:sql@localhost/tmp?charset=cp936')  # charset=cp936 解决了数据库编码为GBK时中文乱码的问题

    sql = 'select * from params'
    df = pd.read_sql(sql, conn)
    print(df)

    conn.close()


def odbc():
    """
    Microsoft Access
    """

    import pyodbc

    mdb = r'DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=test.mdb;'
    conn = pyodbc.connect(mdb)
    cur = conn.cursor()

    for table in cur.tables(tableType='TABLE'):
        print(table.table_name)

    sql = 'select * from USERINFO'
    rows = cur.execute(sql)
    for row in rows:
        print(row)

    conn.close()


def sqlite():
    """
    SQLite
    """

    import sqlite3

    conn = sqlite3.connect('history.sqlite')
    cur = conn.cursor()

    sql = 'select name from sqlite_master where type="table" order by name'
    cur.execute(sql)
    print(cur.fetchall())

    conn.close()
