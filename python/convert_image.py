# coding: utf-8

import os

import cv2


src = r'D:\Pictures\src'
out = r'D:\Desktop\out'


def cvt_img(img_path, out_path):
    img = cv2.imread(img_path)
    if img is None:
        print('图像读取失败')
        return
    # 去色，灰度化
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # 取反
    inv = 255 - gray
    # 高斯滤波
    blur = cv2.GaussianBlur(inv, ksize=(15, 15), sigmaX=50, sigmaY=50)
    # 颜色减淡混合
    res = cv2.divide(gray, 255 - blur, scale=255)

    """
    上面是根据PS的流程转化的Python实现流程，感觉实际上起作用的就是这个公式x / (255 - blur_inv_x) * 255。
    
    上面的流程是：
    
    灰度图->取反->高斯滤波->再取反(除法里面)->除法运算(divide)。
    """

    cv2.imwrite(out_path, res)


files = os.listdir(src)
paths = [os.path.join(src, f) for f in files if f[-4:] in ['.jpg', '.JPG']]
for p in paths:
    print(p)
    cvt_img(p, os.path.join(out, p.split('\\')[-1]))
