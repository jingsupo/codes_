import pycrfsuite
from seqeval.metrics import classification_report


def load_data(path):
    sents = []
    sent = []
    with open(path, encoding="utf-8") as f:
        for line in f.readlines():
            if line == "\n":
                sents.append(sent)
                sent = []
            else:
                word = line.strip("\n").split()
                sent.append((word[0], "", word[1]))
        if sent:
            sents.append(sent)
    return sents


def word2features(sent, i):
    word = sent[i][0]
    postag = sent[i][1]

    features = {
        "bias": 1.0,
        "word.lower()": word.lower(),
        "word[-3:]": word[-3:],
        "word[-2:]": word[-2:],
        "word.isupper()": word.isupper(),
        "word.istitle()": word.istitle(),
        "word.isdigit()": word.isdigit(),
        "postag": postag,
        "postag[:2]": postag[:2],
    }
    if i > 0:
        word1 = sent[i - 1][0]
        postag1 = sent[i - 1][1]
        features.update(
            {
                "-1:word.lower()": word1.lower(),
                "-1:word.istitle()": word1.istitle(),
                "-1:word.isupper()": word1.isupper(),
                "-1:postag": postag1,
                "-1:postag[:2]": postag1[:2],
            }
        )
    else:
        features["BOS"] = True

    if i < len(sent) - 1:
        word1 = sent[i + 1][0]
        postag1 = sent[i + 1][1]
        features.update(
            {
                "+1:word.lower()": word1.lower(),
                "+1:word.istitle()": word1.istitle(),
                "+1:word.isupper()": word1.isupper(),
                "+1:postag": postag1,
                "+1:postag[:2]": postag1[:2],
            }
        )
    else:
        features["EOS"] = True

    return features


def sent2features(sent):
    return [word2features(sent, i) for i in range(len(sent))]


def sent2labels(sent):
    return [label for token, postag, label in sent]


def sent2tokens(sent):
    return [token for token, postag, label in sent]


def gen_features(sents):
    return [sent2features(s) for s in sents], [sent2labels(s) for s in sents]


def train_crf(X_train, y_train, output="model.crfsuite", verbose=False):
    trainer = pycrfsuite.Trainer(verbose=verbose)
    for x, y in zip(X_train, y_train):
        trainer.append(x, y)
    trainer.set_params(
        {
            "c1": 1.0,
            "c2": 1e-3,
            "max_iterations": 50,
            "feature.possible_transitions": True,
        }
    )
    trainer.train(output)
    return trainer


def predictor(output):
    tagger = pycrfsuite.Tagger()
    tagger.open(output)
    return tagger


if __name__ == '__main__':
    train_sents = load_data("/mnt/d/program/data/ner/data/train.char.bmes")
    test_sents = load_data("/mnt/d/program/data/ner/data/test.char.bmes")

    X_train, y_train = gen_features(train_sents)
    X_test, y_test = gen_features(test_sents)

    output = "model"
    trainer = train_crf(X_train, y_train, output)
    print(trainer.logparser.last_iteration)

    tagger = predictor(output)
    y_pred = [tagger.tag(x) for x in X_test]

    print(classification_report(y_test, y_pred))
