# coding: utf-8

def docx_demo():
    import re
    import docx
    from colorama import Fore
    from ist_ner.engine import IstNer

    # 获取文档对象
    doc = docx.Document(r'D:\Desktop\ist\swift报文样例.docx')
    print('段落数：' + str(len(doc.paragraphs)), end='\n\n')

    texts_list = []
    # 输出每一段的内容
    for para in doc.paragraphs:
        texts_list.append(para.text)

    texts1 = '\n'.join(texts_list)
    texts2 = ',\n'.join(texts_list)

    def get_field_list(text):
        pattern = r':[\s]*[\w+]{2,5}[\s]*:'
        return re.split(pattern, text)

    def get_field_content(sep1, text):
        sep2 = r':[\s]*[\w+]{2,5}[\s]*:'
        pattern = rf'{sep1}([\s\S]*?){sep2}'
        return re.findall(pattern, text)

    find_list = {':50K:': '汇款人名称和地址', ':52D:': '汇款行名称和地址', ':59:': '收款人名称和地址', ':50F:': 'MT202 收款人名称和地址'}
    result1 = dict()
    result2 = dict()
    for k, v in find_list.items():
        result1[k + v] = get_field_content(k, texts1)
        result2[k + v] = get_field_content(k, texts2)

    ner = IstNer()
    for k, v in zip(result1.items(), result2.items()):
        print(Fore.GREEN + f'=============================={k[0]}栏位==============================' + Fore.RESET)
        for text1, text2 in zip(k[1], v[1]):
            print(Fore.BLUE + '==============================报文==============================' + Fore.RESET)
            print(text1)
            ner.print_named_entities(text1, ner_tags_for_token=True, dep=True)
            ner.print_named_entities(text2, ner_tags_for_token=True, dep=True)
