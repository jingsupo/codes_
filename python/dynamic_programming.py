# coding: utf-8

import numpy as np


def dynamic_programming():
    """
    DP算法（动态规划）初试

        假设有一个6 * 6的棋盘，每个格子里面有一个奖品（每个奖品的价值在100到1000之间），现在要求从左上角开始到右下角结束，
    每次只能往右或往下走一个格子，所经过的格子里的奖品归自己所有。问最多能收集价值多少的奖品。
        DP算法适用于前一步的决策影响后一步决策的问题。
        本题右下角格子的决策取决于其左边和上面的最优决策，因此，右下角a[i][j]只需要取max(a[i-1][j], a[i][j-1]) + a[i][j]；
    其余部分只受左边或者上面的决策影响，因此，横向的a[i][j]应该取a[i][j-1] + a[i][j], 纵向的a[i][j]应该取a[i-1][j] + a[i][j]。
    """

    arr = np.zeros((6, 6), dtype=int)
    for i in range(6):
        for j in range(6):
            arr[i, j] = np.random.randint(100, 1000)
    print("随机生成一个6*6的二维数组作为棋盘中的权值：")
    print(arr)

    # 首先将第一行和第一列的格子向右或向下累加，除了第一个格子，其余格子的值为前一个格子的值加当前格子的值
    for i in range(1, 6):
        arr[0, i] = arr[0, i - 1] + arr[0, i]
        arr[i, 0] = arr[i - 1, 0] + arr[i, 0]

    # 计算每个格子的最大值
    for i in range(1, 6):
        for j in range(1, 6):
            arr[i, j] = max(arr[i - 1, j], arr[i, j - 1]) + arr[i, j]
    print("变化后的数组：")
    print(arr)
