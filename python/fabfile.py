from fabric import Connection, task
from invoke import Responder

hosts = ['10.6.9.211', '10.6.9.213']
user = 'root'
passwords = ['Roz4GKBD', 'iBsA6RNM']


@task
def hello(c):
    print('hello world')
    c.run('which cat', warn=True)


@task
def welcome(c, name, word):
    print(f'hello {name}, {word}')
    c.run('hostname')
    c.run('pwd')
    c.run('cd /opt && pwd')


@task
def demo(c, i):
    i = int(i)
    c = Connection(hosts[i], user=user, connect_kwargs={'password': passwords[i]})
    result = c.run('hostname', hide=True, replace_env=True)
    print('stdout')
    print(result.stdout.strip())
    print('stderr')
    print(result.stderr.strip())
    c.local('hostname')
    envs = ['export JAVA_HOME=/opt/jdk1.8.0_291', 'export PATH=$PATH:$JAVA_HOME/bin']
    cmd = ['java -version']
    c.local('&&'.join(envs + cmd))


@task
def auto_response(c, i):
    i = int(i)
    c = Connection(hosts[i], user=user, connect_kwargs={'password': passwords[i]})
    sudopass = Responder(pattern=r'\[sudo\] password:', response='mypassword\n')
    c.run('sudo hostname', pty=True, watchers=[sudopass])
