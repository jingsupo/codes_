import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.nn.functional as F

import dgl
from dgl.data import DGLDataset
from dgl.nn import GraphConv


# 1.读取并清洗数据


base_dir = "/mnt/d/program/data/elliptic_txs/"

df_features = pd.read_csv(base_dir + "elliptic_txs_features.csv", header=None)

colNames1 = {"0": "txId", "1": "Time step"}
colNames2 = {str(ii + 2): "Local_feature_" + str(ii + 1) for ii in range(93)}
colNames3 = {str(ii + 95): "Aggregate_feature_" + str(ii + 1) for ii in range(72)}

colNames = dict(colNames1, **colNames2, **colNames3)
colNames = {int(k): v for k, v in colNames.items()}

df_features = df_features.rename(columns=colNames)

df_classes = pd.read_csv(base_dir + "elliptic_txs_classes.csv")

df_classes = pd.merge(df_classes, df_features, how="left", on="txId")

del df_features

df_edges = pd.read_csv(base_dir + "elliptic_txs_edgelist.csv")

node_map = {}  # 节点编号
for i, j in enumerate(df_classes["txId"]):
    node_map[j] = i

df_edges["Src"] = df_edges["txId1"].map(node_map)
df_edges["Dst"] = df_edges["txId2"].map(node_map)
df_classes["id"] = df_classes["txId"].map(node_map)


# 2.用DGL构建图数据


class MyDataset(DGLDataset):
    def __init__(self):
        super().__init__(name="Elliptic")

    def process(self):
        node_features = torch.from_numpy(
            np.array(df_classes.drop(["txId", "class", "id"], axis=1))
        ).to(torch.float32)
        node_labels = torch.from_numpy(
            np.array(df_classes["class"].astype("category").cat.codes)
        ).long()

        edges_src = torch.from_numpy(np.array(df_edges["Src"]))
        edges_dst = torch.from_numpy(np.array(df_edges["Dst"]))

        self.graph = dgl.graph((edges_src, edges_dst), num_nodes=df_classes.shape[0])

        self.graph.ndata["feat"] = node_features
        self.graph.ndata["label"] = node_labels

        # If your dataset is a node classification dataset, you will need to assign
        # masks indicating whether a node belongs to training, validation, and test set.
        n_nodes = df_classes.shape[0]

        n_train = int(n_nodes * 0.6)
        n_val = int(n_nodes * 0.2)

        train_mask = torch.zeros(n_nodes, dtype=torch.bool)
        val_mask = torch.zeros(n_nodes, dtype=torch.bool)
        test_mask = torch.zeros(n_nodes, dtype=torch.bool)

        train_mask[:n_train] = True
        val_mask[n_train : n_train + n_val] = True
        test_mask[n_train + n_val :] = True

        self.graph.ndata["train_mask"] = train_mask
        self.graph.ndata["val_mask"] = val_mask
        self.graph.ndata["test_mask"] = test_mask

    def __getitem__(self, i):
        return self.graph

    def __len__(self):
        return 1


# 3.搭建GCN模型


class GCN(nn.Module):
    def __init__(self, in_feats, h_feats, num_classes):
        super(GCN, self).__init__()
        self.conv1 = GraphConv(in_feats, h_feats)
        self.conv2 = GraphConv(h_feats, num_classes)

    def forward(self, g, in_feat):
        h = self.conv1(g, in_feat)
        h = F.relu(h)
        h = self.conv2(g, h)
        return h


def train(g, model):
    optimizer = torch.optim.Adam(model.parameters(), lr=0.01)

    features = g.ndata["feat"]
    labels = g.ndata["label"]

    train_mask = g.ndata["train_mask"]
    val_mask = g.ndata["val_mask"]
    test_mask = g.ndata["test_mask"]

    best_val_acc = 0
    best_test_acc = 0

    for e in range(50):
        # Forward
        logits = model(g, features)

        # Compute prediction
        pred = logits.argmax(1)

        # Compute loss
        # Note that you should only compute the losses of the nodes in the training set.
        loss = F.cross_entropy(logits[train_mask], labels[train_mask])

        # Compute accuracy on training/validation/test
        train_acc = (pred[train_mask] == labels[train_mask]).float().mean()
        val_acc = (pred[val_mask] == labels[val_mask]).float().mean()
        test_acc = (pred[test_mask] == labels[test_mask]).float().mean()

        # Save the best validation accuracy and the corresponding test accuracy.
        if best_val_acc < val_acc:
            best_val_acc = val_acc
            best_test_acc = test_acc

        # Backward
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if e % 5 == 0:
            print(
                "In epoch {},\t loss: {:.3f},\t val acc: {:.3f} (best {:.3f}),\t test acc: {:.3f} (best {:.3f})".format(
                    e, loss, val_acc, best_val_acc, test_acc, best_test_acc
                )
            )


dataset = MyDataset()

g = dataset[0]
g = dgl.add_self_loop(g)

# Create the model with given dimensions
model = GCN(g.ndata["feat"].shape[1], 16, 3)

train(g, model)
