# coding: utf-8

def generate_swift_message(n=1000, output=''):
    import random
    import re
    import pandas as pd
    from faker import Faker
    from xeger import Xeger
    from ist_ner.config import company_name_keywords

    companies = pd.read_excel(r'D:\Desktop\世界著名企业中英文对照表.xls').iloc[:, 0].values.tolist()
    fake = Faker()
    fake_companies = []
    company_name_keywords = [x.lower() for x in company_name_keywords]
    while True:
        assert n / 2 > len(companies)
        if len(fake_companies) == (n / 2 - len(companies)):
            break
        fake_company = fake.company()
        words = fake_company.split()
        cnt = 0
        for w in words:
            if w.lower() in company_name_keywords:
                cnt += 1
        if cnt > 0 and fake_company.lower() not in company_name_keywords:
            fake_companies.append(fake_company)
    companies = companies + fake_companies
    persons = [fake.name() for _ in range(int(n / 2))]
    com_per = companies + persons
    addresses = [fake.address() for _ in range(n)]
    hc = pd.read_csv(r'D:\program\python\ist_ner\high_risk_countries_regions.csv', header=None).values.ravel().tolist()
    hc = random.choices(hc, k=n)
    pattern_acct_num = '^/[0-9]{11,34}?$'  # 结果中带'/'
    pattern_acct_num2 = '^/[A-Za-z0-9]{11,34}?$'  # 结果中带'/'
    pattern_id_code = '^[A-Z]{6}[A-Z0-9]{2}[A-Z0-9]{0,3}?$'
    acct_num = []
    id_code = []
    num_0_6 = int(n * 0.6)
    num_0_2 = int(n * 0.2)
    for i in range(num_0_6):
        acct_num.append(Xeger().xeger(pattern_acct_num))
    for i in range(num_0_2):
        acct_num.append(Xeger().xeger(pattern_acct_num2))
    for i in range(num_0_2):
        id_code.append(Xeger().xeger(pattern_id_code))
    acct_num_bic = acct_num + id_code

    def ensure_length(text: str) -> str:
        result = []

        def inner(text: str, result: list):
            """
            保证字符串每行长度不超过35，超过的自动增加换行符
            """
            if len(text) < 1:
                return
            if len(text[:36]) < 36:
                substr = text[:35]
            elif not text[:36].endswith(' '):
                substr = text[:35].split()[-1].replace('(', r'\(').replace(')', r'\)')
                substr = re.sub(rf'{substr}$', '', text[:35])
            else:
                substr = text[:35]
            if substr:
                result.append(substr.strip() + '\n')
                substr = substr.replace('(', r'\(').replace(')', r'\)')
                inner(re.sub(rf'^{substr}', '', text), result)

        inner(text, result)
        return ''.join(result)

    _swift_messages = [x for x in zip(acct_num_bic, com_per, addresses, hc)]
    swift_messages = []
    for x in _swift_messages:
        tmp = []
        for i in x:
            tmp.append(ensure_length(i))
        swift_messages.append(''.join(tmp))

    swift_messages = ['=' * 60 + '\n' + x for i, x in enumerate(swift_messages)]

    if output:
        with open(output, 'w', encoding='utf-8') as f:
            f.writelines(swift_messages)

    return swift_messages
