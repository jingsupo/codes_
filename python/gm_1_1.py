import numpy as np
import pandas as pd


data = [
    [
        "1985",
        "1986",
        "1987",
        "1988",
        "1989",
        "1990",
        "1991",
        "1992",
        "1993",
        "1994",
        "1995",
        "1996",
        "1997",
        "1998",
        "1999",
        "2000",
    ],
    [
        101.02,
        102.19,
        106.5,
        111.08,
        113.28,
        115.97,
        118.02,
        119.99,
        123.23,
        132.37,
        135.95,
        138.82,
        143.18,
        146.51,
        151.54,
        159.47,
    ],
]
df = pd.DataFrame(data).T
df.columns = ["年份", "非农业人口"]
# 对要预测的序列进行一次累加
df["累加序列"] = df["非农业人口"].cumsum()
# 求一次累加序列的滑动平均
df["滑动平均"] = -df["累加序列"].rolling(2).mean()
df["flag"] = 1
print(df)
B = df[["滑动平均", "flag"]].iloc[1:].values  # B矩阵
Y = df["非农业人口"].iloc[1:].values  # Y矩阵
f1 = B.T @ B  # 矩阵乘法
f2 = np.linalg.inv(f1)  # 矩阵求逆
f3 = B.T @ Y  # 矩阵乘法
f = f2 @ f3  # 矩阵乘法
a = f[0]
u = f[1]
v = df["非农业人口"].iloc[0] - u / a
num = 20  # 要预测的序列长度
# 预测的累加序列
pred_cumsum = [v * np.exp(-a * i) + u / a for i in range(num)]
print(pred_cumsum)
# 将累加序列还原为原始序列
pred = np.ediff1d(pred_cumsum, to_begin=pred_cumsum[0])
print(pred)
