import datetime

from graphframes import GraphFrame
from pyspark.sql import SparkSession
from pyspark.sql.functions import count, desc

spark = (
    SparkSession.builder.config(
        "spark.jars", "/mnt/d/lib/graphframes-0.8.1-spark3.0-s_2.12.jar"
    )
    .appName("spark")
    .getOrCreate()
)
sc = spark.sparkContext
sc.setLogLevel("ERROR")
sc.setCheckpointDir("ckpt")

# 计时开始
start = datetime.datetime.now()

# Obtain airports dataset
airports = spark.read.csv(
    "/mnt/d/program/data/openflights/airport-codes-na.txt",
    header=True,
    inferSchema=True,
    sep="\t",
)
airports.createOrReplaceTempView("airports")
# Obtain departure Delays data
departureDelays = spark.read.csv(
    "/mnt/d/program/data/openflights/departuredelays.csv", header=True
)
departureDelays.createOrReplaceTempView("departureDelays")
departureDelays.cache()

# Available IATA codes from the departuredelays sample dataset
tripIATA = spark.sql(
    """
select distinct iata from (
select distinct origin as iata from departureDelays
union all
select distinct destination as iata from departureDelays) a
"""
)
tripIATA.createOrReplaceTempView("tripIATA")

# Only include airports with at least one trip from the departureDelays dataset
airports = spark.sql(
    """
select a.IATA, a.City, a.State, a.Country from airports a
join tripIATA t
on t.IATA=a.IATA
"""
)
airports.createOrReplaceTempView("airports")
airports.cache()

# Build `departureDelaysGeo` DataFrame
# Obtain key attributes such as Date of flight, delays, distance, and airport information (Origin, Destination)
departureDelaysGeo = spark.sql(
    """
select cast(d.date as int) as tripid, 
cast(concat(concat(concat(concat(concat(concat(
'2014-', concat(concat(substr(cast(d.date as string), 1, 2), '-')), substr(cast(d.date as string), 3, 2)), ' '), 
substr(cast(d.date as string), 5, 2)), ':'), substr(cast(d.date as string), 7, 2)), ':00') as timestamp) as localdate, 
cast(d.delay as int), cast(d.distance as int), d.origin as src, d.destination as dst, 
a1.city as city_src, a2.city as city_dst, a1.state as state_src, a2.state as state_dst from departureDelays d
join airports a1 on a1.iata=d.origin
join airports a2 on a2.iata=d.destination
"""
)
departureDelaysGeo.createOrReplaceTempView("departureDelaysGeo")
departureDelaysGeo.cache()

"""
Building the Graph

Now that we've imported our data, we're going to need to build our graph. 
To do so we're going to do two things: 
we are going to build the structure of the vertices (or nodes) and we're going to build the structure of the edges. 
What's awesome about GraphFrames is that this process is incredibly simple.

Rename IATA airport code to id in the Vertices Table
Start and End airports to src and dst for the Edges Table (flights)
These are required naming conventions for vertices and edges in GraphFrames as of the time of this writing (Feb. 2016).
"""
# Create Vertices (airports) and Edges (flights)
tripVertices = airports.withColumnRenamed("IATA", "id").distinct()
tripVertices.show()
tripVertices.cache()
tripEdges = departureDelaysGeo.select(
    "tripid", "delay", "src", "dst", "city_dst", "state_dst"
)
tripEdges.show()
tripEdges.cache()

# Build `tripGraph` GraphFrame
# This GraphFrame builds up on the vertices and edges based on our trips (flights)
tripGraph = GraphFrame(tripVertices, tripEdges)
# Build `tripGraphPrime` GraphFrame
# This GraphFrame contains a smaller subset of data to make it easier to display motifs and subgraphs (below)
tripEdgesPrime = departureDelaysGeo.select("tripid", "delay", "src", "dst")
tripGraphPrime = GraphFrame(tripVertices, tripEdgesPrime)

# Determine the number of airports and trips
print("Airports: %d" % tripGraph.vertices.count())
print("Trips: %d" % tripGraph.edges.count())

# Determining the longest delay in this dataset
longestDelay = tripGraph.edges.groupBy().max("delay")
longestDelay.show()

# Determining the number of delayed vs. on-time / early flights
print("On-time / Early Flights: %d" % tripGraph.edges.filter("delay <= 0").count())
print("Delayed Flights: %d" % tripGraph.edges.filter("delay > 0").count())

# What destinations tend to have delays
tripDelays = tripGraph.edges.filter("delay > 0")

# What flights departing SEA are most likely to have significant delays
(
    tripGraph.edges.filter("src = 'SEA' and delay > 0")
    .groupby("src", "dst")
    .avg("delay")
    .sort(desc("avg(delay)"))
    .show()
)
# What destinations tend to have significant delays departing from SEA
tripGraph.edges.filter("src = 'SEA' and delay > 100").show()

"""
Vertex Degrees

inDegrees: Incoming connections to the airport
outDegrees: Outgoing connections from the airport
degrees: Total connections to and from the airport

Reviewing the various properties of the property graph 
to understand the incoming and outgoing connections between airports.
"""
# tripGraph.degrees.sort(desc('degree')).limit(20).show()
# tripGraph.inDegrees.sort(desc('inDegree')).limit(20).show()
# tripGraph.outDegrees.sort(desc('outDegree')).limit(20).show()

# What delays might we blame on SFO
# Using tripGraphPrime to more easily display
#   - The associated edge (ab, bc) relationships
#   - With the different the city / airports (a, b, c) where SFO is the connecting city (b)
#   - Ensuring that flight ab (i.e. the flight to SFO) occurred before flight bc (i.e. flight leaving SFO)
#   - Note, TripID was generated based on time in the format of MMDDHHMM converted to int
#       - Therefore bc.tripid < ab.tripid + 10000
#           means the second flight (bc) occured within approx a day of the first flight (ab)
# Note: In reality, we would need to be more careful to link trips ab and bc.
motifs = tripGraphPrime.find("(a)-[ab]->(b); (b)-[bc]->(c)").filter(
    "(b.id = 'SFO') and (ab.delay > 500 or bc.delay > 500) "
    "and bc.tripid > ab.tripid and bc.tripid < ab.tripid + 10000"
)
motifs.show()

# Determining Airport Ranking using PageRank
# ranks = tripGraph.pageRank(resetProbability=0.15, maxIter=5)
# ranks.vertices.orderBy(ranks.vertices.pagerank.desc()).limit(20).show()

# Determine the most popular flights (single city hops)
topTrips = tripGraph.edges.groupBy("src", "dst").agg(count("delay").alias("trips"))
# Show the top 20 most popular flights (single city hops)
topTrips.orderBy(topTrips.trips.desc()).limit(20).show()

"""
Top Transfer Cities

Many airports are used as transfer points instead of the final Destination. 
An easy way to calculate this is by calculating the ratio of inDegree 
(the number of flights to the airport) / outDegree (the number of flights leaving the airport). 
Values close to 1 may indicate many transfers, 
whereas values < 1 indicate many outgoing flights and > 1 indicate many incoming flights. 
Note, this is a simple calculation that does not take into account of timing or scheduling of flights, 
just the overall aggregate number within the dataset.
"""
# Calculate the inDeg (flights into the airport) and outDeg (flights leaving the airport)
inDeg = tripGraph.inDegrees
outDeg = tripGraph.outDegrees
# Calculate the degreeRatio (inDeg/outDeg)
degreeRatio = (
    inDeg.join(outDeg, inDeg.id == outDeg.id)
    .drop(outDeg.id)
    .selectExpr("id", "double(inDegree)/double(outDegree) as degreeRatio")
    .cache()
)
degreeRatio.show()
# Join back to the `airports` DataFrame (instead of registering temp table as above)
nonTransferAirports = (
    degreeRatio.join(airports, degreeRatio.id == airports.IATA)
    .selectExpr("id", "city", "degreeRatio")
    .filter("degreeRatio < 0.9 or degreeRatio > 1.1")
)
nonTransferAirports.show()
# Join back to the `airports` DataFrame (instead of registering temp table as above)
transferAirports = (
    degreeRatio.join(airports, degreeRatio.id == airports.IATA)
    .selectExpr("id", "city", "degreeRatio")
    .filter("degreeRatio between 0.9 and 1.1")
)
transferAirports.orderBy("degreeRatio").limit(20).show()

"""
Breadth First Search

Breadth-first search (BFS) is designed to traverse the graph 
to quickly find the desired vertices (i.e. airports) and edges (i.e flights). 
Let's try to find the shortest number of connections between cities based on the dataset. 
Note, these examples do not take into account of time or distance, just hops between cities.
"""
# Direct San Francisco and Buffalo
filteredPaths = tripGraph.bfs(
    fromExpr="id = 'SFO'", toExpr="id = 'BUF'", maxPathLength=1
)
filteredPaths.show()
# Flying from San Francisco to Buffalo
filteredPaths = tripGraph.bfs(
    fromExpr="id = 'SFO'", toExpr="id = 'BUF'", maxPathLength=2
)
filteredPaths.show()
# Display most popular layover cities by descending count
(
    filteredPaths.groupBy("v1.id", "v1.City")
    .count()
    .orderBy(desc("count"))
    .limit(20)
    .show()
)

"""
Connected components

Computes the connected component membership of each vertex 
and returns a graph with each vertex assigned a component ID.
"""
# cc = tripGraph.connectedComponents()
# cc.select("id", "component").orderBy("component").show()

"""
Strongly connected components

Compute the strongly connected component (SCC) of each vertex 
and return a graph with each vertex assigned to the SCC containing that vertex.
"""
# scc = tripGraph.stronglyConnectedComponents(maxIter=10)
# scc.select("id", "component").orderBy("component").show()

"""
Label Propagation Algorithm (LPA)

Run static Label Propagation Algorithm for detecting communities in networks.

Each node in the network is initially assigned to its own community. 
At every superstep, nodes send their community affiliation to all neighbors 
and update their state to the mode community affiliation of incoming messages.

LPA is a standard community detection algorithm for graphs. 
It is very inexpensive computationally, although (1) convergence is not guaranteed 
and (2) one can end up with trivial solutions (all nodes are identified into a single community).
"""
# lpa = tripGraph.labelPropagation(maxIter=5)
# lpa.select("id", "label").show()

"""
Shortest paths

Computes shortest paths from each vertex to the given set of landmark vertices, 
where landmarks are specified by vertex ID. Note that this takes edge direction into account.
"""
shortest = tripGraph.shortestPaths(["SFO"])
shortest.select("id", "distances").show()

"""
Triangle count

Computes the number of triangles passing through each vertex.
"""
tc = tripGraph.triangleCount()
tc.select("id", "count").show()

# 计时结束
end = datetime.datetime.now()
print(f"开始时间：{start.strftime('%Y/%m/%d %H:%M:%S')}")
print(f"结束时间：{end.strftime('%Y/%m/%d %H:%M:%S')}")
print(f"程序运行时间：{(end - start).total_seconds()} s")
