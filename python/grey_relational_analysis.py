import pandas as pd


df = pd.DataFrame(
    data=[
        [2000, 1988, 386, 839, 763],
        [2001, 2061, 408, 846, 808],
        [2002, 2335, 422, 960, 953],
        [2003, 2750, 482, 1258, 1010],
        [2004, 3356, 511, 1577, 1268],
        [2005, 3806, 561, 1893, 1352],
    ],
    columns=["年份", "国内生产总值", "第一产业", "第二产业", "第三产业"],
)
print(df)

# 1. 确定母序列与子序列
def get_parent_child_sequence(df):
    parent_sequence = df.iloc[:, 1]
    child_sequence = df.iloc[:, 2:]
    return parent_sequence, child_sequence


def data_preprocessing(df):
    # 2. 序列进行预处理（序列的元素除以序列的均值）
    df.iloc[:, 1:] = df.iloc[:, 1:].apply(lambda x: x / x.mean())
    return df


def calc_corr(parent_sequence, child_sequence):
    # 3. 计算子序列与母序列的关联系数
    # 3.1 计算子序列与母序列对应元素的差值的绝对值
    child_sequence = child_sequence.apply(lambda x: (x - parent_sequence).abs())
    # 3.2 确定两级最小差a、两级最大差b
    a = child_sequence.min().min()
    b = child_sequence.max().max()
    # 3.3 计算关联系数
    child_sequence = child_sequence.applymap(lambda x: (a + 0.5 * b) / (x + 0.5 * b))
    return child_sequence


if __name__ == "__main__":
    df = data_preprocessing(df)
    print(df)
    parent_sequence, child_sequence = get_parent_child_sequence(df)
    child_sequence = calc_corr(parent_sequence, child_sequence)
    print(child_sequence)
    # 4. 计算子序列与母序列的关联度（子序列元素的关联系数的均值）
    corr = child_sequence.mean().values
    print(corr)
