import multiprocessing

import gevent.monkey

gevent.monkey.patch_all()

debug = True  # 调试模式
loglevel = "debug"  # 错误日志输出等级
bind = "0.0.0.0:8000"  # Gunicorn绑定服务器套接字，Host形式的字符串格式。
pidfile = "log/gunicorn.pid"  # 设置pid文件的文件名，如果不设置将不会创建pid文件。
accesslog = "log/access.log"  # 要写入的访问日志目录
errorlog = "log/error.log"  # 要写入的错误日志目录
daemon = True  # 守护Gunicorn进程，默认为False。
reload = True  # 代码更新时将重启工作，默认为False。此设置用于开发，每当应用程序发生更改时，都会导致工作重新启动。
workers = multiprocessing.cpu_count() * 2 + 1 # 用于处理工作进程的数量，为正整数，默认为1。worker推荐的数量为当前的CPU个数*2 + 1。
worker_class = "gevent"  # 要使用的工作模式，默认为sync。可引用以下常见类型作为捆绑类：gevent,eventlet,tornado,gthread,gaiohttp。
worker_connections = 2000  # 最大客户端并发数量，默认为1000。此设置将影响gevent和eventlet工作模式。
threads = 2  # 处理请求的工作线程数，使用指定数量的线程运行每个worker。为正整数，默认为1。
timeout = 6000  # 超时后工作将被杀掉，并重新启动。一般设定为30秒。
