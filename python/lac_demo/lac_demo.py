from LAC import LAC


# 选择使用默认的词法分析模型
lac = LAC()


# 定制化功能

# 装载干预词典, sep参数表示词典文件采用的分隔符，为None时默认使用空格或制表符'\t'
lac.load_customization("userdict.txt", sep=None)

# 干预后结果
custom_result = lac.run("北京市朝阳区东四环中路8号楼8层8609")
print(custom_result)


# 增量训练

## 样例数据

"""
LAC/nz 是/v 个/q 优秀/a 的/u 分词/n 工具/n 。/w
百度/ORG 是/v 一家/m 高科技/n 公司/n 。/w
春天/TIME 的/u 花开/v 秋天/TIME 的/u 风/n 以及/c 冬天/TIME 的/u 落阳/n 。/w
"""

# 训练和测试数据集，格式一致
train_file = "./data/lac_train.tsv"
test_file = "./data/lac_test.tsv"
lac.train(model_save_dir="./my_lac_model/", train_data=train_file, test_data=test_file)

# 使用自己训练好的模型
my_lac = LAC(model_path="my_lac_model")
