def execute(conn, inputs, params, outputs, reportFileName):
    '''
    载入模块
    '''
    import datetime
    import joblib
    import os
    import warnings

    import matplotlib.pyplot as plt
    import numpy as np
    import pandas as pd
    from lightgbm import LGBMClassifier
    from sklearn.metrics import auc, precision_recall_curve, roc_curve
    from sklearn.metrics import classification_report, confusion_matrix
    from sklearn.model_selection import GridSearchCV
    from sklearn.preprocessing import Binarizer, label_binarize
    from sklearn2pmml.pipeline import PMMLPipeline
    from sklearn2pmml import sklearn2pmml

    import db_utils
    import report_utils

    warnings.filterwarnings("ignore")
    plt.switch_backend('agg')
    report = report_utils.Report()

    '''
    选择目标数据
    '''
    data_in = db_utils.query(conn,
                             'select ' + params['features'] + ',' + params['label'] + ' from ' + inputs['data_in'])
    x_train = data_in.drop(params['label'], 1)
    y_train = data_in[params['label']]
    y_train = y_train.astype(str)
    class_names = y_train.unique()
    y_binarize = label_binarize(y_train, classes=class_names)  # 标签二值化

    def getparams(x):
        if x == 'None':
            return None
        elif type(eval(x)) == float:
            return float(x)
        elif type(eval(x)) == int:
            return int(x)
        else:
            return x

    '''
    构建LightGBM分类模型
    '''
    # boosting_type = getparams(params['boosting_type'])  # 会报错：NameError: name 'gbdt' is not defined
    n_estimators = getparams(params['n_estimators'])
    num_leaves = getparams(params['num_leaves'])
    max_depth = getparams(params['max_depth'])
    learning_rate = getparams(params['learning_rate'])
    min_split_gain = getparams(params['min_split_gain'])
    min_child_weight = getparams(params['min_child_weight'])
    min_child_samples = getparams(params['min_child_samples'])
    subsample = getparams(params['subsample'])
    subsample_freq = getparams(params['subsample_freq'])
    colsample_bytree = getparams(params['colsample_bytree'])
    reg_alpha = getparams(params['reg_alpha'])
    reg_lambda = getparams(params['reg_lambda'])
    random_state = getparams(params['random_state'])
    n_jobs = getparams(params['n_jobs'])
    if params['GridSearchCV'] == 'no':
        model = LGBMClassifier(
            boosting_type=params['boosting_type'],
            n_estimators=n_estimators,
            num_leaves=num_leaves,
            max_depth=max_depth,
            learning_rate=learning_rate,
            min_split_gain=min_split_gain,
            min_child_weight=min_child_weight,
            min_child_samples=min_child_samples,
            subsample=subsample,
            subsample_freq=subsample_freq,
            colsample_bytree=colsample_bytree,
            reg_alpha=reg_alpha,
            reg_lambda=reg_lambda,
            random_state=random_state,
            n_jobs=n_jobs
        )
        model.fit(x_train, y_train)
    else:
        parameters = {
            'n_estimators': [20, 60, 100],
            'num_leaves': [10, 30, 50, 70, 90],
            'max_depth': [3, 5, 8],
            'learning_rate': [0.01, 0.05, 0.1],
            'min_split_gain': [0, 0.1, 0.2],
            'min_child_samples': [10, 20, 30],
            'subsample': [0.5, 0.7, 0.9],
            'subsample_freq': [3, 4, 5],
            'colsample_bytree': [0.5, 0.7, 0.9],
        }
        lgb = LGBMClassifier(
            boosting_type=params['boosting_type'],
            min_child_weight=min_child_weight,
            reg_alpha=reg_alpha,
            reg_lambda=reg_lambda,
            random_state=random_state,
            n_jobs=n_jobs
        )
        cv_model = GridSearchCV(lgb, param_grid=parameters, scoring='accuracy', cv=3)
        cv_model.fit(x_train, y_train)
        model = cv_model.best_estimator_
    '''
    模型预测
    '''
    y_fit = model.predict(x_train)  # 用模型进行预测，返回预测值
    y_score = model.predict_proba(x_train)  # 返回一个数组，数组的元素依次是X预测为各个类别的概率值
    fit_label = pd.DataFrame(y_fit, columns=['predict_label'])
    '''
    输出预测值
    '''
    data_out = pd.concat([x_train, y_train, fit_label], axis=1)

    '''
    报告
    '''
    report.h1('LightGBM算法')

    '''
    模型参数
    '''
    if params['GridSearchCV'] == 'no':
        model_params = params
    else:
        model_params = model.get_params()
    a = pd.DataFrame([model_params.keys(), model_params.values()]).T
    a.columns = ['参数名称', '参数值']
    report.h3('模型参数')
    report.p("输出配置的参数以及参数的取值。")
    report.table(a)
    model_params = dict()
    model_params['模型分类'] = model.n_classes_
    model_params['模型特征数'] = model.n_features_in_
    a = pd.DataFrame([model_params.keys(), model_params.values()]).T
    a.columns = ['参数名称', '参数值']
    report.h3('模型属性')
    report.p("输出模型的属性信息。")
    report.table(a)

    a = pd.DataFrame([x_train.columns, np.around(model.feature_importances_, decimals=4)]).T
    a.columns = ['特征', 'feature importance']
    a = a.sort_values('feature importance', ascending=False)
    b = a.iloc[:20, :]
    report.p("输出模型的特征的重要性信息：")
    report.table(b)
    report.writeToHtml(reportFileName)
    a.index = [a['特征'].unique()]

    # 显示所有列
    pd.set_option('display.max_columns', None)
    # 显示所有行
    pd.set_option('display.max_rows', None)
    # 设置value的显示长度为100，默认为50
    pd.set_option('max_colwidth', 100)
    b.plot(kind='barh', figsize=(10, 6), ).get_figure()
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.tight_layout()
    plt.legend(loc='best')
    plt.savefig('bar')
    plt.show()
    report.image('bar.png')

    cm = confusion_matrix(y_train, fit_label)  # 混淆矩阵
    n_classes = len(cm)

    if n_classes == 2:
        TP = cm[0][0]
        FN = cm[0][1]
        FP = cm[1][0]
        TN = cm[1][1]
        acc = (TP + TN) / (TP + TN + FP + FN)
        precision = TP / (TP + FP)
        recall = TP / (TP + FN)
        f1 = 2 * precision * recall / (precision + recall)
        model_params = dict()
        model_params['accuracy'] = np.around(acc, decimals=2)
        model_params['precision'] = np.around(precision, decimals=2)
        model_params['recall'] = np.around(recall, decimals=2)
        model_params['f1'] = np.around(f1, decimals=2)
        a = pd.DataFrame([model_params.keys(), model_params.values()]).T
        a.columns = ['指标', '值']
        report.h3('模型评价指标')
        report.table(a)
        print(acc)
        print(precision)
        print(recall)
        print(f1)
    if n_classes > 2:
        binarizer = Binarizer(threshold=0.5)
        y_score = binarizer.transform(y_score)
        target_names = class_names
        a = classification_report(y_train, fit_label, target_names=target_names)
        b = a.split('\n')
        res = []
        for bb in b:
            if bb != '':
                z = []
                c = bb.split('  ')
                for cc in c:
                    if cc != '':
                        z.append(cc.strip())
                res.append(z)
        res_table = pd.DataFrame(res[1:])
        res_table.columns = ['index', 'precision', 'recall', 'f1-score', 'support']
        report.h3('模型评价指标')
        report.table(res_table)

    '''
    绘制混淆矩阵图
    '''
    plt.figure(figsize=(4, 4))
    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
    for x in range(len(cm)):
        for y in range(len(cm)):
            plt.annotate(cm[x, y], xy=(y, x),
                         size='large',
                         horizontalalignment='center',
                         verticalalignment='center')
    tick_marks = np.arange(len(class_names))
    plt.xticks(tick_marks, class_names)
    plt.yticks(tick_marks, class_names)
    plt.xlabel('Predict Label')  # 坐标轴标签
    plt.ylabel('True Label')  # 坐标轴标签
    plt.tight_layout()
    plt.savefig('cm_img.png')
    plt.show()
    report.h3('混淆矩阵')
    report.p("如下所示混淆矩阵图：")
    report.image('cm_img.png')

    '''
    绘制ROC曲线
    fpr：假正例率
    tpr：真正例率
    '''
    y_fit = label_binarize(y_fit, classes=class_names)
    if n_classes == 2:
        fpr, tpr, _ = roc_curve(y_binarize.ravel(), y_fit.ravel())
        roc_auc = auc(fpr, tpr)
        plt.figure(figsize=(8, 4))
        lw = 2
        plt.plot(fpr, tpr, label='ROC curve (area = %0.2f)' % roc_auc)
        plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
        plt.fill_between(fpr, tpr, alpha=0.2, color='b')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('ROC and AUC')
        plt.legend(loc="lower right")
        plt.savefig('roc.png')
        plt.show()
        report.h3('ROC图')
        report.p("如下图所示：AUC所占的面积是" + str(np.around(roc_auc, decimals=2)))
        report.image('roc.png')
    if n_classes == 2:
        fpr, tpr, _ = precision_recall_curve(y_binarize.ravel(), y_fit.ravel())
        pr_auc = auc(fpr, tpr)
        fpr[0] = 0
        plt.figure(figsize=(8, 4))
        lw = 2
        plt.plot(fpr, tpr, label='PR curve (area = %0.2f)' % pr_auc)
        plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
        plt.fill_between(fpr, tpr, alpha=0.2, color='b')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.0])
        plt.xlabel('precision')
        plt.ylabel('recall')
        plt.title('PR')
        plt.legend(loc="lower right")
        plt.savefig('pr.png')
        plt.show()
        report.h3('Precision-Recall图')
        report.image('pr.png')
    if n_classes > 2:
        fpr, tpr, thresholds = roc_curve(y_binarize.ravel(), y_fit.ravel())
        roc_auc = auc(fpr, tpr)
        plt.figure(figsize=(8, 4))
        lw = 2
        plt.plot(fpr, tpr, label='ROC curve (area = %0.2f)' % roc_auc)
        plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
        plt.fill_between(fpr, tpr, alpha=0.2, color='b')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('ROC and AUC')
        plt.legend(loc="lower right")
        plt.savefig('roc.png')
        plt.show()
        report.h3('ROC图')
        report.p("如下图所示：AUC所占的面积是" + str(np.around(roc_auc, decimals=2)))
        report.image('roc.png')

    '''
    保存模型
    '''
    date = datetime.datetime.now().strftime("%Y%m%d")
    name = outputs['data_out'].split('_')[0]
    if not os.path.exists('/tmp/pyserve/pkl'):
        os.mkdir('/tmp/pyserve/pkl')
    # joblib.dump(model, "/tmp/pyserve/pkl/{}_{}.pkl".format(name, date))
    if not os.path.exists('/tmp/pyserve/pmml'):
        os.mkdir('/tmp/pyserve/pmml')
    pipeline = PMMLPipeline([("classifier", model)])
    sklearn2pmml(pipeline, "/tmp/pyserve/pmml/{}_{}.pmml".format(name, date))

    '''
    生成报告
    '''
    report.writeToHtml(reportFileName)

    '''
    将结果写出
    '''
    db_utils.dbWriteTable(conn, outputs['data_out'], data_out)

    return model
