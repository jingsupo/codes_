# coding: utf-8

import matplotlib.pyplot as plt
import numpy as np


class PointBuilder:
    def __init__(self, figure):
        self.figure = figure
        self.cid = figure.canvas.mpl_connect('pick_event', self)

    def __call__(self, event):
        print('click', event)
        self.xs = list(event.artist.get_xdata())
        self.ys = list(event.artist.get_ydata())
        ind = event.ind[0]
        self.xs.remove(self.xs[ind])
        self.ys.remove(self.ys[ind])
        event.artist.set_data(self.xs, self.ys)
        self.figure.canvas.draw()


fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_title('click to delete point')
ax.plot(np.random.rand(100), 'o', picker=5)
PointBuilder(fig)

plt.show()
