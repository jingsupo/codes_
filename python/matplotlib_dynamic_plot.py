# coding: utf-8


import matplotlib.pyplot as plt
import numpy as np

fig, ax = plt.subplots()

data = np.random.randint(80, 100, size=(1000,))

y = []
a = 0
b = 100

for i in range(len(data)):
    y.append(data[i])
    ax.cla()
    ax.plot(y, label='test')
    ax.legend()
    ax.set_xlim(a, b)
    if i > 100:
        a += 1
        b += 1
    plt.pause(0.1)
