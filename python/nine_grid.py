# coding: utf-8

from itertools import permutations


def nine_grid():
    """
    九宫格，从整数1-9中抽取，使每行每列对角线的和都为15
    """

    nums = [i for i in range(1, 10)]
    # itertools模块中permutations方法：穷举参数中的元素，3表示3个元素一组，并过滤出一组内和为15的元素
    seq = [i for i in permutations(nums, 3) if sum(i) == 15]
    # 搜索行、列、对角线均为15的排列组合
    matrix = []
    for r1_1, r1_2, r1_3 in seq:
        for r2_1, r2_2, r2_3 in seq:
            for r3_1, r3_2, r3_3 in seq:
                if (r1_1 + r1_2 + r1_3 == 15
                        and r2_1 + r2_2 + r2_3 == 15
                        and r3_1 + r3_2 + r3_3 == 15
                        and r1_1 + r2_1 + r3_1 == 15
                        and r1_2 + r2_2 + r3_2 == 15
                        and r1_3 + r2_3 + r3_3 == 15
                        and r1_1 + r2_2 + r3_3 == 15
                        and r1_3 + r2_2 + r3_1 == 15):
                    r1 = [r1_1, r1_2, r1_3]
                    r2 = [r2_1, r2_2, r2_3]
                    r3 = [r3_1, r3_2, r3_3]
                    # 去重
                    if len(set(r1) & set(r2)) == 0:
                        if r1 not in matrix:
                            matrix = [r1, r2, r3]
                            print(matrix)
