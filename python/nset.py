# coding: utf-8

import numpy as np
from scipy.spatial.distance import cdist


def nset(matrix_d, obs_vector):
    """
    非线性状态估计技术(nonlinear state estimation technique, nset)是一种经典的模式识别技术，在工业界常用来解决异常检测问题。
    其基于海量历史高维样本向量形成的“记忆矩阵”并计算出“认知矩阵”，认知矩阵中蕴含了所有历史样本向量两两之间的某种“非线性模式”，
    最后只要计算输入样本向量与输出估计向量之间的相似性就可以评估样本异常情况。
    :param matrix_d: there are n samples and d features in matrix_d (n row vectors).
    :param obs_vector: matrix, row vectors.
    :return: est_vector: matrix, row vectors.
    """

    dist_d_d = cdist(matrix_d, matrix_d)
    d_dot_dist_d_d_inv = np.dot(matrix_d.T, np.linalg.inv(dist_d_d))
    obs_vector_row = obs_vector.shape[0]
    obs_vector_col = obs_vector.shape[1]
    est_vector = np.zeros((obs_vector_row, obs_vector_col))
    matrix_d_row = len(matrix_d)
    dist_d_obs = np.zeros((matrix_d_row, 1))
    for i in range(obs_vector_row):
        for j in range(matrix_d_row):
            dist_d_obs[j, 0] = np.linalg.norm(matrix_d[j, :] - obs_vector[i])
        est_vector[i, :] = np.dot(d_dot_dist_d_d_inv, dist_d_obs).T
    return est_vector
