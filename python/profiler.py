from line_profiler import LineProfiler

from ist_ner.istner import get_all, get_countries, get_high_risk_countries_regions

lp = LineProfiler()
lp.add_function(get_high_risk_countries_regions)
lp.add_function(get_countries)
lp_wrapper = lp(get_all)
data = lp_wrapper("text")
lp.print_stats()
