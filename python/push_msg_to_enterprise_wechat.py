# coding: utf-8

import datetime
import json
import os

import requests
from lxml import etree

corpid = os.environ.get('corpid')
secret = os.environ.get('corpsecret')


class WeChatPub:
    session = requests.session()

    def __init__(self):
        self.token = self.get_token()

    def get_token(self):
        url = f'https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={corpid}&corpsecret={secret}'
        resp = self.session.get(url)
        if resp.status_code != 200:
            print('request failed.')
            return
        content = json.loads(resp.content)
        if content['errcode'] != 0:
            print(content['errcode'], content['errmsg'])
            return
        return content['access_token']

    def send_msg(self, content):
        url = f'https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token={self.token}'
        header = {
            'Content-Type': 'application/json'
        }
        form_data = {
            'touser': '@all',
            'toparty': 'PartyID1|PartyID2',
            'totag': 'TagID1|TagID2',
            'msgtype': 'textcard',
            'agentid': 1000002,
            'textcard': {
                'title': '双色球开奖信息',
                'description': content,
                'url': 'https://www.cjcp.com.cn/kaijiang/ssq/',
                'btntxt': '更多'
            },
            'safe': 0
        }
        resp = self.session.post(url, data=json.dumps(form_data).encode('utf-8'), headers=header)
        if resp.status_code != 200:
            print('request failed.')
            return
        return json.loads(resp.content)

    def get_data(self):
        url = 'https://www.cjcp.com.cn/kaijiang/ssq/'
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) Chrome/63.0.3239.132 Safari/537.36',
            'Referer': url,
        }
        resp = self.session.get(url, headers=headers)
        if resp.status_code != 200:
            print('request failed.')
            return
        try:
            text = resp.text
            html = etree.HTML(text)
            kj_data = html.xpath('//div[@class="kj_data"]//span')
            num_div = html.xpath('//div[@class="num_div"]//span')
            periods = []
            nums = []
            for period in kj_data:
                periods.append(period.text)
            for num in num_div:
                nums.append(num.text)
            return periods, nums
        except Exception as e:
            print(e)
            return


def main():
    wechat = WeChatPub()
    now = datetime.datetime.now()
    now_str = now.strftime('%Y-%m-%d %H:%M:%S')
    data = wechat.get_data()
    if data:
        msg = f'''
        <div class=\"gray\">{now_str}</div>
        <div class=\"normal\">{data[0]}</div>
        <div class=\"highlight\">{data[1]}</div>
        '''
        wechat.send_msg(msg)


if __name__ == '__main__':
    main()
