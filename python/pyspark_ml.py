from pyspark.ml import Pipeline, PipelineModel
from pyspark.ml.evaluation import (
    MulticlassClassificationEvaluator,
    RegressionEvaluator,
    ClusteringEvaluator,
)
from pyspark.ml.feature import (
    HashingTF,
    Tokenizer,
    StringIndexer,
    VectorIndexer,
    IndexToString,
    MinMaxScaler,
)
from pyspark.ml.linalg import Vectors
from pyspark.sql import SparkSession
from pyspark.sql.functions import col
from pyspark.sql.types import Row

spark = SparkSession.builder.appName("spark").getOrCreate()
sc = spark.sparkContext
sc.setLogLevel("ERROR")


def pipeline_demo():
    from pyspark.ml.classification import LogisticRegression

    training = spark.createDataFrame(
        [
            (1.0, Vectors.dense([0.0, 1.1, 0.1])),
            (0.0, Vectors.dense([2.0, 1.0, -1.0])),
            (0.0, Vectors.dense([2.0, 1.3, 1.0])),
            (1.0, Vectors.dense([0.0, 1.2, -0.5])),
        ],
        ["label", "features"],
    )
    lr = LogisticRegression(maxIter=10, regParam=0.01)
    print("LogisticRegression parameters:\n" + lr.explainParams() + "\n")

    model1 = lr.fit(training)
    print("Model 1 was fit using parameters: ")
    print(model1.extractParamMap())

    paramMap = {lr.maxIter: 30}
    paramMap.update({lr.regParam: 0.1, lr.threshold: 0.55})  # type: ignore
    paramMap2 = {lr.probabilityCol: "myProbability"}  # type: ignore
    paramMapCombined = paramMap.copy()
    paramMapCombined.update(paramMap2)  # type: ignore

    model2 = lr.fit(training, paramMapCombined)
    print("Model 2 was fit using parameters: ")
    print(model2.extractParamMap())

    test = spark.createDataFrame(
        [
            (1.0, Vectors.dense([-1.0, 1.5, 1.3])),
            (0.0, Vectors.dense([3.0, 2.0, -0.1])),
            (1.0, Vectors.dense([0.0, 2.2, -1.5])),
        ],
        ["label", "features"],
    )
    prediction = model2.transform(test)
    result = prediction.select(
        "features", "label", "myProbability", "prediction"
    ).collect()
    for row in result:
        print(
            "features=%s, label=%s -> prob=%s, prediction=%s"
            % (row.features, row.label, row.myProbability, row.prediction)
        )

    training = spark.createDataFrame(
        [
            (0, "a b c d e spark", 1.0),
            (1, "b d", 0.0),
            (2, "spark f g h", 1.0),
            (3, "hadoop mapreduce", 0.0),
        ],
        ["id", "text", "label"],
    )
    tokenizer = Tokenizer(inputCol="text", outputCol="words")
    hashingTF = HashingTF(inputCol=tokenizer.getOutputCol(), outputCol="features")
    lr = LogisticRegression(maxIter=10, regParam=0.001)
    pipeline = Pipeline(stages=[tokenizer, hashingTF, lr])
    model = pipeline.fit(training)
    pipeline.write().overwrite().save("pipeline")
    PipelineModel.write(model).overwrite().save("model")

    test = spark.createDataFrame(
        [
            (4, "spark i j k"),
            (5, "l m n"),
            (6, "spark hadoop spark"),
            (7, "apache hadoop"),
        ],
        ["id", "text"],
    )
    prediction = model.transform(test)
    selected = prediction.select("id", "text", "probability", "prediction")
    for row in selected.collect():
        rid, text, prob, prediction = row  # type: ignore
        print(
            "(%d, %s) --> prob=%s, prediction=%f"
            % (rid, text, str(prob), prediction)  # type: ignore
        )


"""
Classification
"""


def lr_fit(lr, training):
    # Fit the model
    lrModel = lr.fit(training)

    # Print the coefficients and intercept for multinomial logistic regression
    print("Coefficients: \n" + str(lrModel.coefficientMatrix))
    print("Intercept: " + str(lrModel.interceptVector))

    trainingSummary = lrModel.summary

    # Obtain the objective per iteration
    objectiveHistory = trainingSummary.objectiveHistory
    print("objectiveHistory:")
    for objective in objectiveHistory:
        print(objective)
    return trainingSummary


def lrc_demo():
    """
    Logistic regression
    """
    from pyspark.ml.classification import LogisticRegression

    training = spark.read.format("libsvm").load(
        "../../data/mllib/sample_libsvm_data.txt"
    )
    training2 = spark.read.format("libsvm").load(
        "../../data/mllib/sample_multiclass_classification_data.txt"
    )

    # Binomial logistic regression
    lr = LogisticRegression(maxIter=10, regParam=0.3, elasticNetParam=0.8)
    mlr = LogisticRegression(
        maxIter=10, regParam=0.3, elasticNetParam=0.8, family="multinomial"
    )
    lr_fit(mlr, training)
    trainingSummary = lr_fit(lr, training)
    trainingSummary.roc.show()
    print("areaUnderROC: " + str(trainingSummary.areaUnderROC))
    fMeasure = trainingSummary.fMeasureByThreshold
    maxFMeasure = fMeasure.groupBy().max("F-Measure").select("max(F-Measure)").head()
    bestThreshold = (
        fMeasure.where(fMeasure["F-Measure"] == maxFMeasure["max(F-Measure)"])
        .select("threshold")
        .head()["threshold"]
    )
    lr.setThreshold(bestThreshold)

    # Multinomial logistic regression
    trainingSummary = lr_fit(lr, training2)
    # for multiclass, we can inspect metrics on a per-label basis
    print("False positive rate by label:")
    for i, rate in enumerate(trainingSummary.falsePositiveRateByLabel):
        print("label %d: %s" % (i, rate))

    print("True positive rate by label:")
    for i, rate in enumerate(trainingSummary.truePositiveRateByLabel):
        print("label %d: %s" % (i, rate))

    print("Precision by label:")
    for i, prec in enumerate(trainingSummary.precisionByLabel):
        print("label %d: %s" % (i, prec))

    print("Recall by label:")
    for i, rec in enumerate(trainingSummary.recallByLabel):
        print("label %d: %s" % (i, rec))

    print("F-measure by label:")
    for i, f in enumerate(trainingSummary.fMeasureByLabel()):
        print("label %d: %s" % (i, f))

    accuracy = trainingSummary.accuracy
    falsePositiveRate = trainingSummary.weightedFalsePositiveRate
    truePositiveRate = trainingSummary.weightedTruePositiveRate
    fMeasure = trainingSummary.weightedFMeasure()
    precision = trainingSummary.weightedPrecision
    recall = trainingSummary.weightedRecall
    print(
        "Accuracy: %s\nFPR: %s\nTPR: %s\nF-measure: %s\nPrecision: %s\nRecall: %s"
        % (accuracy, falsePositiveRate, truePositiveRate, fMeasure, precision, recall)
    )


def dtc_demo():
    """
    Decision tree classifier
    """
    from pyspark.ml.classification import DecisionTreeClassifier

    data = spark.read.format("libsvm").load("../../data/mllib/sample_libsvm_data.txt")
    labelIndexer = StringIndexer(inputCol="label", outputCol="indexedLabel").fit(data)
    featureIndexer = VectorIndexer(
        inputCol="features", outputCol="indexedFeatures", maxCategories=4
    ).fit(data)
    trainingData, testData = data.randomSplit([0.7, 0.3])
    dt = DecisionTreeClassifier(labelCol="indexedLabel", featuresCol="indexedFeatures")
    pipeline = Pipeline(stages=[labelIndexer, featureIndexer, dt])
    model = pipeline.fit(trainingData)
    predictions = model.transform(testData)
    predictions.select("prediction", "indexedLabel", "features").show(5)
    evaluator = MulticlassClassificationEvaluator(
        labelCol="indexedLabel", predictionCol="prediction", metricName="accuracy"
    )
    accuracy = evaluator.evaluate(predictions)
    print("Test Error = %g " % (1.0 - accuracy))
    treeModel = model.stages[2]
    print(treeModel)


def rfc_demo():
    """
    Random forest classifier
    """
    from pyspark.ml.classification import RandomForestClassifier

    data = spark.read.format("libsvm").load("../../data/mllib/sample_libsvm_data.txt")
    labelIndexer = StringIndexer(inputCol="label", outputCol="indexedLabel").fit(data)
    featureIndexer = VectorIndexer(
        inputCol="features", outputCol="indexedFeatures", maxCategories=4
    ).fit(data)
    trainingData, testData = data.randomSplit([0.7, 0.3])
    rf = RandomForestClassifier(
        labelCol="indexedLabel", featuresCol="indexedFeatures", numTrees=10
    )
    labelConverter = IndexToString(
        inputCol="prediction", outputCol="predictedLabel", labels=labelIndexer.labels
    )
    pipeline = Pipeline(stages=[labelIndexer, featureIndexer, rf, labelConverter])
    model = pipeline.fit(trainingData)
    predictions = model.transform(testData)
    predictions.select("predictedLabel", "label", "features").show(5)
    evaluator = MulticlassClassificationEvaluator(
        labelCol="indexedLabel", predictionCol="prediction", metricName="accuracy"
    )
    accuracy = evaluator.evaluate(predictions)
    print("Test Error = %g" % (1.0 - accuracy))
    rfModel = model.stages[2]
    print(rfModel)


def gbtc_demo():
    """
    Gradient-boosted tree classifier
    """
    from pyspark.ml.classification import GBTClassifier

    data = spark.read.format("libsvm").load("../../data/mllib/sample_libsvm_data.txt")
    labelIndexer = StringIndexer(inputCol="label", outputCol="indexedLabel").fit(data)
    featureIndexer = VectorIndexer(
        inputCol="features", outputCol="indexedFeatures", maxCategories=4
    ).fit(data)
    trainingData, testData = data.randomSplit([0.7, 0.3])
    gbt = GBTClassifier(
        labelCol="indexedLabel", featuresCol="indexedFeatures", maxIter=10
    )
    pipeline = Pipeline(stages=[labelIndexer, featureIndexer, gbt])
    model = pipeline.fit(trainingData)
    predictions = model.transform(testData)
    predictions.select("prediction", "indexedLabel", "features").show(5)
    evaluator = MulticlassClassificationEvaluator(
        labelCol="indexedLabel", predictionCol="prediction", metricName="accuracy"
    )
    accuracy = evaluator.evaluate(predictions)
    print("Test Error = %g" % (1.0 - accuracy))
    gbtModel = model.stages[2]
    print(gbtModel)


def mpc_demo():
    """
    Multilayer perceptron classifier
    """
    from pyspark.ml.classification import MultilayerPerceptronClassifier

    data = spark.read.format("libsvm").load(
        "../../data/mllib/sample_multiclass_classification_data.txt"
    )
    train, test = data.randomSplit([0.6, 0.4], 1234)
    layers = [4, 5, 4, 3]
    trainer = MultilayerPerceptronClassifier(
        maxIter=100, layers=layers, blockSize=128, seed=1234
    )
    model = trainer.fit(train)
    result = model.transform(test)
    evaluator = MulticlassClassificationEvaluator(metricName="accuracy")
    accuracy = evaluator.evaluate(result.select("prediction", "label"))
    print(f"Test set accuracy = {accuracy}")


def lsvc_demo():
    """
    Linear Support Vector Machine
    """
    from pyspark.ml.classification import LinearSVC

    training = spark.read.format("libsvm").load(
        "../../data/mllib/sample_libsvm_data.txt"
    )
    lsvc = LinearSVC(maxIter=10, regParam=0.1)
    lsvcModel = lsvc.fit(training)
    print("Coefficients: " + str(lsvcModel.coefficients))
    print("Intercept: " + str(lsvcModel.intercept))


def nbc_demo():
    """
    Naive Bayes classifiers
    """
    from pyspark.ml.classification import NaiveBayes

    data = spark.read.format("libsvm").load("../../data/mllib/sample_libsvm_data.txt")
    train, test = data.randomSplit([0.6, 0.4], 1234)
    nb = NaiveBayes(smoothing=1.0, modelType="multinomial")
    model = nb.fit(train)
    predictions = model.transform(test)
    predictions.show()
    evaluator = MulticlassClassificationEvaluator(
        labelCol="label", predictionCol="prediction", metricName="accuracy"
    )
    accuracy = evaluator.evaluate(predictions)
    print("Test set accuracy = " + str(accuracy))


def fmc_demo():
    """
    Factorization machines classifier
    """
    from pyspark.ml.classification import FMClassifier

    data = spark.read.format("libsvm").load("../../data/mllib/sample_libsvm_data.txt")
    labelIndexer = StringIndexer(inputCol="label", outputCol="indexedLabel").fit(data)
    featureScaler = MinMaxScaler(inputCol="features", outputCol="scaledFeatures").fit(
        data
    )
    trainingData, testData = data.randomSplit([0.7, 0.3])
    fm = FMClassifier(
        labelCol="indexedLabel", featuresCol="scaledFeatures", stepSize=0.001
    )
    pipeline = Pipeline(stages=[labelIndexer, featureScaler, fm])
    model = pipeline.fit(trainingData)
    predictions = model.transform(testData)
    predictions.select("prediction", "indexedLabel", "features").show(5)
    evaluator = MulticlassClassificationEvaluator(
        labelCol="indexedLabel", predictionCol="prediction", metricName="accuracy"
    )
    accuracy = evaluator.evaluate(predictions)
    print("Test set accuracy = %g" % accuracy)
    fmModel = model.stages[2]
    print("Factors: " + str(fmModel.factors))  # type: ignore
    print("Linear: " + str(fmModel.linear))  # type: ignore
    print("Intercept: " + str(fmModel.intercept))  # type: ignore


"""
Regression
"""


def lr_demo():
    """
    Linear regression
    """
    from pyspark.ml.regression import LinearRegression

    training = spark.read.format("libsvm").load(
        "../../data/mllib/sample_linear_regression_data.txt"
    )
    lr = LinearRegression(maxIter=10, regParam=0.3, elasticNetParam=0.8)
    lrModel = lr.fit(training)
    print("Coefficients: %s" % str(lrModel.coefficients))
    print("Intercept: %s" % str(lrModel.intercept))
    trainingSummary = lrModel.summary
    print("numIterations: %d" % trainingSummary.totalIterations)
    print("objectiveHistory: %s" % str(trainingSummary.objectiveHistory))
    trainingSummary.residuals.show()
    print("RMSE: %f" % trainingSummary.rootMeanSquaredError)
    print("r2: %f" % trainingSummary.r2)


def glr_demo():
    """
    Generalized linear regression
    """
    from pyspark.ml.regression import GeneralizedLinearRegression

    data = spark.read.format("libsvm").load(
        "../../data/mllib/sample_linear_regression_data.txt"
    )
    glr = GeneralizedLinearRegression(
        family="gaussian", link="identity", maxIter=10, regParam=0.3
    )
    model = glr.fit(data)
    print("Coefficients: " + str(model.coefficients))
    print("Intercept: " + str(model.intercept))
    summary = model.summary
    print("Coefficient Standard Errors: " + str(summary.coefficientStandardErrors))
    print("T Values: " + str(summary.tValues))
    print("P Values: " + str(summary.pValues))
    print("Dispersion: " + str(summary.dispersion))
    print("Null Deviance: " + str(summary.nullDeviance))
    print(
        "Residual Degree Of Freedom Null: " + str(summary.residualDegreeOfFreedomNull)
    )
    print("Deviance: " + str(summary.deviance))
    print("Residual Degree Of Freedom: " + str(summary.residualDegreeOfFreedom))
    print("AIC: " + str(summary.aic))
    print("Deviance Residuals: ")
    summary.residuals().show()


def dtr_demo():
    """
    Decision tree regression
    """
    from pyspark.ml.regression import DecisionTreeRegressor

    data = spark.read.format("libsvm").load("../../data/mllib/sample_libsvm_data.txt")
    featureIndexer = VectorIndexer(
        inputCol="features", outputCol="indexedFeatures", maxCategories=4
    ).fit(data)
    trainingData, testData = data.randomSplit([0.7, 0.3])
    dt = DecisionTreeRegressor(featuresCol="indexedFeatures")
    pipeline = Pipeline(stages=[featureIndexer, dt])
    model = pipeline.fit(trainingData)
    predictions = model.transform(testData)
    predictions.select("prediction", "label", "features").show(5)
    evaluator = RegressionEvaluator(
        labelCol="label", predictionCol="prediction", metricName="rmse"
    )
    rmse = evaluator.evaluate(predictions)
    print("Root Mean Squared Error (RMSE) on test data = %g" % rmse)
    treeModel = model.stages[1]
    print(treeModel)


def rfr_demo():
    """
    Random forest regression
    """
    from pyspark.ml.regression import RandomForestRegressor

    data = spark.read.format("libsvm").load("../../data/mllib/sample_libsvm_data.txt")
    featureIndexer = VectorIndexer(
        inputCol="features", outputCol="indexedFeatures", maxCategories=4
    ).fit(data)
    trainingData, testData = data.randomSplit([0.7, 0.3])
    rf = RandomForestRegressor(featuresCol="indexedFeatures")
    pipeline = Pipeline(stages=[featureIndexer, rf])
    model = pipeline.fit(trainingData)
    predictions = model.transform(testData)
    predictions.select("prediction", "label", "features").show(5)
    evaluator = RegressionEvaluator(
        labelCol="label", predictionCol="prediction", metricName="rmse"
    )
    rmse = evaluator.evaluate(predictions)
    print("Root Mean Squared Error (RMSE) on test data = %g" % rmse)
    rfModel = model.stages[1]
    print(rfModel)


def gbtr_demo():
    """
    Gradient-boosted tree regression
    """
    from pyspark.ml.regression import GBTRegressor

    data = spark.read.format("libsvm").load("../../data/mllib/sample_libsvm_data.txt")
    featureIndexer = VectorIndexer(
        inputCol="features", outputCol="indexedFeatures", maxCategories=4
    ).fit(data)
    trainingData, testData = data.randomSplit([0.7, 0.3])
    gbt = GBTRegressor(featuresCol="indexedFeatures", maxIter=10)
    pipeline = Pipeline(stages=[featureIndexer, gbt])
    model = pipeline.fit(trainingData)
    predictions = model.transform(testData)
    predictions.select("prediction", "label", "features").show(5)
    evaluator = RegressionEvaluator(
        labelCol="label", predictionCol="prediction", metricName="rmse"
    )
    rmse = evaluator.evaluate(predictions)
    print("Root Mean Squared Error (RMSE) on test data = %g" % rmse)
    gbtModel = model.stages[1]
    print(gbtModel)


def aft_demo():
    """
    Survival regression
    Accelerated failure time (AFT) model
    """
    from pyspark.ml.regression import AFTSurvivalRegression

    training = spark.createDataFrame(
        [
            (1.218, 1.0, Vectors.dense(1.560, -0.605)),
            (2.949, 0.0, Vectors.dense(0.346, 2.158)),
            (3.627, 0.0, Vectors.dense(1.380, 0.231)),
            (0.273, 1.0, Vectors.dense(0.520, 1.151)),
            (4.199, 0.0, Vectors.dense(0.795, -0.226)),
        ],
        ["label", "censor", "features"],
    )
    quantileProbabilities = [0.3, 0.6]
    aft = AFTSurvivalRegression(
        quantileProbabilities=quantileProbabilities, quantilesCol="quantiles"
    )
    model = aft.fit(training)
    print("Coefficients: " + str(model.coefficients))
    print("Intercept: " + str(model.intercept))
    print("Scale: " + str(model.scale))
    model.transform(training).show(truncate=False)


def fmr_demo():
    """
    Factorization machines regressor
    """
    from pyspark.ml.regression import FMRegressor

    data = spark.read.format("libsvm").load("../../data/mllib/sample_libsvm_data.txt")
    featureScaler = MinMaxScaler(inputCol="features", outputCol="scaledFeatures").fit(
        data
    )
    trainingData, testData = data.randomSplit([0.7, 0.3])
    fm = FMRegressor(featuresCol="scaledFeatures", stepSize=0.001)
    pipeline = Pipeline(stages=[featureScaler, fm])
    model = pipeline.fit(trainingData)
    predictions = model.transform(testData)
    predictions.select("prediction", "label", "features").show(5)
    evaluator = RegressionEvaluator(
        labelCol="label", predictionCol="prediction", metricName="rmse"
    )
    rmse = evaluator.evaluate(predictions)
    print("Root Mean Squared Error (RMSE) on test data = %g" % rmse)
    fmModel = model.stages[1]
    print("Factors: " + str(fmModel.factors))  # type: ignore
    print("Linear: " + str(fmModel.linear))  # type: ignore
    print("Intercept: " + str(fmModel.intercept))  # type: ignore


"""
Clustering
"""


def kmeans_demo():
    """
    K-means
    """
    from pyspark.ml.clustering import KMeans

    data = spark.read.format("libsvm").load("../../data/mllib/sample_kmeans_data.txt")
    kmeans = KMeans(k=2, seed=1)
    model = kmeans.fit(data)
    predictions = model.transform(data)
    evaluator = ClusteringEvaluator()
    score = evaluator.evaluate(predictions)
    print("Silhouette with squared euclidean distance = " + str(score))
    centers = model.clusterCenters()
    print("Cluster Centers: ")
    for center in centers:
        print(center)


def gmm_demo():
    """
    Gaussian Mixture Model (GMM)
    """
    from pyspark.ml.clustering import GaussianMixture

    data = spark.read.format("libsvm").load("../../data/mllib/sample_libsvm_data.txt")
    gmm = GaussianMixture(k=2, seed=1)
    model = gmm.fit(data)
    print("Gaussians shown as a DataFrame: ")
    model.gaussiansDF.show()
    for i in range(model.getK()):
        print(
            f"Gaussian {i}:\nweight={model.weights[i]}\n"
            + f"mu={model.gaussians[i].mean}\nsigma=\n{model.gaussians[i].cov}\n"
        )


"""
Locality Sensitive Hashing
"""


def brp_demo():
    """
    Bucketed Random Projection for Euclidean Distance
    """
    from pyspark.ml.feature import BucketedRandomProjectionLSH

    dataA = [
        (
            0,
            Vectors.dense([1.0, 1.0]),
        ),
        (
            1,
            Vectors.dense([1.0, -1.0]),
        ),
        (
            2,
            Vectors.dense([-1.0, -1.0]),
        ),
        (
            3,
            Vectors.dense([-1.0, 1.0]),
        ),
    ]
    dfA = spark.createDataFrame(dataA, ["id", "features"])

    dataB = [
        (
            4,
            Vectors.dense([1.0, 0.0]),
        ),
        (
            5,
            Vectors.dense([-1.0, 0.0]),
        ),
        (
            6,
            Vectors.dense([0.0, 1.0]),
        ),
        (
            7,
            Vectors.dense([0.0, -1.0]),
        ),
    ]
    dfB = spark.createDataFrame(dataB, ["id", "features"])

    key = Vectors.dense([1.0, 0.0])

    brp = BucketedRandomProjectionLSH(
        inputCol="features", outputCol="hashes", bucketLength=2.0, numHashTables=3
    )
    model = brp.fit(dfA)

    # Feature Transformation
    print("The hashed dataset where hashed values are stored in the column 'hashes':")
    model.transform(dfA).show()

    # Compute the locality sensitive hashes for the input rows, then perform approximate
    # similarity join.
    # We could avoid computing hashes by passing in the already-transformed dataset, e.g.
    # `model.approxSimilarityJoin(transformedA, transformedB, 1.5)`
    print("Approximately joining dfA and dfB on Euclidean distance smaller than 1.5:")
    model.approxSimilarityJoin(dfA, dfB, 1.5, distCol="EuclideanDistance").select(
        col("datasetA.id").alias("idA"),
        col("datasetB.id").alias("idB"),
        col("EuclideanDistance"),
    ).show()

    # Compute the locality sensitive hashes for the input rows, then perform approximate nearest
    # neighbor search.
    # We could avoid computing hashes by passing in the already-transformed dataset, e.g.
    # `model.approxNearestNeighbors(transformedA, key, 2)`
    print("Approximately searching dfA for 2 nearest neighbors of the key:")
    model.approxNearestNeighbors(dfA, key, 2).show()


def mh_demo():
    """
    MinHash for Jaccard Distance
    """
    from pyspark.ml.feature import MinHashLSH

    dataA = [
        (
            0,
            Vectors.sparse(6, [0, 1, 2], [1.0, 1.0, 1.0]),
        ),
        (
            1,
            Vectors.sparse(6, [2, 3, 4], [1.0, 1.0, 1.0]),
        ),
        (
            2,
            Vectors.sparse(6, [0, 2, 4], [1.0, 1.0, 1.0]),
        ),
    ]
    dfA = spark.createDataFrame(dataA, ["id", "features"])

    dataB = [
        (
            3,
            Vectors.sparse(6, [1, 3, 5], [1.0, 1.0, 1.0]),
        ),
        (
            4,
            Vectors.sparse(6, [2, 3, 5], [1.0, 1.0, 1.0]),
        ),
        (
            5,
            Vectors.sparse(6, [1, 2, 4], [1.0, 1.0, 1.0]),
        ),
    ]
    dfB = spark.createDataFrame(dataB, ["id", "features"])

    key = Vectors.sparse(6, [1, 3], [1.0, 1.0])

    mh = MinHashLSH(inputCol="features", outputCol="hashes", numHashTables=5)
    model = mh.fit(dfA)

    # Feature Transformation
    print("The hashed dataset where hashed values are stored in the column 'hashes':")
    model.transform(dfA).show()

    # Compute the locality sensitive hashes for the input rows, then perform approximate
    # similarity join.
    # We could avoid computing hashes by passing in the already-transformed dataset, e.g.
    # `model.approxSimilarityJoin(transformedA, transformedB, 0.6)`
    print("Approximately joining dfA and dfB on distance smaller than 0.6:")
    model.approxSimilarityJoin(dfA, dfB, 0.6, distCol="JaccardDistance").select(
        col("datasetA.id").alias("idA"),
        col("datasetB.id").alias("idB"),
        col("JaccardDistance"),
    ).show()

    # Compute the locality sensitive hashes for the input rows, then perform approximate nearest
    # neighbor search.
    # We could avoid computing hashes by passing in the already-transformed dataset, e.g.
    # `model.approxNearestNeighbors(transformedA, key, 2)`
    # It may return less than 2 rows when not enough approximate near-neighbor candidates are
    # found.
    print("Approximately searching dfA for 2 nearest neighbors of the key:")
    model.approxNearestNeighbors(dfA, key, 2).show()


"""
Collaborative Filtering
"""


def als_demo():
    """
    alternating least squares (ALS)
    """
    from pyspark.ml.recommendation import ALS

    lines = spark.read.text("../../data/mllib/als/sample_movielens_ratings.txt").rdd
    parts = lines.map(lambda row: row.value.split("::"))
    ratingsRDD = parts.map(
        lambda p: Row(
            userId=int(p[0]), movieId=int(p[1]), rating=float(p[2]), timestamp=int(p[3])
        )
    )
    ratings = spark.createDataFrame(ratingsRDD)
    (training, test) = ratings.randomSplit([0.8, 0.2])

    # Build the recommendation model using ALS on the training data
    # Note we set cold start strategy to 'drop' to ensure we don't get NaN evaluation metrics
    als = ALS(
        maxIter=5,
        regParam=0.01,
        userCol="userId",
        itemCol="movieId",
        ratingCol="rating",
        coldStartStrategy="drop",
    )
    model = als.fit(training)

    # Evaluate the model by computing the RMSE on the test data
    predictions = model.transform(test)
    evaluator = RegressionEvaluator(
        metricName="rmse", labelCol="rating", predictionCol="prediction"
    )
    rmse = evaluator.evaluate(predictions)
    print("Root-mean-square error = " + str(rmse))

    # Generate top 10 movie recommendations for each user
    userRecs = model.recommendForAllUsers(10)
    # Generate top 10 user recommendations for each movie
    movieRecs = model.recommendForAllItems(10)

    # Generate top 10 movie recommendations for a specified set of users
    users = ratings.select(als.getUserCol()).distinct().limit(3)
    userSubsetRecs = model.recommendForUserSubset(users, 10)
    # Generate top 10 user recommendations for a specified set of movies
    movies = ratings.select(als.getItemCol()).distinct().limit(3)
    movieSubSetRecs = model.recommendForItemSubset(movies, 10)

    print(userRecs.show())
    print(movieRecs.show())
    print(userSubsetRecs.show())
    print(movieSubSetRecs.show())


if __name__ == "__main__":
    pass
