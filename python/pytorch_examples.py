# coding: utf-8

import torch
from torch.nn import functional as F
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, datasets

"""
PyTorch构建模型基本步骤

1、输入处理模块 (X 输入数据，变成网络能够处理的Tensor类型)

2、模型构建模块 (主要负责从输入的数据，得到预测的y^, 这就是我们经常说的前向过程)

3、定义损失函数和优化器模块 (注意，前向过程只会得到模型预测的结果，并不会自动求导和更新，由这个模块进行处理)

4、构建训练过程
"""


def train(epoch):
    running_loss = 0.0
    for batch_idx, (inputs, target) in enumerate(train_loader):
        # 如果需要在GPU上计算，执行以下操作
        inputs, target = inputs.to(device), target.to(device)  # Send the inputs and targets at every step to the GPU.

        outputs = model(inputs)  # 前向传播
        loss = criterion(outputs, target)  # 计算损失函数
        optimizer.zero_grad()  # 梯度清零
        loss.backward()  # 反向传播
        optimizer.step()  # 更新训练参数

        running_loss += loss.item()
        if batch_idx % 300 == 299:
            print(f'[{epoch + 1}, {batch_idx + 1}] loss: {running_loss / 300}')
            running_loss = 0.0


def test():
    correct = 0
    total = 0
    with torch.no_grad():
        for inputs, target in test_loader:
            # 如果需要在GPU上计算，执行以下操作
            inputs, target = inputs.to(device), target.to(
                device)  # Send the inputs and targets at every step to the GPU.

            outputs = model(inputs)
            _, predicted = torch.max(outputs.data, dim=1)
            total += target.size(0)
            correct += (predicted == target).sum().item()
    print(f'Accuracy on test dataset: {100 * correct / total} %')


class Net(torch.nn.Module):
    """
    全连接神经网络
    """

    def __init__(self):
        super(Net, self).__init__()
        self.linear1 = torch.nn.Linear(784, 512)
        self.linear2 = torch.nn.Linear(512, 256)
        self.linear3 = torch.nn.Linear(256, 128)
        self.linear4 = torch.nn.Linear(128, 64)
        self.linear5 = torch.nn.Linear(64, 10)
        self.activate = torch.nn.ReLU()

    def forward(self, x):
        x = x.view(-1, 784)
        x = self.activate(self.linear1(x))
        x = self.activate(self.linear2(x))
        x = self.activate(self.linear3(x))
        x = self.activate(self.linear4(x))
        return self.linear5(x)


class Cnn(torch.nn.Module):
    """
    卷积神经网络
    """

    def __init__(self):
        super(Cnn, self).__init__()
        self.conv1 = torch.nn.Conv2d(1, 10, kernel_size=(5, 5))
        self.conv2 = torch.nn.Conv2d(10, 20, kernel_size=(5, 5))
        self.pooling = torch.nn.MaxPool2d(2)
        self.fc = torch.nn.Linear(320, 10)
        self.activate = torch.nn.ReLU()

    def forward(self, x):
        in_size = x.size(0)
        x = self.pooling(self.activate(self.conv1(x)))  # 卷积->激活->池化
        x = self.pooling(self.activate(self.conv2(x)))
        x = x.view(in_size, -1)  # 展平
        return self.fc(x)  # 全连接


class InceptionModule(torch.nn.Module):
    """
    InceptionModule
    """

    def __init__(self, in_channels):
        super(InceptionModule, self).__init__()
        self.branch_pool = torch.nn.Conv2d(in_channels, 24, kernel_size=(1, 1))
        self.branch1x1 = torch.nn.Conv2d(in_channels, 16, kernel_size=(1, 1))
        self.branch5x5_1 = torch.nn.Conv2d(in_channels, 16, kernel_size=(1, 1))
        self.branch5x5_2 = torch.nn.Conv2d(16, 24, kernel_size=(5, 5), padding=(2, 2))
        self.branch3x3_1 = torch.nn.Conv2d(in_channels, 16, kernel_size=(1, 1))
        self.branch3x3_2 = torch.nn.Conv2d(16, 24, kernel_size=(3, 3), padding=(1, 1))
        self.branch3x3_3 = torch.nn.Conv2d(24, 24, kernel_size=(3, 3), padding=(1, 1))

    def forward(self, x):
        branch_pool = F.avg_pool2d(x, kernel_size=3, stride=1, padding=1)
        branch_pool = self.branch_pool(branch_pool)

        branch1x1 = self.branch1x1(x)

        branch5x5 = self.branch5x5_1(x)
        branch5x5 = self.branch5x5_2(branch5x5)

        branch3x3 = self.branch3x3_1(x)
        branch3x3 = self.branch3x3_2(branch3x3)
        branch3x3 = self.branch3x3_3(branch3x3)

        outputs = [branch_pool, branch1x1, branch5x5, branch3x3]
        return torch.cat(outputs, dim=1)


class GoogleNet(torch.nn.Module):
    """
    GoogleNet
    """

    def __init__(self):
        super(GoogleNet, self).__init__()
        self.conv1 = torch.nn.Conv2d(1, 10, kernel_size=(5, 5))
        self.conv2 = torch.nn.Conv2d(88, 20, kernel_size=(5, 5))
        self.inception1 = InceptionModule(10)
        self.inception2 = InceptionModule(20)
        self.pooling = torch.nn.MaxPool2d(2)
        self.fc = torch.nn.Linear(1408, 10)
        self.activate = torch.nn.ReLU()

    def forward(self, x):
        in_size = x.size(0)
        x = self.pooling(self.activate(self.conv1(x)))  # 卷积->激活->池化
        x = self.inception1(x)
        x = self.pooling(self.activate(self.conv2(x)))
        x = self.inception2(x)
        x = x.view(in_size, -1)  # 展平
        return self.fc(x)  # 全连接


class ResidualBlock(torch.nn.Module):
    """
    ResidualBlock
    """

    def __init__(self, channels):
        super(ResidualBlock, self).__init__()
        self.conv1 = torch.nn.Conv2d(channels, channels, kernel_size=(3, 3), padding=(1, 1))
        self.conv2 = torch.nn.Conv2d(channels, channels, kernel_size=(3, 3), padding=(1, 1))
        self.activate = torch.nn.ReLU()

    def forward(self, x):
        y = self.activate(self.conv1(x))
        y = self.conv2(y)
        return self.activate(x + y)


class ResidualNet(torch.nn.Module):
    """
    ResidualNet
    """

    def __init__(self):
        super(ResidualNet, self).__init__()
        self.conv1 = torch.nn.Conv2d(1, 16, kernel_size=(5, 5))
        self.conv2 = torch.nn.Conv2d(16, 32, kernel_size=(5, 5))
        self.residual1 = ResidualBlock(16)
        self.residual2 = ResidualBlock(32)
        self.pooling = torch.nn.MaxPool2d(2)
        self.fc = torch.nn.Linear(512, 10)
        self.activate = torch.nn.ReLU()

    def forward(self, x):
        in_size = x.size(0)
        x = self.pooling(self.activate(self.conv1(x)))  # 卷积->激活->池化
        x = self.residual1(x)
        x = self.pooling(self.activate(self.conv2(x)))
        x = self.residual2(x)
        x = x.view(in_size, -1)  # 展平
        return self.fc(x)  # 全连接


class RnnCell(torch.nn.Module):
    def __init__(self, input_size, hidden_size, batch_size):
        super(RnnCell, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.batch_size = batch_size
        self.rnn_cell = torch.nn.RNNCell(input_size=self.input_size, hidden_size=self.hidden_size)

    def forward(self, x, hidden):
        return self.rnn_cell(x, hidden)

    def init_hidden(self):
        return torch.zeros(self.batch_size, self.hidden_size)


class Rnn(torch.nn.Module):
    def __init__(self, input_size, hidden_size, batch_size, num_layers=1):
        super(Rnn, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.batch_size = batch_size
        self.num_layers = num_layers
        self.rnn = torch.nn.RNN(input_size=self.input_size, hidden_size=self.hidden_size, num_layers=self.num_layers)

    def forward(self, x):
        hidden = torch.zeros(self.num_layers, self.batch_size, self.hidden_size)
        x, _ = self.rnn(x, hidden)
        return x.view(-1, self.hidden_size)


class RnnWithEmbedding(torch.nn.Module):
    def __init__(self, input_size, hidden_size, embedding_size, num_class, num_layers=1):
        super(RnnWithEmbedding, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.embedding_size = embedding_size
        self.num_class = num_class
        self.emb = torch.nn.Embedding(num_embeddings=self.input_size, embedding_dim=self.embedding_size)
        self.rnn = torch.nn.RNN(input_size=self.embedding_size, hidden_size=self.hidden_size,
                                num_layers=self.num_layers, batch_first=True)
        self.fc = torch.nn.Linear(self.hidden_size, self.num_class)

    def forward(self, x):
        hidden = torch.zeros(self.num_layers, x.size(0), self.hidden_size)
        x = self.emb(x)
        x, _ = self.rnn(x, hidden)
        x = self.fc(x)
        return x.view(-1, self.num_class)


if __name__ == '__main__':
    # batch_size = 64
    # transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize(0.1307, 0.3081)])
    # train_dataset = datasets.MNIST(root='./dataset/', train=True, transform=transform, download=True)
    # train_loader = DataLoader(dataset=train_dataset, batch_size=batch_size, shuffle=True)
    # test_dataset = datasets.MNIST(root='./dataset/', train=False, transform=transform, download=True)
    # test_loader = DataLoader(dataset=test_dataset, batch_size=batch_size, shuffle=False)

    """
    全连接神经网络
    """
    # model = Net()
    """
    卷积神经网络
    """
    # model = Cnn()
    """
    GoogleNet
    """
    # model = GoogleNet()
    """
    ResidualNet
    """
    # model = ResidualNet()
    # # 如果需要在GPU上计算，执行以下操作
    # device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    # model.to(device)
    #
    # criterion = torch.nn.CrossEntropyLoss()
    # optimizer = torch.optim.SGD(model.parameters(), lr=0.01, momentum=0.5)
    #
    # for i in range(10):
    #     train(i)
    #     test()

    """
    卷积神经网络运行结果：
    [1, 300] loss: 0.5568680834273497
    [1, 600] loss: 0.1743188677728176
    [1, 900] loss: 0.12835460629935067
    Accuracy on test dataset: 96.93 %
    [2, 300] loss: 0.10213683488157889
    [2, 600] loss: 0.09746773090058317
    [2, 900] loss: 0.08723123197754225
    Accuracy on test dataset: 97.86 %
    [3, 300] loss: 0.07604994861409069
    [3, 600] loss: 0.06991400639526546
    [3, 900] loss: 0.07153443168383092
    Accuracy on test dataset: 98.29 %
    [4, 300] loss: 0.06755980266490952
    [4, 600] loss: 0.05499130927336713
    [4, 900] loss: 0.05590225168969482
    Accuracy on test dataset: 98.38 %
    [5, 300] loss: 0.05032998224254698
    [5, 600] loss: 0.05542529703505958
    [5, 900] loss: 0.04969777257880196
    Accuracy on test dataset: 98.52 %
    [6, 300] loss: 0.050742812536967295
    [6, 600] loss: 0.04312789685364502
    [6, 900] loss: 0.04447588006791193
    Accuracy on test dataset: 98.71 %
    [7, 300] loss: 0.044873132495364794
    [7, 600] loss: 0.04387794251049248
    [7, 900] loss: 0.03812874572002329
    Accuracy on test dataset: 98.62 %
    [8, 300] loss: 0.03781602852861397
    [8, 600] loss: 0.04015733652127286
    [8, 900] loss: 0.03948958160510908
    Accuracy on test dataset: 98.84 %
    [9, 300] loss: 0.03339868507153976
    [9, 600] loss: 0.037081154936458915
    [9, 900] loss: 0.03827982437253619
    Accuracy on test dataset: 98.85 %
    [10, 300] loss: 0.03281791350260998
    [10, 600] loss: 0.035134007055700446
    [10, 900] loss: 0.034206007634444785
    Accuracy on test dataset: 98.83 %

    GoogleNet运行结果：
    [1, 300] loss: 0.9095823983848095
    [1, 600] loss: 0.20675982090334097
    [1, 900] loss: 0.13977628343117734
    Accuracy on test dataset: 96.95 %
    [2, 300] loss: 0.11372088224937518
    [2, 600] loss: 0.09559154926799238
    [2, 900] loss: 0.08474381099765499
    Accuracy on test dataset: 97.93 %
    [3, 300] loss: 0.07923733928706497
    [3, 600] loss: 0.0685637303100278
    [3, 900] loss: 0.07217141398073484
    Accuracy on test dataset: 98.27 %
    [4, 300] loss: 0.06802520933871468
    [4, 600] loss: 0.05384923214325681
    [4, 900] loss: 0.06179889791373474
    Accuracy on test dataset: 98.48 %
    [5, 300] loss: 0.05514650969222809
    [5, 600] loss: 0.0519385503209196
    [5, 900] loss: 0.054526740383977694
    Accuracy on test dataset: 98.7 %
    [6, 300] loss: 0.048575950713517765
    [6, 600] loss: 0.04881852086711054
    [6, 900] loss: 0.04543361682522421
    Accuracy on test dataset: 98.71 %
    [7, 300] loss: 0.04476576266577467
    [7, 600] loss: 0.04323415815907841
    [7, 900] loss: 0.042968628708816445
    Accuracy on test dataset: 98.77 %
    [8, 300] loss: 0.036100168261521806
    [8, 600] loss: 0.04259236993539768
    [8, 900] loss: 0.03894320846787499
    Accuracy on test dataset: 98.87 %
    [9, 300] loss: 0.035699767662445085
    [9, 600] loss: 0.037414317640553536
    [9, 900] loss: 0.03685202104000685
    Accuracy on test dataset: 98.84 %
    [10, 300] loss: 0.03519523521652445
    [10, 600] loss: 0.0306122595219252
    [10, 900] loss: 0.03336188001461172
    Accuracy on test dataset: 98.87 %

    ResidualNet运行结果：
    [1, 300] loss: 0.5152363207191228
    [1, 600] loss: 0.14642431207001208
    [1, 900] loss: 0.11156049423230191
    Accuracy on test dataset: 97.2 %
    [2, 300] loss: 0.08426960989211997
    [2, 600] loss: 0.07585615321838607
    [2, 900] loss: 0.06650364223712435
    Accuracy on test dataset: 98.21 %
    [3, 300] loss: 0.05658576269090797
    [3, 600] loss: 0.05682132948267584
    [3, 900] loss: 0.05544893245678395
    Accuracy on test dataset: 98.59 %
    [4, 300] loss: 0.0443599789418901
    [4, 600] loss: 0.045876569028090064
    [4, 900] loss: 0.04771309138314488
    Accuracy on test dataset: 98.78 %
    [5, 300] loss: 0.03892711760165791
    [5, 600] loss: 0.03706008038444755
    [5, 900] loss: 0.04072096246605118
    Accuracy on test dataset: 98.84 %
    [6, 300] loss: 0.03308676072396338
    [6, 600] loss: 0.03396882824677353
    [6, 900] loss: 0.037685825873243935
    Accuracy on test dataset: 99.06 %
    [7, 300] loss: 0.02814949997991789
    [7, 600] loss: 0.03190337732085027
    [7, 900] loss: 0.03132997091044672
    Accuracy on test dataset: 99.01 %
    [8, 300] loss: 0.023563141147218025
    [8, 600] loss: 0.027652051872767817
    [8, 900] loss: 0.030048229959502352
    Accuracy on test dataset: 98.91 %
    [9, 300] loss: 0.024435708613697593
    [9, 600] loss: 0.025226614369118276
    [9, 900] loss: 0.026481845228408928
    Accuracy on test dataset: 99.16 %
    [10, 300] loss: 0.024643485847627745
    [10, 600] loss: 0.022287492007550706
    [10, 900] loss: 0.02283373565034708
    Accuracy on test dataset: 98.93 %
    """

    """
    train a model to learn: 'hello' -> 'ohlol'
    """

    idx2char = ['e', 'h', 'l', 'o']
    x_data = [1, 0, 2, 2, 3]
    y_data = [3, 1, 2, 3, 2]
    one_hot_lookup = [[1, 0, 0, 0],
                      [0, 1, 0, 0],
                      [0, 0, 1, 0],
                      [0, 0, 0, 1]]
    x_one_hot = [one_hot_lookup[x] for x in x_data]

    epochs = 15

    input_size = 4
    hidden_size = 4
    batch_size = 1
    num_layers = 1
    seq_len = 5

    """
    RnnCell
    """

    inputs = torch.Tensor(x_one_hot).view(-1, batch_size, input_size)
    labels = torch.LongTensor(y_data).view(-1, 1)

    model = RnnCell(input_size, hidden_size, batch_size)
    criterion = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=0.1)

    for epoch in range(epochs):
        loss = 0
        optimizer.zero_grad()  # 梯度清零
        hidden = model.init_hidden()
        print('Predicted: ', end='')
        for inp, label in zip(inputs, labels):
            hidden = model(inp, hidden)
            loss += criterion(hidden, label)
            _, idx = hidden.max(dim=1)
            print(idx2char[idx.item()], end='')
        loss.backward()  # 反向传播
        optimizer.step()  # 更新训练参数
        print(f', Epoch [{epoch + 1}/{epochs}] loss={loss.item()}')

    """
    Rnn
    """

    inputs = torch.Tensor(x_one_hot).view(seq_len, batch_size, input_size)
    labels = torch.LongTensor(y_data)

    model = Rnn(input_size, hidden_size, batch_size, num_layers)
    criterion = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=0.1)

    for epoch in range(epochs):
        outputs = model(inputs)
        loss = criterion(outputs, labels)
        optimizer.zero_grad()  # 梯度清零
        loss.backward()  # 反向传播
        optimizer.step()  # 更新训练参数

        _, idx = outputs.max(dim=1)
        idx = idx.data.numpy()
        print('Predicted: ', ''.join([idx2char[x] for x in idx]), end='')
        print(f', Epoch [{epoch + 1}/{epochs}] loss={loss.item()}')

    """
    RnnWithEmbedding
    """

    num_class = 4
    input_size = 4
    hidden_size = 8
    embedding_size = 10
    batch_size = 1
    num_layers = 2
    seq_len = 5

    idx2char = ['e', 'h', 'l', 'o']
    x_data = [[1, 0, 2, 2, 3]]  # (batch_size, seq_len)
    y_data = [3, 1, 2, 3, 2]  # (batch_size * seq_len)

    inputs = torch.LongTensor(x_data)
    labels = torch.LongTensor(y_data)

    model = RnnWithEmbedding(input_size, hidden_size, embedding_size, num_class, num_layers)
    criterion = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=0.1)

    for epoch in range(epochs):
        outputs = model(inputs)
        loss = criterion(outputs, labels)
        optimizer.zero_grad()  # 梯度清零
        loss.backward()  # 反向传播
        optimizer.step()  # 更新训练参数

        _, idx = outputs.max(dim=1)
        idx = idx.data.numpy()
        print('Predicted: ', ''.join([idx2char[x] for x in idx]), end='')
        print(f', Epoch [{epoch + 1}/{epochs}] loss={loss.item()}')
