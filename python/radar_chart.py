# coding: utf-8

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

np.random.seed(666)
n = 50
a = 0.5 * np.random.random(n) + 0.5
b = 0.5 * np.random.random(n) + 0.5
c = (a + b) / 2

df = pd.DataFrame()
df['参数名称'] = [f'参数{i}' for i in range(1, n + 1)]
df['最大值'] = np.where(a > b, a, b)
df['最小值'] = np.where(a < b, a, b)
df['中心值'] = c

# 标签
labels = df['参数名称']
# 数据
data = np.array(df[['最大值', '最小值', '中心值']])
# 数据个数
data_length = len(df)

labels = np.concatenate((labels, [labels[0]]))  # 闭合
data = np.concatenate((data, [data[0]]))  # 闭合
angles = np.linspace(0, 2 * np.pi, data_length, endpoint=False)
angles = np.concatenate((angles, [angles[0]]))  # 闭合

fig = plt.figure(figsize=(16, 10))
ax = fig.add_subplot(111, polar=True)  # polar参数
ax.plot(angles, data, 'o-', linewidth=2)  # 画线
ax.fill(angles, data, facecolor='r', alpha=0.25)  # 填充
ax.set_thetagrids(angles * 180 / np.pi, labels, fontproperties="SimHei")
ax.set_title("设备运行参数雷达图", va='bottom', fontproperties="SimHei")
ax.set_rlim(0, 1)
ax.grid(True)
plt.legend(labels=('最大值', '最小值', '中心值'), loc='best', bbox_to_anchor=(1.1, 1.1), prop={'family': 'SimHei'})
plt.show()
