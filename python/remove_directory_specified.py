import os
import shutil

top = r'D:\program\python'

dirs_to_remove = []
for root, dirs, files in os.walk(top):
    for name in dirs:
        if name in ['.ipynb_checkpoints', '__pycache__']:
            dirs_to_remove.append(os.path.join(root, name))

for path in dirs_to_remove:
    print(path)
    shutil.rmtree(path)
