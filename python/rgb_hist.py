# coding: utf-8

import cv2
import matplotlib.pyplot as plt
import numpy as np


def rgb_hist(filepath):
    """
    OpenCV RGB颜色直方图
    """

    img = cv2.imread(filepath)
    plt.imshow(np.flip(img, axis=2))
    plt.axis('off')
    plt.show()
    channels = cv2.split(img)  # BGR通道分离
    colors = ('b', 'g', 'r')  # 彩色图有三个通道, 通道(b:0, g:1, r:2)
    plt.figure()
    plt.title('RGB Histogram')
    plt.xlabel('Bins')
    plt.ylabel('# of Pixels')
    for i in [0, 1, 2]:
        histogram = cv2.calcHist([img], [i], None, [256], [0, 256])
        plt.plot(histogram, colors[i])
        plt.xlim([0, 256])
    plt.show()

    cv2.waitKey(0)
    cv2.destroyAllWindows()
