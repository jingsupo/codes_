# coding: utf-8

def simhash_demo(texts):
    import simhash as sh
    import spacy
    from datasketch import MinHash, MinHashLSH

    threshold = 0.8
    lsh = MinHashLSH(threshold=threshold, num_perm=128)

    mh_list = []
    for i in range(len(texts)):
        mh_list.append(MinHash(num_perm=128))

    for i, text in enumerate(texts.values()):
        for txt in text:
            mh_list[i].update(txt.encode('utf8'))
        lsh.insert('v' + str(i), mh_list[i])

    result = lsh.query(mh_list[0])
    print(f'Approximate neighbours with Jaccard similarity > {threshold}', result)

    sh_result = []
    for key, value in texts.items():
        sh_result.append((key, sh.Simhash(texts[1]).distance(sh.Simhash(value))))

    nlp_lg = spacy.load('en_core_web_lg')
    result_lg = []
    for key, value in texts.items():
        result_lg.append((key, nlp_lg(texts[1]).similarity(nlp_lg(value))))
