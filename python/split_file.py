# coding: utf-8

import os


def split_file(file, num=100, output='out'):
    """
    分而治之/哈希映射
    """

    if not os.path.exists(output):
        os.mkdir(output)

    # 文件对象存放列表
    file_list = []
    # 创建num个文件对象放入列表
    for i in range(num):
        file_list.append(open(os.path.join(output, f'{i}.txt'), 'a'))
    # 读取大文件
    with open(file, 'r') as f:
        for line in f:
            # 求每行内容的哈希值的模
            m = hash(line) % num
            # 将每行内容写入哈希值的模对应的文件
            file_list[m].write(line)
    # 关闭所有文件对象
    for f in file_list:
        f.close()
