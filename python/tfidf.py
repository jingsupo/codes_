import jieba
from gensim import corpora, models, similarities

# 将地址库存到列表中
all_place = []

# 对列表中的地址进行分词
all_place_list = []
for place in all_place:
    place_list = [word for word in jieba.cut(place)]
    all_place_list.append(place_list)

# 利用地址库生成字典
dictionary = corpora.Dictionary(all_place_list)
# dictionary.keys() 查看字典中的编号
# dictionary.token2id 查看编号与词的对应关系
# 利用doc2bow制作语料库
corpus = [dictionary.doc2bow(doc) for doc in all_place_list]

# 使用TF—IDF对语料库进行建模
tfidf = models.TfidfModel(corpus)

# 索引矩阵
index = similarities.SparseMatrixSimilarity(
    tfidf[corpus], num_features=len(dictionary.keys())
)

test_place = "北京市海淀区万寿路28号院"
print(test_place)

# 对测试地址进行分词
test_place_list = [word for word in jieba.cut(test_place)]

# 将测试地址转换为稀疏矩阵
test_place_vec = dictionary.doc2bow(test_place_list)
# 获取测试地址每个词的TF—IDF值
# tfidf[test_place_vec]

# 获取相似度
sim = index[tfidf[test_place_vec]]
print(sim)
