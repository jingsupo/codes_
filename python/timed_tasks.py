# coding: utf-8

import schedule


def job(_name):
    print(f"My domain name is {_name}")


name = "jingsupo.com"

schedule.every(10).seconds.do(job, name)  # 每隔10秒钟执行一次任务
schedule.every(10).minutes.do(job, name)  # 每隔10分钟执行一次任务
schedule.every().hour.do(job, name)  # 每隔1小时执行一次任务
schedule.every().day.at("10:30").do(job, name)  # 每天10:30执行一次任务
schedule.every(5).to(10).days.do(job, name)  # 每隔5到10天执行一次任务
schedule.every().monday.do(job, name)  # 每周一执行一次任务
schedule.every().wednesday.at("20:20").do(job, name)  # 每周三20:20执行一次任务

while True:
    schedule.run_pending()  # run_pending：运行所有可以运行的任务
