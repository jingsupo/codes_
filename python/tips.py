# coding: utf-8

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import itertools
from io import FileIO
from itertools import groupby
from operator import itemgetter

# python
import importlib
importlib.reload(module)  # reload module
itertools.chain.from_iterable(condition1)  # flatten list

# matplotlib
plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False

# numpy
np.set_printoptions(threshold=np.inf, suppress=True)  # threshold=np.inf full output, suppress=True cancel Scientific notation

# pandas
pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.float_format', lambda x: '%.3f' % x)  # cancel Scientific notation

df1 = pd.DataFrame(np.random.randint(0, 10, size=(100, 3)))
df2 = pd.DataFrame(np.random.randint(10, 20, size=(100, 3)))
writer = pd.ExcelWriter('demo.xlsx')
df1.to_excel(writer, sheet_name='df1', index=False)
df2.to_excel(writer, sheet_name='df2', index=False)
writer.save()

# Permutation and Combination
itertools.permutations(lst, 3)
itertools.combinations(lst, 3)
itertools.product(lst1, lst2)


def find_consecutive_duplicate_values(arr, condition='<= 1', target=0, num=5):
    a = np.array(arr)
    a = np.where(eval(f'a {condition}'), target, a)
    result = [tuple(map(itemgetter(0), g)) for i, g in groupby(enumerate(a), key=itemgetter(1)) if i == target]
    result = [x for x in result if len(x) >= num]
    return result


def find_consecutive_numbers(arr):
    result = [tuple(map(itemgetter(1), g)) for i, g in groupby(enumerate(arr), key=lambda x: x[1] - x[0])]
    return result


def list_bisection(lst, size):
    from math import ceil

    if size <= 0:
        return [lst]
    return [lst[i * size:(i + 1) * size] for i in range(ceil(len(lst) / size))]


def list_bisection(lst, size):
    if size <= 0:
        return [lst]
    return [lst[i:i + size] for i in range(len(lst) - 1)]


file: FileIO = open("tb.log", "a")


def tb(e: Exception, file: FileIO) -> None:
    import traceback

    file.write(f"**********\n{traceback.format_exc()}**********\n")
    # traceback.print_exc(file=file)

    err = f"{e}\t|\t"
    err += f"{e.__traceback__.tb_frame.f_code.co_filename}\t|\t"
    err += f"{e.__traceback__.tb_lineno}\t|\t"
    err += f"{e.__traceback__.tb_frame.f_code.co_name}\n"
    file.write(err)
