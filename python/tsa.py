import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn import svm
from sklearn.ensemble import IsolationForest
from statsmodels.tsa.api import seasonal_decompose, ExponentialSmoothing, STL
import torch

from common import scale_data


def oc_svm():
    df = pd.read_csv('./dataset/tsa/train.csv', sep=',')
    dft = pd.read_csv('./dataset/tsa/test.csv', sep=',')
    df['timestamp'] = range(len(df))
    dft['timestamp'] = range(len(dft))

    data = df['value'].values.reshape(-1, 1)
    data_test = dft['value'].values.reshape(-1, 1)

    data = data[288:] - data[:-288]
    data_test = data_test[288:] - data_test[:-288]

    model = svm.OneClassSVM(nu=0.1, kernel='rbf', gamma=0.01)
    model.fit(data)
    pre_y = model.predict(data_test)

    # rng = np.random.RandomState(42)
    # model = IsolationForest(behaviour='new', max_samples=100, random_state=rng, contamination='auto')
    # model.fit(data)
    # pre_y = model.predict(data)

    df1 = dft.copy()
    df1 = df1[288:]
    df1['class'] = pre_y

    df2 = df1[df1['class'] == 1]
    df3 = df1[df1['class'] == -1]

    plt.figure()
    plt.scatter(df2['timestamp'], df2['value'])
    plt.scatter(df3['timestamp'], df3['value'])
    plt.show()


class LSTM(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.lstm = torch.nn.LSTM(2, 6, 2)  # 输入数据2个特征维度，6个隐藏层维度，2个LSTM串联，第2个LSTM接收第1个的计算结果
        self.fc = torch.nn.Linear(6, 1)  # 线性拟合，接收数据的维度为6，输出数据的维度为1

    def forward(self, x):
        x, _ = self.lstm(x)
        a, b, c = x.shape
        out = self.fc(x.view(-1, c))  # 因为线性层输入的是二维数据，所以应该将LSTM输出的三维数据调整成二维数据，最后的特征维度不能变
        return out.view(a, b, -1)  # 因为是循环神经网络，最后要把二维调整成三维，下一次循环使用


def lstm():
    df = pd.read_csv('./dataset/tsa/train.csv', sep=',')
    dft = pd.read_csv('./dataset/tsa/test.csv', sep=',')
    df['timestamp'] = range(len(df))
    dft['timestamp'] = range(len(dft))

    # 一、数据准备

    train_data = df.value.values
    test_data = dft.value.values

    # 归一化处理，这一步必不可少，不然后面训练数据误差会很大，模型没法用

    train_data = scale_data(train_data.reshape(-1, 1))
    test_data = scale_data(test_data.reshape(-1, 1))

    # 数据集和目标值赋值，dataset为数据，look_back为以几行数据为特征维度数量

    def create_dataset(dataset, look_back):
        data_x = []
        data_y = []
        for i in range(len(dataset) - look_back):
            data_x.append(dataset[i:i + look_back])
            data_y.append(dataset[i + look_back])
        return np.asarray(data_x), np.asarray(data_y)

    # 以2为特征维度，得到数据集

    dataX, dataY = create_dataset(train_data, 2)

    train_size = int(len(dataX) * 0.7)

    x_train = dataX[:train_size]  # 训练数据
    y_train = dataY[:train_size]  # 训练数据目标值

    x_train = x_train.reshape(-1, 1, 2)  # 将训练数据调整成pytorch中lstm算法的输入维度
    y_train = y_train.reshape(-1, 1, 1)  # 将目标值调整成pytorch中lstm算法的输出维度

    # 将ndarray数据转换为张量，因为pytorch用的数据类型是张量

    x_train = torch.from_numpy(x_train)
    y_train = torch.from_numpy(y_train)

    # 二、构建模型

    model = LSTM()

    # 参数寻优，计算损失函数

    optimizer = torch.optim.Adam(model.parameters(), lr=0.02)
    loss_func = torch.nn.MSELoss()

    # 三、训练模型

    model_path = 'rnn.pth'  # 训练模型保存路径

    if not os.path.exists(model_path):
        for i in range(100):
            var_x = torch.autograd.Variable(x_train).type(torch.FloatTensor)
            var_y = torch.autograd.Variable(y_train).type(torch.FloatTensor)
            out = model(var_x)
            loss = loss_func(out, var_y)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            if (i + 1) % 10 == 0:
                print('Epoch:{}, Loss:{:.5f}'.format(i + 1, loss.item()))

        torch.save(model.state_dict(), model_path)

    # 四、模型测试

    # 准备测试数据

    if os.path.exists(model_path):
        model.load_state_dict(torch.load(model_path))
        model.eval()

    dataX, dataY = create_dataset(test_data, 2)
    dataX = dataX.reshape((-1, 1, 2))
    dataX = torch.from_numpy(dataX)
    var_dataX = torch.autograd.Variable(dataX).type(torch.FloatTensor)

    pred = model(var_dataX)
    pred = pred.view(-1).data.numpy()  # 转换成一维的ndarray数据，这是预测值

    # 五、画图检验

    plt.plot(pred, 'r', label='prediction')
    plt.plot(dataY, 'b', label='real')
    plt.legend(loc='best')
    plt.show()


def tsd():
    df = pd.read_csv('./dataset/tsa/train.csv', sep=',')
    dft = pd.read_csv('./dataset/tsa/test.csv', sep=',')

    # data['value'] = data['value'].apply(pd.to_numeric, errors='ignore')
    # data['value'].interpolate(inplace=True)

    train_data = df.value.values
    test_data = dft.value.values

    """
    时间序列分解算法：Seasonal decomposition using moving averages.
    """
    sd = seasonal_decompose(train_data, period=288)
    sd2 = seasonal_decompose(test_data, period=288)
    fig = sd.plot()
    fig2 = sd2.plot()
    fig.set_size_inches(12, 8)
    fig2.set_size_inches(12, 8)
    fig.set_tight_layout(True)
    fig2.set_tight_layout(True)

    """
    时间序列分解算法：Seasonal decomposition using exponential smoothing.
    """
    es = ExponentialSmoothing(train_data,
                              seasonal_periods=288,
                              trend="add",
                              seasonal="add",
                              initialization_method='estimated').fit()
    es2 = ExponentialSmoothing(test_data,
                               seasonal_periods=288,
                               trend="add",
                               seasonal="add",
                               initialization_method='estimated').fit()

    def plot(x, y, model):
        figure, ax_arr = plt.subplots(4)
        figure.set_size_inches(12, 8)

        pd.Series(data=y, index=x).plot(ax=ax_arr[0], color="b", linestyle='-')
        ax_arr[0].set_title("Original sequence")

        pd.Series(data=model.level, index=x).plot(ax=ax_arr[1], color="c", linestyle='-')
        ax_arr[1].set_title("Trend section")

        pd.Series(data=model.season, index=x).plot(ax=ax_arr[2], color="g", linestyle='-')
        ax_arr[2].set_title("Seasonal section")

        pd.Series(data=model.resid, index=x).plot(ax=ax_arr[3], color="r", linestyle='-')
        ax_arr[3].set_title("Residual section")

        plt.tight_layout(pad=0.5, w_pad=0.5, h_pad=2.0)

    plot(df['value'].index, df['value'].values, es)
    plot(dft['value'].index, dft['value'].values, es2)

    """
    时间序列分解算法：Season-Trend decomposition using LOESS.
    """
    stl = STL(train_data, period=288).fit()
    stl2 = STL(test_data, period=288).fit()
    fig = stl.plot()
    fig2 = stl2.plot()
    fig.set_size_inches(12, 8)
    fig2.set_size_inches(12, 8)
    fig.set_tight_layout(True)
    fig2.set_tight_layout(True)

    plt.show()


if __name__ == '__main__':
    pass
    tsd()
