import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import (
    accuracy_score,
    f1_score,
    precision_score,
    recall_score,
)
from sklearn.model_selection import cross_val_score


def start_spark(app_name="pyspark", master="local[1]", options=None):
    """
    以下为可用配置项：
        driver.memory ：driver运行内存，默认值512m，一般2-6G
        num-executors ：集群中启动的executor总数
        executor.memory ：每个executor分配的内存数，默认值512m，一般4-8G
        executor.cores ：每个executor分配的核心数目

        yarn.am.memory ：AppMaster内存，默认值512m
        yarn.am.memoryOverhead ：am堆外内存，值为 AM memory * 0.07, 最小384
        yarn.driver.memoryOverhead ：driver堆外内存，Max(384MB, 7% of spark.executor.memory)
        yarn.executor.memoryOverhead ：executor堆外内存，值为 executorMemory * 0.07, 最小384

        每个executor从Yarn请求的内存 = executor.memory + yarn.executor.memoryOverhead

        'hadoop_conf_dir': '/opt/hadoop-3.3.0/etc/hadoop',
        'spark.jars': '/root/lib/ojdbc8.jar, /root/lib/mysql-connector-java-5.1.50.jar',
        'spark.submit.deployMode': 'client',
        'spark.executor.instances': '30',
        'spark.executor.cores': '3',
        'spark.executor.memory': '4G',
        'spark.driver.memory': '4G',
        'spark.dynamicAllocation.enabled': 'true',
    """
    import os
    import sys
    import pyspark
    from pyspark.sql import SparkSession

    executable = sys.executable
    # os.environ["JAVA_HOME"] = "C:\\Program Files\\Java\\jdk1.8.0_202"
    os.environ["SPARK_HOME"] = pyspark.__path__[0]
    os.environ["PYSPARK_PYTHON"] = executable
    os.environ["PYSPARK_DRIVER_PYTHON"] = executable

    builder = SparkSession.builder.appName(app_name).master(master)
    if options and isinstance(options, dict):
        for k, v in options.items():
            builder.config(k, v)
    spark = builder.enableHiveSupport().getOrCreate()

    return spark


def evaluate(train, train_labels, test, test_labels):
    """Evaluate a training dataset with a standard sklearn model"""

    # Use the same model for each training set for now
    model = RandomForestClassifier(n_estimators=100,
                                   random_state=50, n_jobs=-1)

    feature_names = list(train.columns)

    train = imputer(train)
    test = imputer(test)

    cv_score = 1 * cross_val_score(model, train, train_labels,
                                   scoring="f1_micro",
                                   cv=5)

    # Fit on the training data and make predictions
    model.fit(train, train_labels)
    preds = model.predict(test)

    # Calculate the performance
    f1 = f1_score(test_labels, preds, average='micro')
    print('5-fold CV F1: {:.2f} with std: {:.2f}'.format(cv_score.mean(), cv_score.std()))
    print('Test F1: {:.2f}.'.format(f1))

    feature_importances = pd.DataFrame({'feature': feature_names,
                                        'importance': model.feature_importances_})

    return model, preds, feature_importances


def plot_feature_importances(df, n=15, threshold=None):
    """Plots n most important features. Also plots the cumulative importance if
    threshold is specified and prints the number of features needed to reach threshold cumulative importance.
    Intended for use with any tree-based feature importances.

    Args:
        df (dataframe): Dataframe of feature importances. Columns must be "feature" and "importance".

        n (int): Number of most important features to plot. Default is 15.

        threshold (float): Threshold for cumulative importance plot. If not provided, no plot is made. Default is None.

    Returns:
        df (dataframe): Dataframe ordered by feature importances with a normalized column (sums to 1)
                        and a cumulative importance column

    Note:

        * Normalization in this case means sums to 1.
        * Cumulative importance is calculated by summing features from most to least important
        * A threshold of 0.9 will show the most important features needed to reach 90% of cumulative importance

    """

    # Sort features with most important at the head
    df = df.sort_values('importance', ascending=False).reset_index(drop=True)

    # Normalize the feature importances to add up to one and calculate cumulative importance
    df['importance_normalized'] = df['importance'] / df['importance'].sum()
    df['cumulative_importance'] = np.cumsum(df['importance_normalized'])

    plt.rcParams['font.size'] = 12

    # Bar plot of n most important features
    df.loc[:n, :].plot.barh(y='importance_normalized',
                            x='feature', color='blue',
                            edgecolor='k', figsize=(12, 8),
                            legend=False)

    plt.xlabel('Normalized Importance', size=18)
    plt.ylabel('')
    plt.title(f'Top {n} Most Important Features', size=18)
    plt.gca().invert_yaxis()

    if threshold:
        # Cumulative importance plot
        plt.figure(figsize=(8, 6))
        plt.plot(list(range(len(df))), df['cumulative_importance'], 'b-')
        plt.xlabel('Number of Features', size=16)
        plt.ylabel('Cumulative Importance', size=16)
        plt.title('Cumulative Feature Importance', size=18)

        # Number of features needed for threshold cumulative importance
        # This is the index (will need to add 1 for the actual number)
        importance_index = np.min(np.where(df['cumulative_importance'] > threshold))

        # Add vertical line to plot
        plt.vlines(importance_index + 1, ymin=0, ymax=1.05, linestyles='--', colors='red')
        plt.show()

        print('{} features required for {:.0f}% of cumulative importance.'.format(importance_index + 1,
                                                                                  100 * threshold))

    return df


def feature_selection(feature_matrix, missing_threshold=90, correlation_threshold=0.95):
    """Feature selection for a dataframe."""

    # feature_matrix = pd.get_dummies(feature_matrix)
    # n_features_start = feature_matrix.shape[1]
    print('Original shape: ', feature_matrix.shape)

    # Find missing and percentage
    missing = pd.DataFrame(feature_matrix.isnull().sum())
    missing['percent'] = 100 * (missing[0] / feature_matrix.shape[0])
    missing.sort_values('percent', ascending=False, inplace=True)

    # Missing above threshold
    missing_cols = list(missing[missing['percent'] > missing_threshold].index)
    n_missing_cols = len(missing_cols)

    # Remove missing columns
    feature_matrix = feature_matrix[[x for x in feature_matrix if x not in missing_cols]]
    print('{} missing columns with threshold: {}.'.format(n_missing_cols,
                                                          missing_threshold))

    # Zero variance
    unique_counts = pd.DataFrame(feature_matrix.nunique()).sort_values(0, ascending=True)
    zero_variance_cols = list(unique_counts[unique_counts[0] == 1].index)
    n_zero_variance_cols = len(zero_variance_cols)

    # Remove zero variance columns
    feature_matrix = feature_matrix[[x for x in feature_matrix if x not in zero_variance_cols]]
    print('{} zero variance columns.'.format(n_zero_variance_cols))

    # Correlations
    corr_matrix = feature_matrix.corr()

    # Extract the upper triangle of the correlation matrix
    upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(np.bool))

    # Select the features with correlations above the threshold
    # Need to use the absolute value
    to_drop = [column for column in upper.columns if any(upper[column].abs() > correlation_threshold)]

    n_collinear = len(to_drop)

    feature_matrix = feature_matrix[[x for x in feature_matrix if x not in to_drop]]
    print('{} collinear columns removed with threshold: {}.'.format(n_collinear,
                                                                    correlation_threshold))

    total_removed = n_missing_cols + n_zero_variance_cols + n_collinear

    print('Total columns removed: ', total_removed)
    print('Shape after feature selection: {}.'.format(feature_matrix.shape))
    return feature_matrix


def encoding_categorical_features(df):
    from sklearn.preprocessing import OneHotEncoder

    ohe = OneHotEncoder(sparse=False)
    df_to_trans = df.select_dtypes(include=["object", "category"])
    # df_to_trans = df_to_trans.fillna("nan")
    # df_to_trans = df_to_trans.applymap(lambda x: "nan" if x == "" else x)
    # df_to_trans = df_to_trans.applymap(lambda x: str(x))
    cols_to_trans = df_to_trans.columns
    transformed = pd.DataFrame(ohe.fit_transform(df_to_trans))
    return cols_to_trans, transformed


def imputer(df):
    from sklearn.impute import SimpleImputer

    df = df.replace({np.inf: np.nan, -np.inf: np.nan})
    # Impute the missing values
    imputer = SimpleImputer(strategy='median')
    df = imputer.fit_transform(df)
    return df


def get_metrics(y_true, y_pred):
    print("accuracy score: ", accuracy_score(y_true=y_true, y_pred=y_pred))
    print("precision score: ", precision_score(y_true=y_true, y_pred=y_pred))
    print("recall score: ", recall_score(y_true=y_true, y_pred=y_pred))
    print("f1 score: ", f1_score(y_true=y_true, y_pred=y_pred))


def classification_report(y_true, y_pred):
    from sklearn.metrics import classification_report

    return classification_report(y_true, y_pred)


def confusion_matrix_from_estimator(estimator, X, y):
    from sklearn.metrics import ConfusionMatrixDisplay

    X = imputer(X)
    return ConfusionMatrixDisplay.from_estimator(estimator, X, y)


def confusion_matrix_from_predictions(y_true, y_pred):
    from sklearn.metrics import ConfusionMatrixDisplay

    return ConfusionMatrixDisplay.from_predictions(y_true, y_pred)


def roc_curve_from_estimator(estimator, X, y):
    from sklearn.metrics import RocCurveDisplay

    X = imputer(X)
    return RocCurveDisplay.from_estimator(estimator, X, y)


def roc_curve_from_predictions(y_true, y_pred):
    from sklearn.metrics import RocCurveDisplay

    return RocCurveDisplay.from_predictions(y_true, y_pred)


def precision_recall_curve_from_estimator(estimator, X, y):
    from sklearn.metrics import PrecisionRecallDisplay

    X = imputer(X)
    return PrecisionRecallDisplay.from_estimator(estimator, X, y)


def precision_recall_curve_from_predictions(y_true, y_pred):
    from sklearn.metrics import PrecisionRecallDisplay

    return PrecisionRecallDisplay.from_predictions(y_true, y_pred)
