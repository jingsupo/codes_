# coding: utf-8

def word_similarity():
    import re
    import nltk
    from bs4 import BeautifulSoup
    from sklearn.datasets import fetch_20newsgroups

    news = fetch_20newsgroups(subset='all')
    x, y = news.data, news.target

    # 把段落分解成由句子组成的list（每个句子又被分解成词语）
    def news_to_sentences(news):
        news_text = BeautifulSoup(news, 'lxml').get_text()
        tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
        raw_sentences = tokenizer.tokenize(news_text)

        # 对每个句子进行处理，分解成词语
        sentences = []
        for sent in raw_sentences:
            sentences.append(re.sub('[^a-zA-Z]', ' ', sent.lower().strip()).split())
        return sentences

    sentences = []
    for xx in x:
        sentences += news_to_sentences(xx)

    # import numpy
    # # 将预处理过的"词库"保存到文件中，便于调试
    # numpy_array = numpy.array(sentences)
    # numpy.save('sentences.npy', numpy_array)
    #
    # # 将预处理后的"词库"从文件中读出，便于调试
    # numpy_array = numpy.load('sentences.npy')
    # sentences = numpy_array.tolist()

    num_features = 300
    min_word_count = 20
    num_workers = 2
    context = 5
    down_sampling = 1e-3

    from gensim.models import word2vec

    model = word2vec.Word2Vec(sentences,
                              workers=num_workers,
                              size=num_features,
                              min_count=min_word_count,
                              window=context,
                              sample=down_sampling)

    model.init_sims(replace=True)

    # 保存word2vec训练参数便于调试
    # model.wv.save_word2vec_format('word2vec_model.bin', binary=True)
    # model.wv.load_word2vec_format('word2vec_model.bin', binary=True)
    return model


if __name__ == '__main__':
    model = word_similarity()

    print('词语相似度计算：')
    print('morning vs morning:')
    print(model.wv.n_similarity('morning', 'morning'))
    print('morning vs afternoon:')
    print(model.wv.n_similarity('morning', 'afternoon'))
    print('morning vs hello:')
    print(model.wv.n_similarity('morning', 'hello'))
    print('morning vs shell:')
    print(model.wv.n_similarity('morning', 'shell'))
