# 使用mysqldump备份数据库

## 备份一个数据库

mysqldump -h host -u root -p --databases dbname > backup.sql

## 备份压缩

mysqldump -h host -u root -p --databases dbname | gzip > backup.sql.gz

> gzip解压命令

gzip -d backup.sql.gz

## 备份数据库中的某个表

mysqldump -h host -u root -p --databases dbname --tables tbname1 tbname2 > backup.sql

## 备份多个数据库

mysqldump -h host -u root -p --databases dbname1 dbname2 > backup.sql

## 备份系统中所有数据库

mysqldump -h host -u root -p --all-databases > backup.sql


# 还原数据库

mysql -u root -p [dbname] < backup.sql
> 执行前需要先创建dbname数据库，如果backup.sql是mysqldump创建的备份文件则执行时不需要dbname。

MYSQL> source backup.sql;
> 执行source命令前需要先选择数据库。

