#!/bin/bash
#功能说明：本功能用于备份MySQL数据库
#编写日期：2020/09/23
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:/usr/local/mysql/bin
export PATH
mkdir -p /opt/mysql_backup/log
#数据库用户名
dbuser='root'
#数据库密码
dbpasswd='MYSQL@jing1015'
#数据库名,可以定义多个数据库，中间以空格隔开，如：test test1 test2
dbnames='demo'
#备份时间
backuptime=`date +%Y%m%d%H%M%S`
#日志备份路径
logpath='/opt/mysql_backup/log'
#数据备份路径
datapath='/opt/mysql_backup'
#日志记录头部
echo “备份时间为${backuptime}，备份数据库表 ${dbnames} 开始” >> ${logpath}/mysqllog.log
#正式备份数据库
for dbname in $dbnames; do
source=`mysqldump --single-transaction -u${dbuser} -p${dbpasswd} --databases ${dbname} > ${datapath}/${backuptime}.sql` 2>> ${logpath}/mysqllog.log;
#备份成功执行以下操作
if [ "$?" == 0 ];then
cd $datapath
#为节约硬盘空间，将数据库压缩
tar -czvf ${dbname}${backuptime}.tar.gz ${backuptime}.sql
#删除原始文件，只留压缩后文件
rm -f ${datapath}/${backuptime}.sql
echo “数据库表 ${dbname} 备份成功!” >> ${logpath}/mysqllog.log
else
#备份失败执行以下操作
echo “数据库表 ${dbname} 备份失败!” >> ${logpath}/mysqllog.log
fi
done
