#!/bin/bash
#功能说明：本功能用于还原MySQL数据库
#编写日期：2020/09/24
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:/usr/local/mysql/bin
export PATH
#数据库用户名
dbuser='root'
#数据库密码
dbpasswd='MYSQL@jing1015'
#还原时间
restoretime=`date +%Y%m%d%H%M%S`
#日志备份路径
logpath='/opt/mysql_backup/log'
#数据备份路径
datapath='/opt/mysql_backup'
cd $datapath
#********************要还原的数据库文件********************
filepath='demo20200923215821.tar.gz'
sqlpath=`tar -xzvf ${filepath}`
#日志记录头部
echo “还原时间为${restoretime}，还原数据库文件 ${filepath} 开始” >> ${logpath}/mysqllog.log
#还原数据库
source=`mysql -u${dbuser} -p${dbpasswd} < ${datapath}/${sqlpath}` 2>> ${logpath}/mysqllog.log;
#还原成功执行以下操作
if [ "$?" == 0 ];then
#删除原始文件，只留压缩后文件
rm -f ${datapath}/${sqlpath}
echo “数据库文件 ${filepath} 还原成功!” >> ${logpath}/mysqllog.log
else
#备份失败执行以下操作
echo “数据库文件 ${filepath} 还原失败!” >> ${logpath}/mysqllog.log
fi
