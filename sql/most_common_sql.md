# 查询表结构

## MySQL

```sql
select column_name, column_type, data_type, numeric_scale 
from information_schema.columns 
where table_name='table_name' and table_schema='db_name';

show full columns from table_name;

show columns from table_name;

describe table_name; --简写desc
```

## Oracle

```sql
select column_name, data_type from user_tab_columns where table_name = upper('table_name');
```

# 查询建表语句

## MySQL

```sql
show create table db_name.table_name;
```

## Oracle

```sql
SELECT DBMS_METADATA.GET_DDL('TABLE', 'TABLE_NAME') FROM DUAL; --最好都大写
```

# 查询包含指定字符串的表名

## impala

```sql
show tables in default like '*alarm*';
```
